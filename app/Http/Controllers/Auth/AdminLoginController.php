<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminLoginController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm(){
        return view('auth.admin-login');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ],
        [
            'email.email'   => 'Alamat Email harus dalam format email!'
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
            return Redirect::route('admin.dashboard');
        }
        else{
            return redirect()->back()->withErrors('Alamat Email atau Kata Sandi salah!', 'default')->withInput($request->only('email'));
        }
    }
}
