<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\InvoiceHeader;
use App\Models\KajianOrderHeader;
use App\Models\Order;
use App\Models\PermissionMenu;
use App\Models\SalesOrderHeader;
use App\Models\SuperadminNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboard(){
        $today = Carbon::today('Asia/Jakarta');
        $admin = Auth::guard('admin')->user();
        $isSuperAdmin = $admin->is_super_admin === 1;
        $role = $admin->admin_user_role()->pluck('id')[0];

        // Get dashboard menus
        $role = $admin->admin_user_role()->pluck('id')[0];
        $authorizedMenus = PermissionMenu::join('menus', 'permission_menus.menu_id', '=', 'menus.id')
            ->where('permission_menus.admin_role_id', $role)
            ->orderBy('menus.index')
            ->get();

        $data = [
            'isSuperAdmin'              => $isSuperAdmin,
            'role'                      => $role,
            'today'                     => $today,
            'authorizedMenus'           => $authorizedMenus,
        ];

        return view('admin.dashboard')->with($data);
    }
}
