<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClientData;
use App\Models\Invoice;
use App\Models\InvoiceDiscount;
use App\Models\InvoiceModule;
use App\Models\InvoiceModuleDetail;
use App\Models\Module;
use App\Models\ModuleDetail;
use App\Models\Quotation;
use App\Models\QuotationDiscount;
use App\Models\QuotationModule;
use App\Models\QuotationModuleDetail;
use App\Transformer\ModuleTransformer;
use App\Transformer\QuotationTransformer;
use Carbon\Carbon;
use http\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class QuotationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $quotations = Quotation::where('id', '>', 0)
            ->orderBy('created_at', 'DESC');
        return DataTables::of($quotations)
            ->setTransformer(new QuotationTransformer)
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.quotations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = ClientData::all();
        $modules = Module::all();
        $latestModule = Module::latest()->first();
        $data = [
            'type'      => "Quotation",
            'clients'   => $clients,
            'modules'   => $modules,
            'moduleJsons'   => json_encode($modules),
            'latestModule'   => $latestModule->id,
            'todayDate' => Carbon::now('Asia/Jakarta')->format('d M Y')
        ];

        return view('admin.quotations.create-new')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'quotation_number'  => 'required',
                'project_name'      => 'required',
                'header_type'       => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            $isAuthorization = 0;
            $authorization = $request->input('authorization');
            if(!empty($authorization)){
                $isAuthorization = 1;
            }

            $amounts = $request->input('prices');
            $modules = $request->input('modules');
            $details = $request->input('moduleDetails');

            $newModules = $request->input('new-modules');
            $newDetailModuleIds = $request->input('newModuleDetailIds');
            $newDetailModules = $request->input('new-module-details');
            $newAmounts = [];

            foreach ($amounts as $amount){
                if(!empty($amount)) {
                    array_push($newAmounts, $amount);
                }
            }
            if(count($newAmounts) == 0){
                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
            }

            $validFinish = true;
            if(!empty($modules)){
                $ct = 0;
                foreach ($modules as $module){
                    if(empty($module) && empty($newModules[$ct])) {
                        $validFinish = false;
                    }
                    else{
                        if(empty($newAmounts[$ct])) $validFinish = false;
                    }
                    $ct++;
                }
            }
            if(!$validFinish){
                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
            }
//            dd($request);
            $user = Auth::user();

            $totalAmount = 0;
            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
            //add if new client

            //check client
            if(!empty($request->input('newClientName')) && !empty($request->input('newClientPic'))){
                $checkingClient = ClientData::where('name', $request->input('newClientName'))->first();
                if(empty($checkingClient)){
                    $newClient = ClientData::create([
                        'name'          => $request->input('newClientName'),
                        'address'       => "",
                        'pic_name'       => $request->input('newClientPic'),
                        'pic_position'   => "",
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $clientId = $newClient->id;
                }
                else{
                    $clientId = $request->input('client');
                }
            }
            else{
                $clientId = $request->input('client');
            }

            //create invoice
            $quotation = Quotation::create([
                'number'            => $request->input('quotation_number'),
                'project_name'      => $request->input('project_name'),
                'client_id'         => $clientId,
                'date'              => $date->toDateTimeString(),
                'header_type'       => $request->input('header_type'),
                'authorization'     => $isAuthorization,
                'created_at'        => Carbon::now('Asia/Jakarta'),
                'created_by'        => $user->id
            ]);

            //looping module
            $idxModule = 0;
            foreach ($modules as $module){

                // exist module and save to InvoiceModule
                $moduleId = $module;
                $dbModule = Module::find($module);
                if($dbModule != null){
                    QuotationModule::create([
                        'quotation_id'  => $quotation->id,
                        'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                        'module_id'     => $module,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                }
                // create new module and save to InvoiceModule
                else{
                    if(!empty($newModules[$idxModule])){
                        $newModuleDB = Module::where('description', ucwords($newModules[$idxModule]))->first();
                        if(empty($newModuleDB)){
                            $moduleSubmit = Module::create([
                                'description'     => ucwords($newModules[$idxModule]),
                                'created_at'    => Carbon::now('Asia/Jakarta'),
                                'created_by'    => $user->id
                            ]);
                            $moduleId = $moduleSubmit->id;
                        }
                        else{
                            $moduleId = $newModuleDB->id;
                        }
                        QuotationModule::create([
                            'quotation_id'  => $quotation->id,
                            'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                            'module_id'     => $moduleId,
                            'created_at'    => Carbon::now('Asia/Jakarta'),
                            'created_by'    => $user->id
                        ]);
                        $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                    }
                }

                // detail module looping
                foreach ($details as $detail){
                    // existing detail module / not and save to InvoiceModuleDetail
                    if(strpos($detail, $module) !== false){
                        $detailArr = explode("#", $detail);
                        $moduleDetail = ModuleDetail::where('description', ucwords($detailArr[1]))->first();
                        $moduleDetailId = 0;
                        if(empty($moduleDetail)){
                            $newModuleDetail = ModuleDetail::create([
                                'module_id'          => $module,
                                'description'        => $detailArr[1],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetail->id;
                        }
                        else{
                            $moduleDetailId = $moduleDetail->id;
                        }
                        QuotationModuleDetail::create([
                            'quotation_id'        => $quotation->id,
                            'module_detail_id'  => $moduleDetailId,
                            'created_at'        => Carbon::now('Asia/Jakarta'),
                            'created_by'        => $user->id
                        ]);
                    }
                }

                //check if any new detail module inputed
                $idxDetail = 0;
                foreach ($newDetailModuleIds as $newDetailModuleId){
                    // existing detail module / not and save to InvoiceModuleDetail
                    $moduleDetailId = 0;
                    if($newDetailModuleId == $module){
                        if(!empty($newDetailModules[$idxDetail])){
                            $newModuleDetailSubmit = ModuleDetail::create([
                                'module_id'          => $moduleId,
                                'description'        => $newDetailModules[$idxDetail],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetailSubmit->id;

                            QuotationModuleDetail::create([
                                'quotation_id'        => $quotation->id,
                                'module_detail_id'  => $moduleDetailId,
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                        }
                    }
                    $idxDetail++;
                }

                $idxModule++;
            }
            $quotation->subtotal = $totalAmount;
            $quotation->total_amount = $totalAmount;
            $quotation->save();

            //Add Module discount
            $discNames = $request->input('disc_name');
            $discAmounts = $request->input('disc_amount');
            $discPercents = $request->input('disc_percentage');
            $j = 0;
            foreach ($discNames as $discName){
                if(!empty($discName)){
                    $newInvoiceDiscount = QuotationDiscount::create([
                        'quotation_id'        => $quotation->id,
                        'description'       => $discName,
                        'amount'            => 0,
                        'percentage'        => 0,
                        'percentage_rupiah' => 0,
                        'created_at'        => Carbon::now('Asia/Jakarta'),
                        'created_by'        => $user->id
                    ]);
                    if(!empty($discAmounts[$j])){
                        $totalAmount = $totalAmount - (int)str_replace(",","",$discAmounts[$j]);
                        $quotation->total_amount = $totalAmount;
                        $quotation->save();

                        $newInvoiceDiscount->amount = (int)str_replace(",","",$discAmounts[$j]);
                        $newInvoiceDiscount->save();
                    }
                    if(!empty($discPercents[$j])){
                        $percentRupiah = ($totalAmount * $discPercents[$j]) / 100;
                        $totalAmount = $totalAmount - $percentRupiah;
                        $quotation->total_amount = $totalAmount;
                        $quotation->save();

                        $newInvoiceDiscount->percentage = $discPercents[$j];
                        $newInvoiceDiscount->percentage_rupiah = $percentRupiah;
                        $newInvoiceDiscount->save();
                    }
                }
                $j++;
            }

            Session::flash('success', 'Success Creating new Quotation!');
            return redirect()->route('admin.quotations.index');
        }
        catch(\Exception $ex){
//            dd($ex);
            Log::error('Admin/QuotationController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        try{
//            $validator = Validator::make($request->all(), [
//                'quotation_number'   => 'required',
//                'project_name'   => 'required',
//                'header_type'       => 'required'
//            ]);
//
//            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
//
//            $amounts = $request->input('amounts');
//            $newAmounts = [];
//            foreach ($amounts as $amount){
//                if(!empty($amount)) {
//                    array_push($newAmounts, $amount);
//                }
//            }
//            if(count($newAmounts) == 0){
//                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
//            }
//
//            $modules = $request->input('modules');
//            $validFinish = true;
//            if(!empty($modules)){
//                $ct = 0;
//                foreach ($modules as $module){
//                    if(empty($module)) {
//                        $validFinish = false;
//                    }
//                    else{
//                        if(empty($newAmounts[$ct])) $validFinish = false;
//                    }
//                    $ct++;
//                }
//            }
//            if(!$validFinish){
//                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
//            }
//            $user = Auth::user();
//
//            $totalAmount = 0;
//            $amounts = $request->input('amounts');
//            $i = 0;
//            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
//
//            $quotation = Quotation::create([
//                'number'            => $request->input('quotation_number'),
//                'project_name'      => $request->input('project_name'),
//                'client_id'         => $request->input('client'),
//                'date'              => $date->toDateTimeString(),
//                'header_type'       => $request->input('header_type'),
//                'created_at'        => Carbon::now('Asia/Jakarta'),
//                'created_by'        => $user->id
//            ]);
//
//            //Add Quotation Modules
//            foreach ($request->input('modules') as $module){
//                $dbModule = Module::find($module);
//                if($dbModule != null){
//                    QuotationModule::create([
//                        'quotation_id'  => $quotation->id,
//                        'amount'        => $newAmounts[$i],
//                        'module_id'     => $module,
//                        'created_at'    => Carbon::now('Asia/Jakarta'),
//                        'created_by'    => $user->id
//                    ]);
//                    $totalAmount += $newAmounts[$i];
//                }
//                $i++;
//            }
//
//            $quotation->subtotal = $totalAmount;
//            $quotation->discount_1 = 0;
//            $quotation->discount_2 = 0;
//            $quotation->discount_3 = 0;
//            $quotation->discount_4 = 0;
//            $quotation->total_amount = $totalAmount;
//            if($request->input('discount_1') != null){
//                $totalAmount = $totalAmount - $request->input('discount_1');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_1 = $request->input('discount_1');
//            }
//            if($request->input('discount_2') != null){
//                $totalAmount = $totalAmount - $request->input('discount_2');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_2 = $request->input('discount_2');
//            }
//            if($request->input('discount_3') != null){
//                $totalAmount = $totalAmount - $request->input('discount_3');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_3 = $request->input('discount_3');
//            }
//            if($request->input('discount_4') != null){
//                $totalAmount = $totalAmount - $request->input('discount_4');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_4 = $request->input('discount_4');
//            }
//            $quotation->save();
//
//            //Add Module Details
//            foreach ($request->input('details') as $detail){
//                $dbDetail = ModuleDetail::find($detail);
//                if($dbDetail != null){
//                    QuotationModuleDetail::create([
//                        'quotation_id'      => $quotation->id,
//                        'module_detail_id'  => $detail,
//                        'created_at'        => Carbon::now('Asia/Jakarta'),
//                        'created_by'        => $user->id
//                    ]);
//                }
//            }
//
//            Session::flash('success', 'Success Creating new Quotation!');
//            return redirect()->route('admin.quotations.index');
//        }
//        catch(\Exception $ex){
//            Log::error('Admin/ModuleController - store error EX: '. $ex);
//            return redirect()->back()->withErrors('Internal Server Error')->withInput();
//        }
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $module = Module::find($id);
            if(empty($module)){
                return redirect()->back();
            }

            $data = [
                'module'    => $module
            ];

            return view('admin.quotations.show')->with($data);
        }
        catch (\Exception $ex){
            Log::error('Admin/QuotationController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quotation = Quotation::find($id);
        $quotationModules = QuotationModule::where('quotation_id', $quotation->id)->get();
        $quotationModuleDetails = QuotationModuleDetail::where('quotation_id', $quotation->id)->get();
        $quotationModuleDiscounts = QuotationDiscount::where('quotation_id', $quotation->id)->get();
        $clients = ClientData::all();
        $modules = Module::all();
        $latestModule = Module::latest()->first();

        $itemList = collect();
        $itemIdx = 1;
        foreach ($quotationModules as $quotationModule){
            $detailModules = collect();
            foreach($quotationModuleDetails as $quotationModuleDetail){
                $moduleDetailDB = ModuleDetail::find($quotationModuleDetail->module_detail_id);
                if($moduleDetailDB->module_id == $quotationModule->module_id){
                    $itemDetail = ([
                        'value'       => $moduleDetailDB->module_id."#".$moduleDetailDB->description,
                    ]);
                    $detailModules->push($itemDetail);
                }
            }

            $detailDBs = ModuleDetail::where('module_id', $quotationModule->module_id)->get();
            $details = collect();
            foreach ($detailDBs as $detail){
                $itemDetail = ([
                    'id' => $quotationModule->module_id."#".ucwords($detail->description),
                    'text' => $detail->description,
                ]);
                $details->push($itemDetail);
            }

            $module = ([
                'itemId'            => "item".$itemIdx,
                'price'             => $quotationModule->amount,
                'idModules'         => $quotationModule->module_id,
                'modules'           => $modules,
                'selectedModule'    => $quotationModule->module_id,
                'detailModules'     => $detailModules,
                'details'           => $details,
                'newDetailModules'  => ([
                    'value'         => "",
                ]),
            ]);
            $itemList->push($module);
            $itemIdx++;
        }
        $discountList = collect();
        foreach ($quotationModuleDiscounts as $quotationModuleDiscount){

            $discount = ([
                'description'   => $quotationModuleDiscount->description,
                'amount'        => $quotationModuleDiscount->amount,
                'percentage'    => $quotationModuleDiscount->percentage,
            ]);
            $discountList->push($discount);
        }

        //dd($quotation->modules);

        $data = [
            'quotation'         => $quotation,
            'clients'           => $clients,
            'modules'           => $modules,
            'moduleJsons'       => json_encode($modules),
            'itemListJsons'     => json_encode($itemList),
            'discountListJsons' => json_encode($discountList),
            'latestModule'      => $latestModule->id,
            'quotation_date'    => $quotation->date->format('d M Y')
        ];
//        dd($data);

        return view('admin.quotations.edit-new')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'quotation_number'   => 'required',
                'project_name'   => 'required',
                'header_type'       => 'required'
            ]);
//            dd($request);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $amounts = $request->input('prices');
            $modules = $request->input('modules');
            $details = $request->input('moduleDetails');

            $newModules = $request->input('new-modules');
            $newDetailModuleIds = $request->input('newModuleDetailIds');
            $newDetailModules = $request->input('new-module-details');
            $newAmounts = [];
            foreach ($amounts as $amount){
                if(!empty($amount)) {
                    array_push($newAmounts, $amount);
                }
            }
            if(count($newAmounts) == 0){
                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
            }

            $validFinish = true;
            if(!empty($modules)){
                $ct = 0;
                foreach ($modules as $module){
                    if(empty($module)) {
                        $validFinish = false;
                    }
                    else{
                        if(empty($newAmounts[$ct])) $validFinish = false;
                    }
                    $ct++;
                }
            }
            if(!$validFinish){
                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
            }
            $user = Auth::user();

            $totalAmount = 0;
            $i = 0;
            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');

            $quotation = Quotation::find($request->input('id'));
            $quotation->number = $request->input('quotation_number');
            $quotation->project_name = $request->input('project_name');
            $quotation->client_id = $request->input('client');
            $quotation->date = $date;
            $quotation->header_type = $request->input('header_type');
            $quotation->updated_at = Carbon::now('Asia/Jakarta');
            $quotation->updated_by = $user->id;
            $quotation->save();

            //looping module
//            dd($request);
            $idxModule = 0;
            //delete existing data QuotationModule
            $res = QuotationModule::where('quotation_id',$quotation->id)->delete();
            //delete existing data QuotationModuleDetail
            $res2 = QuotationModuleDetail::where('quotation_id',$quotation->id)->delete();

            foreach ($modules as $module){

                // exist module and save to InvoiceModule
                $moduleId = $module;
                $dbModule = Module::find($module);
                if($dbModule != null){
                    QuotationModule::create([
                        'quotation_id'  => $quotation->id,
                        'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                        'module_id'     => $module,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                }
                // create new module and save to InvoiceModule
                else{
                    if(!empty($newModules[$idxModule])){
                        $newModuleDB = Module::where('description', ucwords($newModules[$idxModule]))->first();
                        if(empty($newModuleDB)){
                            $moduleSubmit = Module::create([
                                'description'     => ucwords($newModules[$idxModule]),
                                'created_at'    => Carbon::now('Asia/Jakarta'),
                                'created_by'    => $user->id
                            ]);
                            $moduleId = $moduleSubmit->id;
                        }
                        else{
                            $moduleId = $newModuleDB->id;
                        }
                        QuotationModule::create([
                            'quotation_id'  => $quotation->id,
                            'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                            'module_id'     => $moduleId,
                            'created_at'    => Carbon::now('Asia/Jakarta'),
                            'created_by'    => $user->id
                        ]);
                        $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                    }
                }

                // detail module looping
                foreach ($details as $detail){
                    // existing detail module / not and save to InvoiceModuleDetail
                    if(strpos($detail, $module) !== false){
                        $detailArr = explode("#", $detail);
                        $moduleDetail = ModuleDetail::where('description', ucwords($detailArr[1]))->first();
                        $moduleDetailId = 0;
                        if(empty($moduleDetail)){
                            $newModuleDetail = ModuleDetail::create([
                                'module_id'          => $module,
                                'description'        => $detailArr[1],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetail->id;
                        }
                        else{
                            $moduleDetailId = $moduleDetail->id;
                        }
                        $isExistModuleDetail = QuotationModuleDetail::where('quotation_id', $quotation->id)
                            ->where('module_detail_id', $moduleDetailId)
                                ->first();
                        if(empty($isExistModuleDetail)){
                            QuotationModuleDetail::create([
                                'quotation_id'        => $quotation->id,
                                'module_detail_id'  => $moduleDetailId,
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                        }
                    }
                }

                //check if any new detail module inputed
                $idxDetail = 0;
                foreach ($newDetailModuleIds as $newDetailModuleId){
                    // existing detail module / not and save to InvoiceModuleDetail
                    $moduleDetailId = 0;
                    if($newDetailModuleId == $module){
                        if(!empty($newDetailModules[$idxDetail])){
                            $newModuleDetailSubmit = ModuleDetail::create([
                                'module_id'          => $moduleId,
                                'description'        => $newDetailModules[$idxDetail],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetailSubmit->id;

                            $isExistModuleDetail = QuotationModuleDetail::where('quotation_id', $quotation->id)
                                ->where('module_detail_id', $moduleDetailId)
                                ->first();
                            if(empty($isExistModuleDetail)){
                                QuotationModuleDetail::create([
                                    'quotation_id'        => $quotation->id,
                                    'module_detail_id'  => $moduleDetailId,
                                    'created_at'        => Carbon::now('Asia/Jakarta'),
                                    'created_by'        => $user->id
                                ]);
                            }
                        }
                    }
                    $idxDetail++;
                }

                $idxModule++;
            }
            $quotation->subtotal = $totalAmount;
            $quotation->total_amount = $totalAmount;
            $quotation->save();

            //Add Module discount
            $discNames = $request->input('disc_name');
            $discAmounts = $request->input('disc_amount');
            $discPercents = $request->input('disc_percentage');
            $j = 0;
            $res = QuotationDiscount::where('quotation_id',$quotation->id)->delete();
            if(!empty($discNames)){
                foreach ($discNames as $discName){
                    if(!empty($discName)){
                        $newInvoiceDiscount = QuotationDiscount::create([
                            'quotation_id'        => $quotation->id,
                            'description'       => $discName,
                            'amount'            => 0,
                            'percentage'        => 0,
                            'percentage_rupiah' => 0,
                            'created_at'        => Carbon::now('Asia/Jakarta'),
                            'created_by'        => $user->id
                        ]);
                        if(!empty($discAmounts[$j])){
                            $totalAmount = $totalAmount - (int)str_replace(",","",$discAmounts[$j]);
                            $quotation->total_amount = $totalAmount;
                            $quotation->save();

                            $newInvoiceDiscount->amount = (int)str_replace(",","",$discAmounts[$j]);
                            $newInvoiceDiscount->save();
                        }
                        if(!empty($discPercents[$j])){
                            $percentRupiah = ($totalAmount * $discPercents[$j]) / 100;
                            $totalAmount = $totalAmount - $percentRupiah;
                            $quotation->total_amount = $totalAmount;
                            $quotation->save();

                            $newInvoiceDiscount->percentage = $discPercents[$j];
                            $newInvoiceDiscount->percentage_rupiah = $percentRupiah;
                            $newInvoiceDiscount->save();
                        }
                    }
                    $j++;
                }
            }

            Session::flash('success', 'Success Changing Quotation Data!');
            return redirect()->route('admin.quotations.index');
        }
        catch(\Exception $ex){
//            dd($ex);
            Log::error('Admin/QuotationController - update error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request)
//    {
//        //
//        try{
//            $validator = Validator::make($request->all(), [
//                'quotation_number'   => 'required',
//                'project_name'   => 'required',
//                'header_type'       => 'required'
//            ]);
//            dd($request);
//
//            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
//
//            $amounts = $request->input('amounts');
//            $newAmounts = [];
//            foreach ($amounts as $amount){
//                if(!empty($amount)) {
//                    array_push($newAmounts, $amount);
//                }
//            }
//            if(count($newAmounts) == 0){
//                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
//            }
//
//            $modules = $request->input('modules');
//            $validFinish = true;
//            if(!empty($modules)){
//                $ct = 0;
//                foreach ($modules as $module){
//                    if(empty($module)) {
//                        $validFinish = false;
//                    }
//                    else{
//                        if(empty($newAmounts[$ct])) $validFinish = false;
//                    }
//                    $ct++;
//                }
//            }
//            if(!$validFinish){
//                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
//            }
//            $user = Auth::user();
//
//            $totalAmount = 0;
//            $i = 0;
//            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
//
//            $quotation = Quotation::find($request->input('id'));
//            $quotation->number = $request->input('quotation_number');
//            $quotation->project_name = $request->input('project_name');
//            $quotation->client_id = $request->input('client');
//            $quotation->date = $date;
//            $quotation->header_type = $request->input('header_type');
//            $quotation->updated_at = Carbon::now('Asia/Jakarta');
//            $quotation->updated_by = $user->id;
//            $quotation->save();
//
//            //Add Quotation Modules
//            QuotationModule::where('quotation_id', $quotation->id)->delete();
//            foreach ($request->input('modules') as $module){
//                $dbModule = Module::find($module);
//                if($dbModule != null){
//                    QuotationModule::create([
//                        'quotation_id'  => $quotation->id,
//                        'amount'        => $newAmounts[$i],
//                        'module_id'     => $module,
//                        'created_at'    => Carbon::now('Asia/Jakarta'),
//                        'created_by'    => $user->id
//                    ]);
//                    $totalAmount += $newAmounts[$i];
//                }
//                $i++;
//            }
//
//            $quotation->subtotal = $totalAmount;
//            $quotation->discount_1 = 0;
//            $quotation->discount_2 = 0;
//            $quotation->discount_3 = 0;
//            $quotation->discount_4 = 0;
//            $quotation->total_amount = $totalAmount;
//            if($request->input('discount_1') != null){
//                $totalAmount = $totalAmount - $request->input('discount_1');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_1 = $request->input('discount_1');
//            }
//            if($request->input('discount_2') != null){
//                $totalAmount = $totalAmount - $request->input('discount_2');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_2 = $request->input('discount_2');
//            }
//            if($request->input('discount_3') != null){
//                $totalAmount = $totalAmount - $request->input('discount_3');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_3 = $request->input('discount_3');
//            }
//            if($request->input('discount_4') != null){
//                $totalAmount = $totalAmount - $request->input('discount_4');
//                $quotation->total_amount = $totalAmount;
//                $quotation->discount_4 = $request->input('discount_4');
//            }
//            $quotation->save();
//
//            QuotationModuleDetail::where('quotation_id', $quotation->id)->delete();
//            //Add Module Details
//            foreach ($request->input('details') as $detail){
//                $dbDetail = ModuleDetail::find($detail);
//                if($dbDetail != null){
//                    QuotationModuleDetail::create([
//                        'quotation_id'      => $quotation->id,
//                        'module_detail_id'  => $detail,
//                        'created_at'        => Carbon::now('Asia/Jakarta'),
//                        'created_by'        => $user->id
//                    ]);
//                }
//            }
//
//            Session::flash('success', 'Success Changing Quotation Data!');
//            return redirect()->route('admin.quotations.index');
//        }
//        catch(\Exception $ex){
//            Log::error('Admin/QuotationController - update error EX: '. $ex);
//            return redirect()->back()->withErrors('Internal Server Error')->withInput();
//        }
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $quotationId = $request->input('id');

            $quotation = Quotation::find($quotationId);

            if(empty($module)){
                return Response::json(array('errors' => 'INVALID'));
            }

            QuotationModule::where('quotation_id', $quotation->id)->delete();
            QuotationModuleDetail::where('quotation_id', $quotation->id)->delete();

            $quotNumber = $quotation->number;
            $quotation->delete();

            Session::flash('success', 'Success Deleting Quotation '.$quotNumber);
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            Log::error('Admin/QuotationController - destroy - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createInvoice($id)
    {
        try {
            $user = Auth::user();

            $quotation = Quotation::find($id);
            $quotationModules = $quotation->modules;
            $quotationModuleDetails = $quotation->module_details;
            $quotationModuleDiscounts = $quotation->quotation_discounts;

//            dd($quotation, $quotationModules, $quotationModuleDetails, $quotationModuleDiscounts);

            $invoice = Invoice::create([
                'number'            => "inv/".$quotation->number,
                'project_name'      => $quotation->project_name,
                'client_id'         => $quotation->client_id,
                'date'              => $quotation->date,
                'header_type'       => $quotation->header_type,
                'authorization'     => 0,
                'is_authorized'     => 0,
                'total_amount'      => $quotation->total_amount,
                'subtotal'          => $quotation->subtotal,
                'created_at'        => Carbon::now('Asia/Jakarta'),
                'created_by'        => $user->id
            ]);

            foreach($quotationModules as $quotationModule){
                $invoiceModule = InvoiceModule::create([
                    'invoice_id'  => $invoice->id,
                    'amount'        => $quotationModule->amount,
                    'module_id'     => $quotationModule->module_id,
                    'created_at'    => Carbon::now('Asia/Jakarta'),
                    'created_by'    => $user->id
                ]);
            }

            foreach($quotationModuleDetails as $quotationModuleDetail){
                $invoiceDetail = InvoiceModuleDetail::create([
                    'invoice_id'        => $invoice->id,
                    'module_detail_id'  => $quotationModuleDetail->module_detail_id,
                    'created_at'        => Carbon::now('Asia/Jakarta'),
                    'created_by'        => $user->id
                ]);
            }

            foreach($quotationModuleDiscounts as $quotationModuleDiscount){
                $newInvoiceDiscount = InvoiceDiscount::create([
                    'invoice_id'        => $invoice->id,
                    'description'       => $quotationModuleDiscount->description,
                    'amount'            => $quotationModuleDiscount->amount,
                    'percentage'        => $quotationModuleDiscount->percentage,
                    'percentage_rupiah' => $quotationModuleDiscount->percentage_rupiah,
                    'created_at'        => Carbon::now('Asia/Jakarta'),
                    'created_by'        => $user->id
                ]);
            }


            return redirect()->route('admin.invoices.edit', ['id' => $invoice->id]);
        }
        catch(\Exception $ex){
//            dd($ex);
            Session::flash('error', 'Error create Invoice from quotation!');
            Log::error('Admin/QuotationController - createInvoice - error EX: '. $ex);
            return redirect()->route('admin.quotations.index');
        }
    }

}
