<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClientData;
use App\Models\Module;
use App\Models\ModuleDetail;
use App\Transformer\ClientTransformer;
use App\Transformer\ModuleTransformer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $modules = ClientData::all();
        return DataTables::of($modules)
            ->setTransformer(new ClientTransformer)
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'pic_name'   => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $user = Auth::user();

            ClientData::create([
                'name'          => $request->input('name'),
                'address'       => $request->input('address'),
                'pic_name'       => $request->input('pic_name'),
                'pic_position'   => $request->input('pic_position'),
                'created_at'    => Carbon::now('Asia/Jakarta'),
                'created_by'    => $user->id
            ]);

            Session::flash('success', 'Sukses membuat client Baru');
            return redirect()->route('admin.clients.index');
        }
        catch(\Exception $ex){
            Log::error('Admin/ClientController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        try{
//            $module = Module::find($id);
//            if(empty($module)){
//                return redirect()->back();
//            }
//
//            $data = [
//                'module'    => $module
//            ];
//
//            return view('admin.clients.show')->with($data);
//        }
//        catch (\Exception $ex){
//            Log::error('Admin/ModuleController - store error EX: '. $ex);
//            return redirect()->back()->withErrors('Internal Server Error')->withInput();
//        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = ClientData::find($id);
        $data = [
            'client'    => $client
        ];

        return view('admin.clients.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'pic_name'   => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $user = Auth::user();

            $client = ClientData::find($request->input('id'));
            $client->name = $request->input('name');
            $client->address = $request->input('address');
            $client->pic_name = $request->input('pic_name');
            $client->pic_position = $request->input('pic_position');
            $client->updated_by = $user->id;
            $client->updated_at = Carbon::now('Jakarta/Asia');
            $client->save();

            Session::flash('success', 'Sukses mengubah data Client!');
            return redirect()->route('admin.clients.index');
        }
        catch(\Exception $ex){
            Log::error('Admin/ClientController - update error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $clientId = $request->input('id');

            $client = ClientData::find($clientId);

            if(empty($module)){
                return Response::json(array('errors' => 'INVALID'));
            }

            $client->delete();

            Session::flash('success', 'Sukses menghapus data Client');
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - destroy - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }
}
