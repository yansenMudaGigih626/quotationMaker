<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\AddressProvince;
use App\Models\AdminUserRole;
use App\Models\Cart;
use App\Models\Configuration;
use App\Models\FcmTokenApp;
use App\Models\Invoice;
use App\Models\Island;
use App\Models\Product;
use App\Models\ProductUserCategory;
use App\Models\Quotation;
use App\Models\User;
use App\Models\UserCategory;
use App\Transformer\UserTransformer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class PrintController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function printQuotation($id, Request $request)
    {
        //
        $quotation = Quotation::find($id);
        $type   = $quotation->header_type;
        $configuration = Configuration::where('id', $type)->first();
        $now = Carbon::parse(Carbon::now())->format('d F Y');
        $data = [
            'quotation'         => $quotation,
            'configuration'     => $configuration,
            'now'   => $now,
            'isSign'          => $request->get('type'),
        ];
//        dd($quotation->module_details);
        return view('admin.print.show-quotation-new1')->with($data);
    }

    public function printInvoice($id, Request $request)
    {
        //
        $invoice = Invoice::find($id);
        $type   = $invoice->header_type;
        $configuration = Configuration::where('id', $type)->first();
        $now = Carbon::parse(Carbon::now())->format('d F Y');
        $data = [
            'invoice'         => $invoice,
            'configuration'   => $configuration,
            'now'             => $now,
            'isSign'          => $request->get('type'),

        ];
//        dd($quotation->module_details);
        return view('admin.print.show-invoice-new1')->with($data);
    }

}
