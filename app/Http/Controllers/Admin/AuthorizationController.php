<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClientData;
use App\Models\Invoice;
use App\Models\Module;
use App\Transformer\InvoiceAuthTransformer;
use App\Transformer\InvoiceTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class AuthorizationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getInvoicesIndex(Request $request){
        $invoices = Invoice::where('authorization', 1)->get();
        return DataTables::of($invoices)
            ->setTransformer(new InvoiceAuthTransformer)
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexInvoices()
    {
        return view('admin.auth-invoices.index');
    }

    /**
     * Function to show Invoice Details.
    */
    public function showInvoice($id){
        try{
            $invoice = Invoice::find($id);
            $clients = ClientData::all();
            $modules = Module::all();

            //dd($invoice->modules);

            $data = [
                'invoice'         => $invoice,
                'clients'           => $clients,
                'modules'           => $modules,
                'quotation_date'    => $invoice->date->format('d M Y')
            ];

            return view('admin.auth-invoices.show')->with($data);
        }
        catch (\Exception $ex){
            Log::error('Admin/AuthorizationController - Show error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Function to authorize Invoice.
     * For status
     * 0 = no need Authorize
     * 1 = accepted
     * 2 = rejected
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approveInvoice(Request $request){
        try{
            $user = Auth::user();
            $invoice = Invoice::find($request->input('invoice_id'));
            $invoice->is_authorized = 1;
            $invoice->authorized_by = $user->id;
            $invoice->save();

            Session::flash('success', 'Success Approving Invoice '. $invoice->number .' !');
            return view('admin.auth-invoices.index');
        }
        catch (\Exception $exception){
            Log::error('Admin/AuthorizationController - Approve error EX: '. $exception);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Function to reject the Invoice.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function rejectInvoice(Request $request){
        try{
            $user = Auth::user();
            $invoice = Invoice::find($request->input('invoice_id'));
            $invoice->is_authorized = 2;
            $invoice->reject_reason = $request->input('reject_reason');
            $invoice->authorization = $user->id;
            $invoice->save();

            Session::flash('success', 'Success Approving Invoice '. $invoice->number .' !');
            return view('admin.auth-invoices.index');
        }
        catch (\Exception $exception){
            Log::error('Admin/AuthorizationController - Approve error EX: '. $exception);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    public function approveQuotation(Request $request){
        try{

        }
        catch (\Exception $exception){

        }
    }
}
