<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminUserRole;
use App\Models\Role;
use App\Transformer\RoleTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.role.index');
    }

    public function getIndex(Request $request){
        $roles = AdminUserRole::all();
        return DataTables::of($roles)
            ->setTransformer(new RoleTransformer)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name'          => 'required|max:100'
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $now = Carbon::now('Asia/Jakarta');

            AdminUserRole::create([
                'name'          => strtoupper($request->input('name')),
                'description'   => $request->filled('description') ? strtoupper($request->input('description')) : '',
                'created_at'    => $now->toDateTimeString(),
                'updated_at'    => $now->toDateTimeString()
            ]);

            Session::flash('success', 'Sukses membuat role/posisi admin baru!');
            return redirect()->route('admin.role.index');
        }
        catch (\Exception $ex){
            error_log($ex);
            Log::error('Admin/RoleController - store error EX: '. $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $role = AdminUserRole::find($id);
        if(empty($role)){
            return redirect()->back();
        }

        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, int $id)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name'          => 'required|max:100'
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $now = Carbon::now('Asia/Jakarta');

            $role = AdminUserRole::find($id);
            if(empty($role)){
                return back()->withErrors("INVALID INPUT!")->withInput($request->all());
            }

            $role->name = strtoupper($request->input('name'));
            $role->description = $request->filled('description') ? strtoupper($request->input('description')) : '';
            $role->updated_at = $now->toDateTimeString();
            $role->save();

            Session::flash('success', 'Sukses mengubah data role/posisi admin!');
            return redirect()->route('admin.role.index');
        }
        catch (\Exception $ex){
            error_log($ex);
            Log::error('Admin/RoleController - update error EX: '. $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        try{
            $role = AdminUserRole::find($request->input('id'));
            $role->delete();

            return \Illuminate\Support\Facades\Response::json(array('errors' => 'INVALID'));
        }
        catch (\Exception $ex){
            error_log($ex);
            Log::error('Admin/RoleController - destroy error EX: '. $ex);
            return \Illuminate\Support\Facades\Response::json(array('errors' => 'INVALID'));
        }
    }

    public function getRoles(Request $request){
        $term = trim($request->q);
        $roles = Role::where(function ($q) use ($term) {
                $q->where('name', 'LIKE', '%' . $term . '%');
            })
            ->get();

        $formatted_tags = [];

        foreach ($roles as $role) {
            $formatted_tags[] = ['id' => $role->id, 'text' => $role->name];
        }

        return \Response::json($formatted_tags);
    }
}
