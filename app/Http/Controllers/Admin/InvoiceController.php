<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClientData;
use App\Models\Invoice;
use App\Models\InvoiceDiscount;
use App\Models\InvoiceModule;
use App\Models\InvoiceModuleDetail;
use App\Models\Module;
use App\Models\ModuleDetail;
use App\Models\Quotation;
use App\Transformer\InvoiceTransformer;
use Carbon\Carbon;
use http\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $quotations = Invoice::where('id', '>', 0)
                ->orderBy('created_at', 'DESC');
        return DataTables::of($quotations)
            ->setTransformer(new InvoiceTransformer)
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.invoices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = ClientData::all();
        $modules = Module::all();
        $latestModule = Module::latest()->first();

        $data = [
            'type'      => "Invoice",
            'clients'   => $clients,
            'modules'   => $modules,
            'moduleJsons'   => json_encode($modules),
            'latestModule'   => $latestModule->id,
            'todayDate' => Carbon::now('Asia/Jakarta')->format('d M Y')
        ];
//        dd($data);

        return view('admin.invoices.create-new')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'quotation_number'  => 'required',
                'project_name'      => 'required',
                'header_type'       => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            $isAuthorization = 0;
            $authorization = $request->input('authorization');
            if(!empty($authorization)){
                $isAuthorization = 1;
            }

            $amounts = $request->input('prices');
            $modules = $request->input('modules');
            $details = $request->input('moduleDetails');

            $newModules = $request->input('new-modules');
            $newDetailModuleIds = $request->input('newModuleDetailIds');
            $newDetailModules = $request->input('new-module-details');
            $newAmounts = [];

            foreach ($amounts as $amount){
                if(!empty($amount)) {
                    array_push($newAmounts, $amount);
                }
            }
            if(count($newAmounts) == 0){
                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
            }

            $validFinish = true;
            if(!empty($modules)){
                $ct = 0;
                foreach ($modules as $module){
                    if(empty($module) && empty($newModules[$ct])) {
                        $validFinish = false;
                    }
                    else{
                        if(empty($newAmounts[$ct])) $validFinish = false;
                    }
                    $ct++;
                }
            }
            if(!$validFinish){
                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
            }
//            dd($request);
            $user = Auth::user();

            $totalAmount = 0;
            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
            //add if new client

            //check client
            if(!empty($request->input('newClientName')) && !empty($request->input('newClientPic'))){
                $checkingClient = ClientData::where('name', $request->input('newClientName'))->first();
                if(empty($checkingClient)){
                    $newClient = ClientData::create([
                        'name'          => $request->input('newClientName'),
                        'address'       => "",
                        'pic_name'       => $request->input('newClientPic'),
                        'pic_position'   => "",
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $clientId = $newClient->id;
                }
                else{
                    $clientId = $request->input('client');
                }
            }
            else{
                $clientId = $request->input('client');
            }

            //create invoice
            $invoice = Invoice::create([
                'number'                => $request->input('quotation_number'),
                'project_name'          => $request->input('project_name'),
                'client_id'             => $clientId,
                'date'                  => $date->toDateTimeString(),
                'header_type'           => $request->input('header_type'),
                'invoice_type'          => "DOWN PAYMENT",
                'invoice_type_percent'  => 50,
                'authorization'         => $isAuthorization,
                'created_at'            => Carbon::now('Asia/Jakarta'),
                'created_by'            => $user->id
            ]);
            $invoice->invoice_type = $request->input('invoice_type');
            $invoice->invoice_type_percent = $request->input('invoice_type_percent');

            //looping module
            $idxModule = 0;
            foreach ($modules as $module){

                // exist module and save to InvoiceModule
                $moduleId = $module;
                $dbModule = Module::find($module);
                if($dbModule != null){
                    InvoiceModule::create([
                        'invoice_id'  => $invoice->id,
                        'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                        'module_id'     => $module,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                }
                // create new module and save to InvoiceModule
                else{
                    if(!empty($newModules[$idxModule])){
                        $newModuleDB = Module::where('description', ucwords($newModules[$idxModule]))->first();
                        if(empty($newModuleDB)){
                            $moduleSubmit = Module::create([
                                'description'     => ucwords($newModules[$idxModule]),
                                'created_at'    => Carbon::now('Asia/Jakarta'),
                                'created_by'    => $user->id
                            ]);
                            $moduleId = $moduleSubmit->id;
                        }
                        else{
                            $moduleId = $newModuleDB->id;
                        }
                        InvoiceModule::create([
                            'invoice_id'  => $invoice->id,
                            'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                            'module_id'     => $moduleId,
                            'created_at'    => Carbon::now('Asia/Jakarta'),
                            'created_by'    => $user->id
                        ]);
                        $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                    }
                }

                // detail module looping
                foreach ($details as $detail){
                    // existing detail module / not and save to InvoiceModuleDetail
                    if(strpos($detail, $module) !== false){
                        $detailArr = explode("#", $detail);
                        $moduleDetail = ModuleDetail::where('description', ucwords($detailArr[1]))->first();
                        $moduleDetailId = 0;
                        if(empty($moduleDetail)){
                            $newModuleDetail = ModuleDetail::create([
                                'module_id'          => $module,
                                'description'        => $detailArr[1],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetail->id;
                        }
                        else{
                            $moduleDetailId = $moduleDetail->id;
                        }
                        $invoiceDetail = InvoiceModuleDetail::create([
                            'invoice_id'        => $invoice->id,
                            'module_detail_id'  => $moduleDetailId,
                            'created_at'        => Carbon::now('Asia/Jakarta'),
                            'created_by'        => $user->id
                        ]);
                    }
                }

                //check if any new detail module inputed
                $idxDetail = 0;
                foreach ($newDetailModuleIds as $newDetailModuleId){
                    // existing detail module / not and save to InvoiceModuleDetail
                    $moduleDetailId = 0;
                    if($newDetailModuleId == $module){
                        if(!empty($newDetailModules[$idxDetail])){
                            $newModuleDetailSubmit = ModuleDetail::create([
                                'module_id'          => $moduleId,
                                'description'        => $newDetailModules[$idxDetail],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetailSubmit->id;

                            $invoiceDetail = InvoiceModuleDetail::create([
                                'invoice_id'        => $invoice->id,
                                'module_detail_id'  => $moduleDetailId,
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                        }
                    }
                    $idxDetail++;
                }

                $idxModule++;
            }
            $invoice->subtotal = $totalAmount;
            $invoice->total_amount = $totalAmount;
            $invoice->save();

            //Add Module discount
            $discNames = $request->input('disc_name');
            $discAmounts = $request->input('disc_amount');
            $discPercents = $request->input('disc_percentage');
            $j = 0;
            foreach ($discNames as $discName){
                if(!empty($discName)){
                    $newInvoiceDiscount = InvoiceDiscount::create([
                        'invoice_id'        => $invoice->id,
                        'description'       => $discName,
                        'amount'            => 0,
                        'percentage'        => 0,
                        'percentage_rupiah' => 0,
                        'created_at'        => Carbon::now('Asia/Jakarta'),
                        'created_by'        => $user->id
                    ]);
                    if(!empty($discAmounts[$j])){
                        $totalAmount = $totalAmount - (int)str_replace(",","",$discAmounts[$j]);
                        $invoice->total_amount = $totalAmount;
                        $invoice->save();

                        $newInvoiceDiscount->amount = (int)str_replace(",","",$discAmounts[$j]);
                        $newInvoiceDiscount->save();
                    }
                    if(!empty($discPercents[$j])){
                        $percentRupiah = ($totalAmount * $discPercents[$j]) / 100;
                        $totalAmount = $totalAmount - $percentRupiah;
                        $invoice->total_amount = $totalAmount;
                        $invoice->save();

                        $newInvoiceDiscount->percentage = $discPercents[$j];
                        $newInvoiceDiscount->percentage_rupiah = $percentRupiah;
                        $newInvoiceDiscount->save();
                    }
                }
                $j++;
            }

            Session::flash('success', 'Success Creating new Invoice!');
            return redirect()->route('admin.invoices.index');
        }
        catch(\Exception $ex){
            dd($ex);
            Log::error('Admin/InvoiceController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        try{
//            dd($request);
//            $validator = Validator::make($request->all(), [
//                'quotation_number'   => 'required',
//                'project_name'   => 'required',
//                'header_type'       => 'required'
//            ]);
//
//            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
//            $isAuthorization = 0;
//            $authorization = $request->input('authorization');
//            if(!empty($authorization)){
//                $isAuthorization = 1;
//            }
//
//            $amounts = $request->input('amounts');
//            $newAmounts = [];
//            foreach ($amounts as $amount){
//                if(!empty($amount)) {
//                    array_push($newAmounts, $amount);
//                }
//            }
//            if(count($newAmounts) == 0){
//                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
//            }
//
//            $modules = $request->input('modules');
//            $validFinish = true;
//            if(!empty($modules)){
//                $ct = 0;
//                foreach ($modules as $module){
//                    if(empty($module)) {
//                        $validFinish = false;
//                    }
//                    else{
//                        if(empty($newAmounts[$ct])) $validFinish = false;
//                    }
//                    $ct++;
//                }
//            }
//            if(!$validFinish){
//                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
//            }
//            $user = Auth::user();
//
//            $totalAmount = 0;
//            $i = 0;
//            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
//
//            $invoice = Invoice::create([
//                'number'            => $request->input('quotation_number'),
//                'project_name'      => $request->input('project_name'),
//                'client_id'         => $request->input('client'),
//                'date'              => $date->toDateTimeString(),
//                'header_type'       => $request->input('header_type'),
//                'authorization'     => $isAuthorization,
//                'created_at'        => Carbon::now('Asia/Jakarta'),
//                'created_by'        => $user->id
//            ]);
//
//            //Add Quotation Modules
//            foreach ($request->input('modules') as $module){
//                $dbModule = Module::find($module);
//                if($dbModule != null){
//                    InvoiceModule::create([
//                        'invoice_id'  => $invoice->id,
//                        'amount'        => $newAmounts[$i],
//                        'module_id'     => $module,
//                        'created_at'    => Carbon::now('Asia/Jakarta'),
//                        'created_by'    => $user->id
//                    ]);
//                    $totalAmount += $newAmounts[$i];
//                }
//                $i++;
//            }
//
//            $invoice->subtotal = $totalAmount;
//            $invoice->discount_1 = 0;
//            $invoice->discount_2 = 0;
//            $invoice->discount_3 = 0;
//            $invoice->discount_4 = 0;
//            $invoice->total_amount = $totalAmount;
//            if($request->input('discount_1') != null){
//                $totalAmount = $totalAmount - $request->input('discount_1');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_1 = $request->input('discount_1');
//            }
//            if($request->input('discount_2') != null){
//                $totalAmount = $totalAmount - $request->input('discount_2');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_2 = $request->input('discount_2');
//            }
//            if($request->input('discount_3') != null){
//                $totalAmount = $totalAmount - $request->input('discount_3');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_3 = $request->input('discount_3');
//            }
//            if($request->input('discount_4') != null){
//                $totalAmount = $totalAmount - $request->input('discount_4');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_4 = $request->input('discount_4');
//            }
//            $invoice->save();
//
//            //Add Module Details
//            foreach ($request->input('details') as $detail){
//                $dbDetail = ModuleDetail::find($detail);
//                if($dbDetail != null){
//                    InvoiceModuleDetail::create([
//                        'invoice_id'      => $invoice->id,
//                        'module_detail_id'  => $detail,
//                        'created_at'        => Carbon::now('Asia/Jakarta'),
//                        'created_by'        => $user->id
//                    ]);
//                }
//            }
//
//            Session::flash('success', 'Success Creating new Invoice!');
//            return redirect()->route('admin.invoices.index');
//        }
//        catch(\Exception $ex){
//            dd($ex);
//            Log::error('Admin/InvoiceController - store error EX: '. $ex);
//            return redirect()->back()->withErrors('Internal Server Error')->withInput();
//        }
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $module = Module::find($id);
            if(empty($module)){
                return redirect()->back();
            }

            $data = [
                'module'    => $module
            ];

            return view('admin.invoices.show')->with($data);
        }
        catch (\Exception $ex){
            Log::error('Admin/InvoiceController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        $invoice = Invoice::find($id);
//        $clients = ClientData::all();
//        $modules = Module::all();
//
//        //dd($invoice->modules);
//
//        $data = [
//            'invoice'         => $invoice,
//            'clients'           => $clients,
//            'modules'           => $modules,
//            'quotation_date'    => $invoice->date->format('d M Y')
//        ];
//
//        return view('admin.invoices.edit')->with($data);
//    }
    public function edit($id)
    {
        $invoice = Invoice::find($id);
        $invoiceModules = InvoiceModule::where('invoice_id', $invoice->id)->get();
        $invoiceModuleDetails = InvoiceModuleDetail::where('invoice_id', $invoice->id)->get();
        $invoiceModuleDiscounts = InvoiceDiscount::where('invoice_id', $invoice->id)->get();
        $clients = ClientData::all();
        $modules = Module::all();
        $latestModule = Module::latest()->first();

        $itemList = collect();
        $itemIdx = 1;
        foreach ($invoiceModules as $quotationModule){
            $detailModules = collect();
            foreach($invoiceModuleDetails as $quotationModuleDetail){
                $moduleDetailDB = ModuleDetail::find($quotationModuleDetail->module_detail_id);
                if($moduleDetailDB->module_id == $quotationModule->module_id){
                    $itemDetail = ([
                        'value'       => $moduleDetailDB->module_id."#".$moduleDetailDB->description,
                    ]);
                    $detailModules->push($itemDetail);
                }
            }

            $detailDBs = ModuleDetail::where('module_id', $quotationModule->module_id)->get();
            $details = collect();
            foreach ($detailDBs as $detail){
                $itemDetail = ([
                    'id' => $quotationModule->module_id."#".ucwords($detail->description),
                    'text' => $detail->description,
                ]);
                $details->push($itemDetail);
            }

            $module = ([
                'itemId'            => "item".$itemIdx,
                'price'             => $quotationModule->amount,
                'idModules'         => $quotationModule->module_id,
                'modules'           => $modules,
                'selectedModule'    => $quotationModule->module_id,
                'detailModules'     => $detailModules,
                'details'           => $details,
                'newDetailModules'  => ([
                    'value'             => "",
                ]),
            ]);
            $itemList->push($module);
            $itemIdx++;
        }
        $discountList = collect();
        foreach ($invoiceModuleDiscounts as $quotationModuleDiscount){

            $discount = ([
                'description'   => $quotationModuleDiscount->description,
                'amount'        => $quotationModuleDiscount->amount,
                'percentage'    => $quotationModuleDiscount->percentage,
            ]);
            $discountList->push($discount);
        }

        //dd($invoice->modules);

        $data = [
            'invoice'           => $invoice,
            'clients'           => $clients,
            'modules'           => $modules,
            'moduleJsons'       => json_encode($modules),
            'itemListJsons'     => json_encode($itemList),
            'discountListJsons' => json_encode($discountList),
            'latestModule'      => $latestModule->id,
            'quotation_date'    => $invoice->date->format('d M Y')
        ];
//        dd($itemList);

        return view('admin.invoices.edit-new')->with($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'invoice_number'   => 'required',
                'project_name'   => 'required',
                'header_type'       => 'required'
            ]);
//            dd($request);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $amounts = $request->input('prices');
            $modules = $request->input('modules');
            $details = $request->input('moduleDetails');

            $newModules = $request->input('new-modules');
            $newDetailModuleIds = $request->input('newModuleDetailIds');
            $newDetailModules = $request->input('new-module-details');
            $newAmounts = [];
            foreach ($amounts as $amount){
                if(!empty($amount)) {
                    array_push($newAmounts, $amount);
                }
            }
            if(count($newAmounts) == 0){
                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
            }

            $validFinish = true;
            if(!empty($modules)){
                $ct = 0;
                foreach ($modules as $module){
                    if(empty($module)) {
                        $validFinish = false;
                    }
                    else{
                        if(empty($newAmounts[$ct])) $validFinish = false;
                    }
                    $ct++;
                }
            }
            if(!$validFinish){
                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
            }
            $user = Auth::user();

            $totalAmount = 0;
            $i = 0;
            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');

            $invoice = Invoice::find($request->input('id'));
            $invoice->number = $request->input('invoice_number');
            $invoice->project_name = $request->input('project_name');
            $invoice->client_id = $request->input('client');
            $invoice->date = $date;
            $invoice->header_type = $request->input('header_type');

            if(empty($request->input('invoice_type'))){
                $invoice->invoice_type = "DOWN PAYMENT";
            }
            else{
                $invoice->invoice_type = $request->input('invoice_type');
            }
            if(empty($request->input('invoice_type_percent'))){
                $invoice->invoice_type_percent = 50;
            }
            else{
                $invoice->invoice_type_percent = $request->input('invoice_type_percent');
            }
            $invoice->updated_at = Carbon::now('Asia/Jakarta');
            $invoice->updated_by = $user->id;
            $invoice->save();

            //looping module
//            dd($request);
            $idxModule = 0;
            //delete existing data InvoiceModule
            $res = InvoiceModule::where('invoice_id',$invoice->id)->delete();
            //delete existing data InvoiceModuleDetail
            $res2 = InvoiceModuleDetail::where('invoice_id',$invoice->id)->delete();

            foreach ($modules as $module){

                // exist module and save to InvoiceModule
                $moduleId = $module;
                $dbModule = Module::find($module);
                if($dbModule != null){
                    InvoiceModule::create([
                        'invoice_id'  => $invoice->id,
                        'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                        'module_id'     => $module,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                    $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                }
                // create new module and save to InvoiceModule
                else{
                    if(!empty($newModules[$idxModule])){
                        $newModuleDB = Module::where('description', ucwords($newModules[$idxModule]))->first();
                        if(empty($newModuleDB)){
                            $moduleSubmit = Module::create([
                                'description'     => ucwords($newModules[$idxModule]),
                                'created_at'    => Carbon::now('Asia/Jakarta'),
                                'created_by'    => $user->id
                            ]);
                            $moduleId = $moduleSubmit->id;
                        }
                        else{
                            $moduleId = $newModuleDB->id;
                        }
                        InvoiceModule::create([
                            'invoice_id'  => $invoice->id,
                            'amount'        => (int)str_replace(",","",$newAmounts[$idxModule]),
                            'module_id'     => $moduleId,
                            'created_at'    => Carbon::now('Asia/Jakarta'),
                            'created_by'    => $user->id
                        ]);
                        $totalAmount += (int)str_replace(",","",$newAmounts[$idxModule]);
                    }
                }

                // detail module looping
                foreach ($details as $detail){
                    // existing detail module / not and save to InvoiceModuleDetail
                    if(strpos($detail, $module) !== false){
                        $detailArr = explode("#", $detail);
                        $moduleDetail = ModuleDetail::where('description', ucwords($detailArr[1]))->first();
                        $moduleDetailId = 0;
                        if(empty($moduleDetail)){
                            $newModuleDetail = ModuleDetail::create([
                                'module_id'          => $module,
                                'description'        => $detailArr[1],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetail->id;
                        }
                        else{
                            $moduleDetailId = $moduleDetail->id;
                        }
                        $isExistModuleDetail = InvoiceModuleDetail::where('invoice_id', $invoice->id)
                            ->where('module_detail_id', $moduleDetailId)
                            ->first();
                        if(empty($isExistModuleDetail)){
                            InvoiceModuleDetail::create([
                                'invoice_id'        => $invoice->id,
                                'module_detail_id'  => $moduleDetailId,
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                        }
                    }
                }

                //check if any new detail module inputed
                $idxDetail = 0;
                foreach ($newDetailModuleIds as $newDetailModuleId){
                    // existing detail module / not and save to InvoiceModuleDetail
                    $moduleDetailId = 0;
                    if($newDetailModuleId == $module){
                        if(!empty($newDetailModules[$idxDetail])){
                            $newModuleDetailSubmit = ModuleDetail::create([
                                'module_id'          => $moduleId,
                                'description'        => $newDetailModules[$idxDetail],
                                'created_at'        => Carbon::now('Asia/Jakarta'),
                                'created_by'        => $user->id
                            ]);
                            $moduleDetailId = $newModuleDetailSubmit->id;

                            $isExistModuleDetail = InvoiceModuleDetail::where('invoice_id', $invoice->id)
                                ->where('module_detail_id', $moduleDetailId)
                                ->first();
                            if(empty($isExistModuleDetail)){
                                InvoiceModuleDetail::create([
                                    'invoice_id'        => $invoice->id,
                                    'module_detail_id'  => $moduleDetailId,
                                    'created_at'        => Carbon::now('Asia/Jakarta'),
                                    'created_by'        => $user->id
                                ]);
                            }
                        }
                    }
                    $idxDetail++;
                }

                $idxModule++;
            }
            $invoice->subtotal = $totalAmount;
            $invoice->total_amount = $totalAmount;
            $invoice->save();

            //Add Module discount
            $discNames = $request->input('disc_name');
            $discAmounts = $request->input('disc_amount');
            $discPercents = $request->input('disc_percentage');
            $j = 0;
            if(count($discNames) > 0){
                $res = InvoiceDiscount::where('invoice_id',$invoice->id)->delete();
                foreach ($discNames as $discName){
                    if(!empty($discName)){
                        $newInvoiceDiscount = InvoiceDiscount::create([
                            'invoice_id'        => $invoice->id,
                            'description'       => $discName,
                            'amount'            => 0,
                            'percentage'        => 0,
                            'percentage_rupiah' => 0,
                            'created_at'        => Carbon::now('Asia/Jakarta'),
                            'created_by'        => $user->id
                        ]);
                        if(!empty($discAmounts[$j])){
                            $totalAmount = $totalAmount - (int)str_replace(",","",$discAmounts[$j]);
                            $invoice->total_amount = $totalAmount;
                            $invoice->save();

                            $newInvoiceDiscount->amount = (int)str_replace(",","",$discAmounts[$j]);
                            $newInvoiceDiscount->save();
                        }
                        if(!empty($discPercents[$j])){
                            $percentRupiah = ($totalAmount * $discPercents[$j]) / 100;
                            $totalAmount = $totalAmount - $percentRupiah;
                            $invoice->total_amount = $totalAmount;
                            $invoice->save();

                            $newInvoiceDiscount->percentage = $discPercents[$j];
                            $newInvoiceDiscount->percentage_rupiah = $percentRupiah;
                            $newInvoiceDiscount->save();
                        }
                    }
                    $j++;
                }
            }

            Session::flash('success', 'Success Changing Invoice Data!');
            return redirect()->route('admin.invoices.index');
        }
        catch(\Exception $ex){
            dd($ex);
            Log::error('Admin/QuotationController - update error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request)
//    {
////        dd($request);
//        try{
//            $validator = Validator::make($request->all(), [
//                'quotation_number'   => 'required',
//                'project_name'   => 'required',
//                'header_type'       => 'required'
//            ]);
//
//            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
//
//            $amounts = $request->input('amounts');
//            $newAmounts = [];
//            foreach ($amounts as $amount){
//                if(!empty($amount)) {
//                    array_push($newAmounts, $amount);
//                }
//            }
//            if(count($newAmounts) == 0){
//                return back()->withErrors("Harga yang belum terisi!")->withInput($request->all());
//            }
//
//            $modules = $request->input('modules');
//            $validFinish = true;
//            if(!empty($modules)){
//                $ct = 0;
//                foreach ($modules as $module){
//                    if(empty($module)) {
//                        $validFinish = false;
//                    }
//                    else{
//                        if(empty($newAmounts[$ct])) $validFinish = false;
//                    }
//                    $ct++;
//                }
//            }
//            if(!$validFinish){
//                return back()->withErrors("Terdapat module atau harga yang belum terpilih!")->withInput($request->all());
//            }
//            $user = Auth::user();
//
//            $totalAmount = 0;
//            $i = 0;
//            $date = Carbon::createFromFormat('d M Y', $request->input('date'), 'Asia/Jakarta');
//
//            $invoice = Invoice::find($request->input('id'));
//            $invoice->number = $request->input('quotation_number');
//            $invoice->project_name = $request->input('project_name');
//            $invoice->client_id = $request->input('client');
//            $invoice->date = $date;
//            $invoice->header_type = $request->input('header_type');
//            $invoice->updated_at = Carbon::now('Asia/Jakarta');
//            $invoice->updated_by = $user->id;
//            $invoice->save();
//
//            //Add Quotation Modules
//            InvoiceModule::where('invoice_id', $invoice->id)->delete();
//            foreach ($request->input('modules') as $module){
//                $dbModule = Module::find($module);
//                if($dbModule != null){
//                    InvoiceModule::create([
//                        'invoice_id'  => $invoice->id,
//                        'amount'        => $newAmounts[$i],
//                        'module_id'     => $module,
//                        'created_at'    => Carbon::now('Asia/Jakarta'),
//                        'created_by'    => $user->id
//                    ]);
//                    $totalAmount += $newAmounts[$i];
//                }
//                $i++;
//            }
//
//            $invoice->subtotal = $totalAmount;
//            $invoice->discount_1 = 0;
//            $invoice->discount_2 = 0;
//            $invoice->discount_3 = 0;
//            $invoice->discount_4 = 0;
//            $invoice->total_amount = $totalAmount;
//            if($request->input('discount_1') != null){
//                $totalAmount = $totalAmount - $request->input('discount_1');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_1 = $request->input('discount_1');
//            }
//            if($request->input('discount_2') != null){
//                $totalAmount = $totalAmount - $request->input('discount_2');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_2 = $request->input('discount_2');
//            }
//            if($request->input('discount_3') != null){
//                $totalAmount = $totalAmount - $request->input('discount_3');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_3 = $request->input('discount_3');
//            }
//            if($request->input('discount_4') != null){
//                $totalAmount = $totalAmount - $request->input('discount_4');
//                $invoice->total_amount = $totalAmount;
//                $invoice->discount_4 = $request->input('discount_4');
//            }
//            $invoice->save();
//
//            InvoiceModuleDetail::where('invoice_id', $invoice->id)->delete();
//            //Add Module Details
//            foreach ($request->input('details') as $detail){
//                $dbDetail = ModuleDetail::find($detail);
//                if($dbDetail != null){
//                    InvoiceModuleDetail::create([
//                        'invoice_id'      => $invoice->id,
//                        'module_detail_id'  => $detail,
//                        'created_at'        => Carbon::now('Asia/Jakarta'),
//                        'created_by'        => $user->id
//                    ]);
//                }
//            }
//
//            Session::flash('success', 'Success Changing Invoice Data!');
//            return redirect()->route('admin.invoices.index');
//        }
//        catch(\Exception $ex){
//            Log::error('Admin/InvoiceController - update error EX: '. $ex);
//            return redirect()->back()->withErrors('Internal Server Error')->withInput();
//        }
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $quotationId = $request->input('id');

            $invoice = Invoice::find($quotationId);

            if(empty($module)){
                return Response::json(array('errors' => 'INVALID'));
            }

            InvoiceModule::where('invoice_id', $invoice->id)->delete();
            InvoiceModuleDetail::where('invoice_id', $invoice->id)->delete();

            $quotNumber = $invoice->number;
            $invoice->delete();

            Session::flash('success', 'Success Deleting Quotation '.$quotNumber);
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            Log::error('Admin/InvoiceController - destroy - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }
}
