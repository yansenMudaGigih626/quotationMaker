<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Configuration;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Transformer\BannerTransformer;
use App\Transformer\ProductBrandTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class ConfigurationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function edit(){
        $configuration = Configuration::where('id', 1)->first();

        return view('admin.configuration.edit', compact('configuration'));
    }

    public function update(Request $request){
        try{
            $configuration = Configuration::where('id', 1)->first();
            $configuration->pt_name = $request->input('pt_name');
            $configuration->pt_address = $request->input('pt_address');
            $configuration->sign_name = $request->input('sign_name');
            $configuration->sign_position = $request->input('sign_position');
            $configuration->acc_bank = $request->input('acc_bank');
            $configuration->acc_no = $request->input('acc_no');
            $configuration->acc_holder = $request->input('acc_holder');
            $configuration->save();

            Session::flash('success', 'Sukses mengubah data configuration!');
            return redirect()->route('admin.configuration.edit');
        }
        catch(\Exception $ex){
            Log::error('Admin/ConfigurationController - update error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

}
