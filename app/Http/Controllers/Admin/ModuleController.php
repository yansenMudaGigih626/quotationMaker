<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\ModuleDetail;
use App\Transformer\ModuleTransformer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $modules = Module::all();
        return DataTables::of($modules)
            ->setTransformer(new ModuleTransformer)
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.modules.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'description'   => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $user = Auth::user();

            $moduleHeader = Module::create([
                'description'   => $request->input('description'),
                'created_at'    => Carbon::now('Asia/Jakarta'),
                'created_by'    => $user->id
            ]);

            //Add Module Details
            foreach ($request->input('details') as $detail){
                if(!$detail == ''){
                    //If not Empty
                    ModuleDetail::create([
                        'module_id' => $moduleHeader->id,
                        'description'   => $detail,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                }
            }

            Session::flash('success', 'Sukses membuat module Baru');
            return redirect()->route('admin.modules.index');
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $module = Module::find($id);
            if(empty($module)){
                return redirect()->back();
            }

            $data = [
                'module'    => $module
            ];

            return view('admin.modules.show')->with($data);
        }
        catch (\Exception $ex){
            Log::error('Admin/ModuleController - store error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);
        $details = json_encode($module->module_details);
        $data = [
            'module'    => $module,
            'details'   => $details
        ];

        return view('admin.modules.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'description'   => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $user = Auth::user();

            $moduleHeader = Module::find($request->input('id'));
            $moduleHeader->description = $request->input('description');
            $moduleHeader->save();

            //Add Module Details
            $i = 0;
            $ids = $request->input('ids');
            foreach ($request->input('details') as $detail){
                $detailData = ModuleDetail::find($ids[$i]);
                if($detailData != null){
                    $detailData->description = $detail;
                    $detailData->save();
                }
                else if(!$detail == ''){
                    //If not Empty
                    ModuleDetail::create([
                        'module_id'     => $moduleHeader->id,
                        'description'   => $detail,
                        'created_at'    => Carbon::now('Asia/Jakarta'),
                        'created_by'    => $user->id
                    ]);
                }
                $i++;
            }

            Session::flash('success', 'Sukses mengubah data Module!');
            return redirect()->route('admin.modules.index');
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - update error EX: '. $ex);
            return redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $moduleId = $request->input('id');

            $module = Module::find($moduleId);

            if(empty($module)){
                return Response::json(array('errors' => 'INVALID'));
            }

            //Check Details
            $details = ModuleDetail::where('module_id', $module->id)->get();
            if($details != null){
                ModuleDetail::where('module_id', $module->id)->delete();
            }

            // Check orders
//            if(DB::table('quotation_details')->where('module_id', $moduleId)
//                ->exists()){
//                return Response::json(array('errors' => 'INVALID'));
//            }
//
//            if(DB::table('invoice_details')->where('module_id', $moduleId)
//                ->exists()){
//                return Response::json(array('errors' => 'INVALID'));
//            }

            $module->delete();

            Session::flash('success', 'Sukses menghapus data Module ');
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - destroy - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }
    public function getModules(Request $request){
        try{
            $term = trim($request->q);
            Log::info('Admin/ModuleController - check 1 term = '. $term);
            $roles = Module::where(function ($q) use ($term) {
                $q->where('description', 'LIKE', '%' . $term . '%');
            })->get();

            $formatted_tags = [];

            foreach ($roles as $role) {
                $formatted_tags[] = ['id' => ucwords($role->description), 'text' => $role->description];
            }

            return \Response::json($formatted_tags);
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - getModules - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }
    public function getModuleDetails(Request $request){
        try{
//            $term = trim($request->keyword);
            $headerId = $request->header_id;
            Log::info('Admin/ModuleController getModuleDetails - check 1 term = '. $headerId);
            $roles = ModuleDetail::where(function ($q) use ($headerId) {
//                $q->where('description', 'LIKE', '%' . $term . '%')
                $q->where('module_id', $headerId);
            })->get();

            $formatted_tags = [];

            foreach ($roles as $role) {
                $formatted_tags[] = ['id' => $headerId."#".ucwords($role->description), 'text' => $role->description];
            }

            return \Response::json($formatted_tags);
        }
        catch(\Exception $ex){
            Log::error('Admin/ModuleController - getModuleDetails - error EX: '. $ex);
            return Response::json(array('errors' => 'INVALID ' . $ex));
        }
    }
}
