<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\libs\Utilities;
use App\Models\Address;
use App\Models\AdminUser;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\ProductUserCategory;
use App\Models\User;
use App\Notifications\FCMNotification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{/**
 * Function to Get Cart Data.
 *
 * @return JsonResponse
 */
    public function getCartAmount()
    {
        try{
            $user = auth('api')->user();
            $cartCount = DB::table('carts')
                ->where('user_id', $user->id)
                ->count();

            return Response::json([
                'message'   => 'SUCCESS',
                'model'     => $cartCount
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - getCartAmount error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    /**
     * Function to Get Cart Data.
     *
     * @return JsonResponse
     */
    public function getCart()
    {
        try{
            $user = auth('api')->user();
            $carts = Cart::with('product')->where('user_id', $user->id)->get();

            if($carts->count() === 0){
                return Response::json([
                    'message'       => 'EMPTY',
                    'model'         => ''
                ], 482);
            }

            $cartModels = collect();
            foreach ($carts as $cart){
                $productImage = $cart->product->product_images->where('is_main_image', 1)->first();
                if(!empty($productImage->path)){
                    $url = asset('storage/products/'. $productImage->path);
                }
                else{
                    $url = asset('images/yifang/no-image.png');
                }

                $cartModel = collect([
                    'id'            => $cart->id,
                    'product_id'    => $cart->product_id,
                    'product_name'  => $cart->product->name,
                    'user_id'       => $cart->user_id,
                    'notes'         => $cart->notes,
                    'qty'           => $cart->qty,
                    'price'         => $cart->price,
                    'price_msrp'    => $cart->product->price,
                    'subtotal'      => $cart->subtotal,
                    'image_path'    => $url
                ]);
                $cartModels->push($cartModel);
            }

            return Response::json([
                'message'   => 'SUCCESS',
                'model'     => json_encode($cartModels)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - getCart error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    /**
     * Function to Add item or Update Cart.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addToCart(Request $request)
    {
        try{
            if(empty($request->input('product_id'))){
                return Response::json([
                    'message'       => 'BAD REQUEST',
                    'model'         => ''
                ], 400);
            }

            $user = auth('api')->user();
            $product = Product::find($request->input('product_id'));
            $totalPrice = $product->price * $request->input('qty');

            Log::info('user '. $user->email. ' menambahkan keranjang produk '. $product->sku. ' sebanyak '. $request->input('qty'). ' unit');

            $cart = Cart::where('user_id', $user->id)->where('product_id', $product->id)->first();
            if(!empty($cart)){
                $totalPriceTmp = $cart->subtotal;
                $totalPrice += $totalPriceTmp;
                $cart->qty += $request->input('qty');
                $cart->subtotal = $totalPrice;
                //$cart->notes = $request->input('notes') ?? "";
                $cart->save();
            }
            else{
                $productUserCategory = ProductUserCategory::where('product_id', $product->id)
                    ->where('user_category_id', $user->category_id)
                    ->where('shipping_method', 'DARAT')
                    ->first();

                Cart::create([
                    'user_id'       => $user->id,
                    'product_id'    => $product->id,
                    'notes'         => $request->input('notes') ?? '',
                    'qty'           => $request->input('qty'),
                    'price'         => $productUserCategory->price,
                    'subtotal'      => $totalPrice,
                ]);
            }

            return Response::json([
                'message'   => 'SUCCESS',
                'model'     => ''
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - addToCart error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    /**
     * Function to Update item in Cart.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateCart(Request $request)
    {
        try{

//            if(empty($request->input('cart_id')) || empty($request->input('qty'))){
//                return Response::json([
//                    'message'       => 'BAD REQUEST',
//                    'model'         => ''
//                ], 400);
//            }

            Log::info($request->input('json_string'));

            $cartJsons = json_decode($request->input('json_string'));

            $user = auth('api')->user();
            //$qtyInt = intval($request->input('qty'));

//            $cart = Cart::find($request->input('cart_id'));
//            if(empty($cart) === 0){
//                return Response::json([
//                    'message'       => 'BAD REQUEST',
//                    'model'         => ''
//                ], 400);
//            }
//            $totalPrice = $cart->product->price * $qtyInt;
//
//            $cart->subtotal = $totalPrice;
//            $cart->qty = $request->input('qty');
//            $cart->save();

            foreach ($cartJsons as $cartJson){
                $cart = Cart::where('user_id', $cartJson->user_id)
                    ->where('product_id', $cartJson->product_id)
                    ->first();

                if(!empty($cart)){
                    $productUserCategory = ProductUserCategory::where('product_id', $cart->product_id)
                        ->where('user_category_id', $user->category_id)
                        ->where('shipping_method', 'DARAT')
                        ->first();

                    $subtotal = intval($cartJson->qty) * $productUserCategory->price;

                    if(!empty($cartJson->notes)){
                        if($cart->notes !== $cartJson->notes){
                            Log::info('user '. $user->email. ' mengubah data notes produk '. $cart->product->sku. ' jadi '. $cartJson->notes);
                        }
                    }

                    if($cart->qty !== $cartJson->qty){
                        Log::info('user '. $user->email. ' mengubah data kuantitas produk '. $cart->product->sku. ' jadi '. $cartJson->qty);
                    }

                    $cart->qty = $cartJson->qty;
                    if(!empty($cartJson->notes)){
                        $cart->notes = $cartJson->notes;
                    }

                    $cart->subtotal = $subtotal;
                    $cart->save();
                }
            }

            return Response::json([
                'message'   => 'SUCCESS',
                'model'     => ''
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - updateCart error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    /**
     * Function to Add item or Update Cart.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteCart(Request $request)
    {
        try{
            if(empty($request->input('cart_id'))){
                return Response::json([
                    'message'       => 'BAD REQUEST',
                    'model'         => ''
                ], 400);
            }

            $cart = Cart::find($request->input('cart_id'));
            if(empty($cart)){
                return Response::json([
                    'message'       => 'BAD REQUEST',
                    'model'         => ''
                ], 400);
            }

            $user = auth('api')->user();
            Log::info('user '. $user->email. ' menghapus keranjang produk '. $cart->product->sku);

            $cart->delete();

            return Response::json([
                'message'   => 'SUCCESS',
                'model'     => ''
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - deleteCart error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    public function getShippingPrices(Request $request){
        try{
            $user = auth('api')->user();

            $carts = Cart::with(['user', 'product'])
                ->where('user_id', $user->id)
                ->get();

            $totalPrice = 0;
            $totalPriceAir = 0;
            $cartModels = collect();
            foreach ($carts as $cart){
                $totalPrice += $cart->subtotal;
                $productImage = $cart->product->product_images->where('is_main_image', 1)->first();

                if(!empty($productImage->path)){
                    $url = asset('storage/products/'. $productImage->path);
                }
                else{
                    $url = asset('images/yifang/no-image.png');
                }

                $cartModel = collect([
                    'id'            => $cart->id,
                    'product_id'    => $cart->product_id,
                    'product_name'  => $cart->product->name,
                    'user_id'       => $cart->user_id,
                    'notes'         => $cart->notes,
                    'qty'           => $cart->qty,
                    'price'         => $cart->price,
                    'price_msrp'    => $cart->product->price,
                    'subtotal'      => $cart->subtotal,
                    'image_path'    => $url
                ]);
                $cartModels->push($cartModel);

                // Get air shipping type
                $productUserCategory = ProductUserCategory::where('product_id', $cart->product_id)
                    ->where('user_category_id', $cart->user->category_id)
                    ->where('shipping_method', 'UDARA')
                    ->first();
                $totalPriceAir += ($productUserCategory->price * $cart->qty);
            }

            $address = Address::where('user_id', $user->id)->first();

            //Log::info($totalPriceAir. ' - '. $totalPrice);

            $cartCheckoutModel = collect([
                'name'                  => $user->name,
                'address'               => $address->description. ', '. $address->postal_code,
                'price_total'           => $totalPrice,
                'price_additional_air'  => $totalPriceAir - $totalPrice,
                'cart_models'           => $cartModels
            ]);

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($cartCheckoutModel)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - getShippingPrices error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    public function checkOutCart(Request $request){
        try{
            $user = auth('api')->user();
            $now = Carbon::now('Asia/Jakarta');

            $carts = Cart::where('user_id', $user->id)->get();

            if($carts->count() === 0){
                return Response::json([
                    'message'       => 'EMPTY',
                    'model'         => ''
                ], 482);
            }

            $shippingId = intval($request->input('shipping_id'));

            // Generate auto number
            $prepend = 'OR/'. $now->year;
            $nextNo = Utilities::GetNextTransactionNumber($prepend);
            $orCode = Utilities::GenerateTransactionNumber($prepend, $nextNo);

            $totalPrice = 0;
            $stringTmp = array();
            $isInStock = true;
            foreach ($carts as $cart){

                // Validate stock
                if($cart->qty > $cart->product->in_stock){
                    $isInStock = false;

                    $tmp = $cart->product->name;
                    array_push($stringTmp, $tmp);
                }

                if($isInStock){
                    if($shippingId === 1){
                        $totalPrice += $cart->subtotal;
                    }
                    else{
                        // Get air shipping type
                        $productUserCategory = ProductUserCategory::where('product_id', $cart->product_id)
                            ->where('user_category_id', $cart->user->category_id)
                            ->where('shipping_method', 'UDARA')
                            ->first();
                        $totalPrice += ($cart->qty * $productUserCategory->price);
                    }
                }
            }

            if(!$isInStock){
                return Response::json($stringTmp, 483);
            }

            $newOrder = Order::create([
                'order_number'      => $orCode,
                'internal_code'     => 'DEFAULT',
                'user_id'           => $user->id,
                'date'              => $now->toDateTimeString(),
                'user_address'      => $user->addresses->first()->description,
                'user_phone'        => $user->phone,
                'province_id'       => $user->addresses->first()->province_id ?? null,
                'total_price'       => $totalPrice,
                'grand_total'       => $totalPrice,
                'shipping_method'   => $shippingId === 1 ? 'DARAT' : 'UDARA',
                'delivery_method'   => $shippingId === 1 ? 'DARAT' : 'UDARA',
                'status_id'         => 4,
                'created_at'        => $now->toDateTimeString(),
                'updated_at'        => $now->toDateTimeString()
            ]);

            foreach ($carts as $cart){
                $price = $cart->price;
                if($shippingId === 2){
                    // Get air shipping type
                    $productUserCategory = ProductUserCategory::where('product_id', $cart->product_id)
                        ->where('user_category_id', $cart->user->category_id)
                        ->where('shipping_method', 'UDARA')
                        ->first();
                    $price = $productUserCategory->price;
                }

                $subtotal = $price * $cart->qty;

                OrderProduct::create([
                    'order_id'              => $newOrder->id,
                    'product_id'            => $cart->product_id,
                    'product_name'          => $cart->product->name,
                    'qty'                   => $cart->qty,
                    'price'                 => $price,
                    'subtotal'              => $subtotal,
                    'notes'                 => $cart->notes ?? ''
                ]);

                // Decrease product stock
                $product = $cart->product;
                $product->in_stock -= $cart->qty;
                $product->save();
            }

            Utilities::UpdateTransactionNumber($prepend);

            // Delete cart
            foreach ($carts as $cart){
                $cart->delete();
            }

            return Response::json([
                'message'   => 'SUCCESS'
            ],200);
        }
        catch (\Exception $ex){
            Log::error('Api/CartController - checkOutCart error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }
}
