<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class ProductCategoryController extends Controller
{
    public function getHomeBrandAndCategory(Request $request){
        try{
            $brands = ProductBrand::all();
            $productCategories = ProductCategory::all();
            $lang = $request->input('lang');

            $brandModels = collect();
            foreach($brands as $brand){
                $brandModel = collect([
                    'id'            => $brand->id,
                    'name'          => $brand->name,
                    'image_path'    => asset('storage/product_brands/'. $brand->image_path)
                ]);
                $brandModels->push($brandModel);
            }

            $categoryModels = collect();
            foreach($productCategories as $productCategory){
                $translation = $productCategory->product_category_translations->where('language_code', $lang)->first();
                $categoryModel = collect([
                    'id'            => $productCategory->id,
                    'name'          => $translation->name
                ]);
                $categoryModels->push($categoryModel);
            }

            $responseModel = collect([
                'brands'                => $brandModels,
                'product_categories'    => $categoryModels
            ]);

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($responseModel)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/ProductCategoryController - getHomeBrandAndCategory error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }
}
