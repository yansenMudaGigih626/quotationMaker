<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\FcmTokenApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class UserManagementController extends Controller
{
    public function logout(Request $request){
        try{

            Log::info($request->getContent());

            //$token = $request->input('fmc_token');
            $json = json_decode($request->getContent());
            $token = $json->fcm_token;
            $user = auth('api')->user();

            if(empty($user)){
                return Response::json('INVALID', 400);
            }

            $fcmToken = FcmTokenApp::where('user_id', $user->id)
                ->where('token', $token)
                ->first();

            if(!empty($fcmToken)){
                $fcmToken->delete();
            }
            else{
                Log::info('TOKEN NOT FOUND: '. $token);
            }

            return Response::json('SUCCESS', 200);
        }
        catch(\Exception $ex){
            Log::error('Api/UserManagementController - logout error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }
}
