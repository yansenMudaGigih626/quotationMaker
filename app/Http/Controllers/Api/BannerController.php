<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class BannerController extends Controller
{
    public function getHomeBanner(){
        try{
            $banners = Banner::where('status_id', 1)
                ->orderBy('name')
                ->get();

            if($banners->count() === 0){
                return Response::json([
                    'message'       => 'EMPTY',
                    'model'         => ''
                ], 482);
            }

            $bannerModels = collect();
            foreach($banners as $banner){
                if(!empty($banner->url) && empty($banner->product_id) && empty($banner->brand_id)){
                    $mode = 'link';
                }
                elseif (empty($banner->url) && !empty($banner->product_id) && empty($banner->brand_id)){
                    $mode = 'product';
                }
                elseif (empty($banner->url) && empty($banner->product_id) && !empty($banner->brand_id)){
                    $mode = 'brand';
                }

                $bannerModel = collect([
                    'id'                => $banner->id,
                    'name'              => $banner->name,
                    'mode'              => $mode,
                    'alt_text'          => $banner->alt_text,
                    'url'               => $banner->url,
                    'product_id'        => $banner->product_id,
                    'brand_id'          => $banner->brand_id,
                    'image_path'        => asset('storage/banners/'. $banner->image_path)
                ]);

                $bannerModels->push($bannerModel);
            }

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($bannerModels)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/BannerController - show error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }
}
