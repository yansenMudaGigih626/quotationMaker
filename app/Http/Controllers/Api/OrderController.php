<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AdminUser;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use App\Notifications\FCMNotification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    /**
     * Function to get the Order Request Data Details.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTransactionData(Request $request)
    {
        try{
            $order = Order::where('order_number', $request->input('order_number'))->with('order_products')->first();
            $date = Carbon::parse($order->date, 'Asia/Jakarta');

            $orderDetailModels = collect();
            foreach ($order->order_products as $detail){
                $productImages = ProductImage::where('product_id', $detail->product_id)
                    ->orderBy('is_main_image', 'desc')
                    ->get();

                $imageArr = array();
                foreach($productImages as $image){
                    array_push($imageArr, asset('storage/products/'. $image->path));
                }

                $orderDetailModel = collect([
                    'id'                => $detail->id,
                    'product_id'        => $detail->product_id,
                    'product_name'      => $detail->product_name,
                    'qty'               => $detail->qty,
                    'image_paths'       => $imageArr,
                    'price'             => $detail->price,
                    'subtotal'          => $detail->subtotal
                ]);

                $orderDetailModels->push($orderDetailModel);
            }

            $processingDate = '';
            if(!empty($order->processed_date)){
                $processingDate = Carbon::parse($order->processed_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
            }

            $shippingDate = '';
            if(!empty($order->processed_date)){
                $shippingDate = Carbon::parse($order->shipping_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
            }

            $finishDate = '';
            if(!empty($order->finish_date)){
                $finishDate = Carbon::parse($order->shipping_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
            }

            $rejectDate = '';
            if(!empty($order->reject_date)){
                $rejectDate = Carbon::parse($order->reject_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
            }

            $orderModel = collect([
                'id'                => $order->id,
                'code'              => $order->order_number,
                'date'              => $date->format('d-m-Y H:i:s'),
                'processing_date'   => $processingDate,
                'reject_date'       => $rejectDate,
                'shipping_date'     => $shippingDate,
                'finish_date'       => $finishDate,
                'shipping'          => $order->shipping_method === 'DARAT' ? 1 : 2,
                'total_price'       => $order->total_price,
                'total_discount'    => $order->total_discount,
                'grand_total'       => $order->grand_total,
                'status_id'         => $order->status_id,
                'order_details'     => $orderDetailModels
            ]);

            return Response::json([
                'message'   => 'SUCCESS',
                'model'    => json_encode($orderModel)
            ], 200);
        }
        catch(\Exception $ex){
            Log::error('Api/OrderController - getTransactionData error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    /**
     * Function to get all the Order Request.
     *
     * @return JsonResponse
     */
    public function getTransactions(Request $request)
    {
        try{
            $user = auth('api')->user();
            $skip = intval($request->input('skip'));
            $statusId = intval($request->input('order_status'));
            $orderingType = $request->input('ordering_type');

            Log::info('skip: '. $skip);
            Log::info('order_status: '. $statusId);
            Log::info('ordering_type: '. $orderingType);

            $orders = Order::where('user_id', $user->id);

            // Filter date
            if(!empty($request->input('date_start') && !empty($request->input('date_end')))){
                $dateStart = Carbon::createFromFormat('d-m-Y', $request->input('date_start'), 'Asia/Jakarta');
                $dateEnd =  Carbon::createFromFormat('d-m-Y', $request->input('date_end'), 'Asia/Jakarta');

                $dateStart->subDays(1);
                $dateEnd->addDays(1);

                $orders = $orders->whereBetween('date', array($dateStart->toDateTimeString(), $dateEnd->toDateTimeString()));
            }

            // 0 default, get all order history
            if($statusId !== 0) {
                $orders = $orders->where('status_id', $statusId);
            }

            if($orders->count() === 0){
                return Response::json([
                    'message'       => 'EMPTY',
                    'model'         => ''
                ], 482);
            }

            $orders = $orders
                ->orderBy('date', $orderingType)
                ->skip($skip)
                ->limit(10)
                ->get();

            $orderModels = collect();
            foreach ($orders as $order){
                $date = Carbon::parse($order->date, 'Asia/Jakarta');

                $orderDetailModels = collect();
                foreach ($order->order_products as $detail){
                    $productImages = ProductImage::where('product_id', $detail->product_id)
                        ->orderBy('is_main_image', 'desc')
                        ->get();

                    $imageArr = array();
                    foreach($productImages as $image){
                        array_push($imageArr, asset('storage/products/'. $image->path));
                    }

                    $orderDetailModel = collect([
                        'id'                => $detail->id,
                        'product_id'        => $detail->product_id,
                        'product_name'      => $detail->product_name,
                        'image_paths'       => $imageArr,
                        'qty'               => $detail->qty,
                        'price'             => $detail->price,
                        'subtotal'          => $detail->subtotal
                    ]);

                    Log::info($detail->product_name);

                    $orderDetailModels->push($orderDetailModel);
                }

                $processingDate = '';
                if(!empty($order->processed_date)){
                    $processingDate = Carbon::parse($order->processed_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
                }

                $shippingDate = '';
                if(!empty($order->processed_date)){
                    $shippingDate = Carbon::parse($order->shipping_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
                }

                $finishDate = '';
                if(!empty($order->finish_date)){
                    $finishDate = Carbon::parse($order->shipping_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
                }

                $rejectDate = '';
                if(!empty($order->reject_date)){
                    $rejectDate = Carbon::parse($order->reject_date, 'Asia/Jakarta')->format('d-m-Y H:i:s');
                }

                $orderModel = collect([
                    'id'                => $order->id,
                    'code'              => $order->order_number,
                    'date'              => $date->format('d-m-Y H:i:s'),
                    'processing_date'   => $processingDate,
                    'reject_date'       => $rejectDate,
                    'shipping_date'     => $shippingDate,
                    'finish_date'       => $finishDate,
                    'shipping'          => $order->shipping_method === 'DARAT' ? 1 : 2,
                    'total_price'       => $order->total_price,
                    'total_discount'    => $order->total_discount,
                    'grand_total'       => $order->grand_total,
                    'status_id'         => $order->status_id,
                    'order_details'     => $orderDetailModels
                ]);

                $orderModels->push($orderModel);
            }

            //Log::info(json_encode($orders));

            return Response::json([
                'message'   => 'success',
                'model'    => json_encode($orderModels)
            ], 200);
        }
        catch(\Exception $ex){
            Log::error('Api/OrderController - getTransactions error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
//        return Response::json(array('data' => $transactions));
    }
}
