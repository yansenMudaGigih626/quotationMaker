<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductUserCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    public function getAllProduct(Request $request){
        try{
            $brand = intval($request->input('brand_id'));
            $startPrice = floatval($request->input('price_start'));
            $endPrice = floatval($request->input('price_end'));
            $orderingField = $request->input('ordering_field');
            $orderingType = $request->input('ordering_type');
            $skip = intval($request->input('skip'));
            $keyword = $request->input('keyword');
            $lang = $request->input('lang');

//            Log::info('start_price: '. $startPrice. ' & end_price: '. $endPrice);
//            Log::info('order_field: '. $orderingField. ' & order_type '. $orderingType);
//            Log::info('keyword: '. $keyword);

            $user = Auth::guard('api')->user();

            $productModels = collect();
            if(empty($user)){
                $products = Product::with('product_images')
                    ->where('in_stock', '>', 0 )
                    ->where('status_id', 1);

                if($brand !== 0){
                    $products = $products->where('brand_id', $brand);
                }

                if($startPrice < $endPrice){

                    $products = $products->whereBetween('price', array($startPrice, $endPrice));
                }

                if(!empty($keyword)){
                    $products = $products->where('name', 'LIKE', '%'. $keyword. '%');
                }

                if($products->count() === 0){
                    return Response::json([
                        'message'       => 'EMPTY',
                        'model'         => ''
                    ], 482);
                }

                $products = $products->orderBy($orderingField, $orderingType)
                    ->skip($skip)
                    ->limit(15)
                    ->get();


                foreach ($products as $product){
                    $productImage = $product->product_images->where('is_main_image', 1)->first();
                    $translationProductCategory = $product->product_category->product_category_translations->where('language_code', $lang)->first();

                    if(!empty($productImage->path)){
                        $url = asset('storage/products/'. $productImage->path);
                    }
                    else{
                        $url = asset('images/yifang/no-image.png');
                    }

                    $productModel = collect([
                        'id'                => $product->id,
                        'name'              => $product->name,
                        'sku'               => $product->sku,
                        'category_id'       => $product->category_id,
                        'category_name'     => $translationProductCategory->name,
                        'brand_id'          => $product->brand_id,
                        'brand_name'        => $product->product_brand->name,
                        'image_path'        => $url,
                        'external_link'     => $product->external_link ?? '',
                        'price'             => $product->price,
                        'price_msrp'        => $product->price,
                        'description'       => $product->meta_description ?? '',
                    ]);

                    $productModels->push($productModel);
                }
            }
            else{
                $productUserCategories = ProductUserCategory::with('product')
                    ->join('products','products.id', '=', 'product_user_categories.product_id')
                    ->select('product_user_categories.*', 'products.name', 'products.status_id', 'products.brand_id')
                    ->where('product_user_categories.user_category_id', $user->category_id)
                    ->where('product_user_categories.shipping_method', '=', 'DARAT')
                    ->where('products.status_id', '=', 1)
                    ->where('products.in_stock', '>', 0);

                if($brand !== 0){
//                    $productUserCategories = $productUserCategories->whereHas('product', function($query) use($brand){
//                        $query->where('brand_id', $brand);
//                    });
                    $productUserCategories = $productUserCategories->where('products.brand_id', '=', $brand);
                }

                if($startPrice < $endPrice){
                    $productUserCategories = $productUserCategories->whereBetween('product_user_categories.price', array($startPrice, $endPrice));
                }

                if(!empty($keyword)){
//                    $productUserCategories = $productUserCategories->whereHas('product', function($query) use($keyword){
//                        $query->where('name', 'ilike', '%'. $keyword. '%');
//                    });
                    $productUserCategories = $productUserCategories->where('products.name', 'LIKE', '%'. $keyword. '%');
                }

                if($productUserCategories->count() === 0){
                    return Response::json([
                        'message'       => 'EMPTY',
                        'model'         => ''
                    ], 482);
                }

                if($orderingField === 'name'){
//                    $productUserCategories = $productUserCategories->whereHas('product', function($query) use($orderingField, $orderingType){
//                        $query->orderBy($orderingField, $orderingType);
//                    })
//                        ->skip($skip)
//                        ->limit(5)
//                        ->get();
                    $productUserCategories = $productUserCategories->orderBy('products.'. $orderingField, $orderingType)
                        ->skip($skip)
                        ->limit(5)
                        ->get();
                }
                else{
                    $productUserCategories = $productUserCategories->orderBy('product_user_categories.'. $orderingField, $orderingType)
                        ->skip($skip)
                        ->limit(5)
                        ->get();
                }

                Log::info('Found '. $productUserCategories->count());

                foreach ($productUserCategories as $productUserCategory){
                    $productImage = $productUserCategory->product->product_images->where('is_main_image', 1)->first();
                    $translationProductCategory = $productUserCategory->product->product_category->product_category_translations->where('language_code', $lang)->first();

                    if(!empty($productImage->path)){
                        $url = asset('storage/products/'. $productImage->path);
                    }
                    else{
                        $url = asset('images/yifang/no-image.png');
                    }

                    $productModel = collect([
                        'id'                => $productUserCategory->product_id,
                        'name'              => $productUserCategory->product->name,
                        'sku'               => $productUserCategory->product->sku,
                        'category_id'       => $productUserCategory->product->category_id,
                        'category_name'     => $translationProductCategory->name,
                        'brand_id'          => $productUserCategory->product->brand_id,
                        'brand_name'        => $productUserCategory->product->product_brand->name,
                        'image_path'        => $url,
                        'external_link'     => $productUserCategory->product->external_link ?? '',
                        'price'             => $productUserCategory->price,
                        'price_msrp'        => $productUserCategory->product->price,
                        'description'       => $productUserCategory->product->meta_description ?? ''
                    ]);

                    $productModels->push($productModel);
                }
            }

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($productModels)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/ProductController - getAllProducts error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    public function show(Request $request){
        try{
            $productId = intval($request->input('product_id'));
            $lang = $request->input('lang');

            $product = Product::find($productId);
            if(empty($product)){
                return Response::json([
                    'message'       => 'BAD REQUEST',
                    'model'         => ''
                ], 400);
            }

//            $productImages = $product->product_images;
//            $mainImage = $productImages->where('is_main_image', 1)->first();
//            $otherImages = $productImages->where('is_main_image', 0)->get();

            $mainImage = ProductImage::where('product_id', $productId)->where('is_main_image', 1)->first();
            $otherImages = ProductImage::where('product_id', $productId)->where('is_main_image', 0)->get();

            $imageArr = array();

            if(!empty($mainImage->path)){
                $mainUrl = asset('storage/products/'. $mainImage->path);
            }
            else{
                $mainUrl = asset('images/yifang/no-image.png');
            }

            array_push($imageArr, $mainUrl);
            foreach($otherImages as $image){
                array_push($imageArr, asset('storage/products/'. $image->path));
            }

            // Get price
            $user = Auth::guard('api')->user();
            if(!empty($user)){
                $productUserCategory = ProductUserCategory::where('product_id', $productId)
                    ->where('user_category_id', $user->category_id)
                    ->where('shipping_method', 'DARAT')
                    ->first();

                $price = $productUserCategory->price;
            }
            else{
                $price = $product->price;
            }

            $translationProductCategory = $product->product_category->product_category_translations->where('language_code', $lang)->first();

            if(!empty($mainImage->path)){
                $url = asset('storage/products/'. $mainImage->path);
            }
            else{
                $url = asset('images/yifang/no-image.png');
            }

            $productModel = collect([
                'id'                => $productId,
                'name'              => $product->name,
                'sku'               => $product->sku,
                'category_id'       => $product->category_id,
                'category_name'     => $translationProductCategory->name,
                'brand_id'          => $product->brand_id,
                'brand_name'        => $product->product_brand->name,
                'image_path'        => $url,
                'image_path_others' => $imageArr,
                'external_link'     => $product->external_link ?? '',
                'price'             => $price,
                'price_msrp'        => $product->price,
                'weight'            => $product->weight ?? 0,
                'length'            => $product->length ?? 0,
                'width'             => $product->width ?? 0,
                'height'            => $product->height ?? 0,
                'description'       => $product->meta_description ?? '',
                'status_id'         => $product->status_id
            ]);

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($productModel)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/ProductController - show error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

    public function getHomeProduct(Request $request){
        try{
            $lang = $request->input('lang');

            $randomProducts = Product::where('status_id', 1)
                ->where('in_stock', '>', 0)
                ->inRandomOrder()
                ->limit(4)
                ->get();

            $user = Auth::guard('api')->user();

            $productModels = collect();
            foreach ($randomProducts as $product){
                if(!empty($user)){
                    $productUserCategory = ProductUserCategory::where('product_id', $product->id)
                        ->where('user_category_id', $user->category_id)
                        ->where('shipping_method', 'DARAT')
                        ->first();

                    $price = $productUserCategory->price;
                }
                else{
                    $price = $product->price;
                }

                $productImage = $product->product_images->where('is_main_image', 1)->first();
                $translationProductCategory = $product->product_category->product_category_translations->where('language_code', $lang)->first();

                if(!empty($productImage->path)){
                    $url = asset('storage/products/'. $productImage->path);
                }
                else{
                    $url = asset('images/yifang/no-image.png');
                }

                $productModel = collect([
                    'id'                => $product->id,
                    'name'              => $product->name,
                    'sku'               => $product->sku,
                    'category_id'       => $product->category_id,
                    'category_name'     => $translationProductCategory->name,
                    'brand_id'          => $product->brand_id,
                    'brand_name'        => $product->product_brand->name,
                    'image_path'        => $url,
                    'external_link'     => $product->external_link ?? '',
                    'price'             => $price,
                    'price_msrp'        => $product->price,
                    'description'       => $product->meta_description ?? '',
                ]);

                $productModels->push($productModel);
            }

            return Response::json([
                'message'       => 'SUCCESS',
                'model'         => json_encode($productModels)
            ]);
        }
        catch (\Exception $ex){
            Log::error('Api/ProductController - getHomeProduct error EX: '. $ex);
            return Response::json([
                'message'       => 'ERROR',
                'model'         => ''
            ], 500);
        }
    }

//    public function search(Request $request){
//        try{
//            $keyword = $request->input('keyword');
//        }
//        catch (\Exception $ex){
//            Log::error('Api/ProductController - search error EX: '. $ex);
//            return Response::json([
//                'message'       => 'ERROR',
//                'model'         => ''
//            ], 500);
//        }
//    }
}
