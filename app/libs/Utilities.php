<?php
/**
 * Created by PhpStorm.
 * User: yanse
 * Date: 14-Sep-17
 * Time: 2:38 PM
 */

namespace App\libs;

use App\Models\AutoNumber;
use App\Models\TransactionNumber;
use Carbon\Carbon;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class Utilities
{
    public static function ExceptionLog($ex){
        $logContent = ['id' => 1,
            'description' => $ex];

        $log = new Logger('exception');
        $log->pushHandler(new StreamHandler(storage_path('logs/error.log')), Logger::ALERT);
        $log->info('exception', $logContent);
    }

    public static function CreateProductSlug($string){
        try{
            $string = strtolower($string);
            $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

            return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
        }catch(\Exception $ex){
//            dd($ex);
            error_log($ex);
        }
    }

    public static function createUserEmail($string){
        try{
            $string = strtolower($string);
            $string = str_replace(' ', '', $string); // Remove all spaces.
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

            return $string;
        }catch(\Exception $ex){
            error_log($ex);
        }
    }

    public static function arrayIsUnique($array){
        return array_unique($array) == $array;
    }

    //  Get next incremental number of transaction number
    /**
     * @param $prepend
     * @return int
     * @throws \Exception
     */
    public static function GetNextTransactionNumber($prepend){
        try{
            $nextNo = 1;
            $orderNumber = AutoNumber::find($prepend);
            if(empty($orderNumber)){
                AutoNumber::create([
                    'id'        => $prepend,
                    'next_no'   => 1
                ]);
            }
            else{
                $nextNo = $orderNumber->next_no;
            }

            return $nextNo;
        }
        catch (\Exception $ex){
            throw $ex;
        }
    }

    // Update incremental number of transaction number
    /**
     * @param $prepend
     * @throws \Exception
     */
    public static function UpdateTransactionNumber($prepend){
        try{
            $orderNumber = AutoNumber::find($prepend);
            $orderNumber->next_no++;
            $orderNumber->save();
        }
        catch (\Exception $ex){
            throw $ex;
        }
    }

    // Generate full transaction number
    /**
     * @param $prepend
     * @param $nextNumber
     * @return string
     * @throws \Exception
     */
    public static function GenerateTransactionNumber($prepend, $nextNumber){
        try{
            $modulus = "";
            $nxt = $nextNumber. '';

            switch (strlen($nxt))
            {
                case 1:
                    $modulus = "000000";
                    break;
                case 2:
                    $modulus = "00000";
                    break;
                case 3:
                    $modulus = "0000";
                    break;
                case 4:
                    $modulus = "000";
                    break;
                case 5:
                    $modulus = "00";
                    break;
                case 6:
                    $modulus = "0";
                    break;
            }

            $now = Carbon::now('Asia/Jakarta');

            return $prepend. "/". $now->format('m'). "/". $now->format('d'). "/". $modulus. $nextNumber;
        }
        catch (\Exception $ex){
            throw $ex;
        }
    }

    public static function toFloat($raw){
        $valueStr1 = str_replace('.','', $raw);
        $valueStr2 = str_replace(',', '.', $valueStr1);

        return (double) $valueStr2;
    }

    public static function clearDecimalTrailingZero( $num ){
        $pos = strpos($num, ',');
        if($pos === false) { // it is integer number
            return $num;
        }else{ // it is decimal number
            return rtrim(rtrim($num, '0'), ',');
        }
    }

    // Function terbilang
    public static function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = Utilities::penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = Utilities::penyebut($nilai/10)." puluh". Utilities::penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . Utilities::penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = Utilities::penyebut($nilai/100) . " ratus" . Utilities::penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . Utilities::penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = Utilities::penyebut($nilai/1000) . " ribu" . Utilities::penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = Utilities::penyebut($nilai/1000000) . " juta" . Utilities::penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = Utilities::penyebut($nilai/1000000000) . " milyar" . Utilities::penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = Utilities::penyebut($nilai/1000000000000) . " trilyun" . Utilities::penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
    }

    // Function Terbilang
    public static function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(Utilities::penyebut($nilai));
        } else {
            $hasil = trim(Utilities::penyebut($nilai));
        }
        return $hasil;
    }
}
