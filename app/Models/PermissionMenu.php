<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Aug 2019 11:24:41 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionMenu
 * 
 * @property int $id
 * @property int $admin_role_id
 * @property int $menu_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 * 
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Menu $menu
 * @property \App\Models\AdminUserRole $admin_user_role
 *
 * @package App\Models
 */
class PermissionMenu extends Eloquent
{
	protected $casts = [
		'admin_role_id' => 'int',
		'menu_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'admin_role_id',
		'menu_id',
		'created_by',
		'updated_by'
	];

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
    }

	public function menu()
	{
		return $this->belongsTo(\App\Models\Menu::class);
	}

	public function admin_user_role()
	{
		return $this->belongsTo(\App\Models\AdminUserRole::class, 'admin_role_id');
	}
}
