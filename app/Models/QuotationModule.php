<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 18 Dec 2019 15:44:14 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QuotationModule
 *
 * @property int $id
 * @property int $quotation_id
 * @property int $amount
 * @property int $module_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $createdBy
 * @property \App\Models\AdminUser $updatedBy
 * @property \App\Models\Quotation $quotation
 *
 * @package App\Models
 */
class QuotationModule extends Eloquent
{
	protected $casts = [
		'quotation_id' => 'int',
		'amount' => 'int',
		'module_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'quotation_id',
		'amount',
		'module_id',
		'created_by',
		'updated_by'
	];
    protected $appends = [
        'amount_string',
    ];

    public function getAmountStringAttribute(){
        return number_format($this->attributes['amount'], 0, ",", ".");
    }

    public function updatedBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}

	public function quotation()
	{
		return $this->belongsTo(\App\Models\Quotation::class);
	}
}
