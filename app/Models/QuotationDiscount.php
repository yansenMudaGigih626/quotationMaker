<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 26 Jun 2020 09:42:12 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QuotationDiscount
 *
 * @property int $id
 * @property int $quotation_id
 * @property string $description
 * @property int $amount
 * @property int $percentage
 * @property int $percentage_rupiah
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Quotation $quotation
 *
 * @package App\Models
 */
class QuotationDiscount extends Eloquent
{
	protected $casts = [
		'quotation_id' => 'int',
		'amount' => 'int',
		'percentage' => 'int',
		'percentage_rupiah' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'quotation_id',
		'description',
		'amount',
		'percentage',
		'percentage_rupiah',
		'created_by',
		'updated_by'
	];
    protected $appends = [
        'amount_string',
        'percentage_rupiah_string',
    ];

    public function getAmountStringAttribute(){
        return number_format($this->attributes['amount'], 0, ",", ".");
    }

    public function getPercentageRupiahStringAttribute(){
        return number_format($this->attributes['percentage_rupiah'], 0, ",", ".");
    }

	public function admin_user()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function quotation()
	{
		return $this->belongsTo(\App\Models\Quotation::class);
	}
}
