<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Aug 2019 13:31:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AppLanguage
 * 
 * @property string $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $product_category_translations
 *
 * @package App\Models
 */
class AppLanguage extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function product_category_translations()
	{
		return $this->hasMany(\App\Models\ProductCategoryTranslation::class, 'language_code');
	}
}
