<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 14:55:04 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Banner
 *
 * @property int $id
 * @property string $name
 * @property string $image_path
 * @property string $alt_text
 * @property string $url
 * @property int $product_id
 * @property int $brand_id
 * @property string $target
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\ProductBrand $product_brand
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Product $product
 * @property \App\Models\Status $status
 *
 * @package App\Models
 */
class Banner extends Eloquent
{
	protected $casts = [
		'product_id' => 'int',
		'brand_id' => 'int',
		'status_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'name',
		'image_path',
		'alt_text',
		'url',
		'product_id',
		'brand_id',
		'target',
		'status_id',
		'created_by',
		'updated_by'
	];

	public function product_brand()
	{
		return $this->belongsTo(\App\Models\ProductBrand::class, 'brand_id');
	}

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

	public function updatedBy()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}

	public function status()
	{
		return $this->belongsTo(\App\Models\Status::class);
	}
}
