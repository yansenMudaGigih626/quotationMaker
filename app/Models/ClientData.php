<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Dec 2019 15:06:38 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ClientData
 *
 * @property int $id
 * @property string $name
 * @property string $pic_name
 * @property string $pic_position
 * @property string $address
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $admin_user
 * @property \Illuminate\Database\Eloquent\Collection $invoices
 * @property \Illuminate\Database\Eloquent\Collection $quotations
 *
 * @package App\Models
 */
class ClientData extends Eloquent
{
	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'name',
		'pic_name',
		'pic_position',
		'address',
		'created_by',
		'updated_by'
	];

	public function admin_user()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function invoices()
	{
		return $this->hasMany(\App\Models\Invoice::class, 'client_id');
	}

	public function quotations()
	{
		return $this->hasMany(\App\Models\Quotation::class, 'client_id');
	}
}
