<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 18 Dec 2019 15:45:13 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QuotationModuleDetail
 * 
 * @property int $id
 * @property int $quotation_id
 * @property int $module_detail_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $createdBy
 * @property \App\Models\AdminUser $updatedBy
 * @property \App\Models\ModuleDetail $module_detail
 * @property \App\Models\Quotation $quotation
 *
 * @package App\Models
 */
class QuotationModuleDetail extends Eloquent
{
	protected $casts = [
		'quotation_id' => 'int',
		'module_detail_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'quotation_id',
		'module_detail_id',
		'created_by',
		'updated_by',
        'created_at',
        'updated_at'
	];

    public function updatedBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

	public function module_detail()
	{
		return $this->belongsTo(\App\Models\ModuleDetail::class);
	}

	public function quotation()
	{
		return $this->belongsTo(\App\Models\Quotation::class);
	}
}
