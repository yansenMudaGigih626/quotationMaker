<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jun 2020 15:40:37 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceDiscount
 *
 * @property int $id
 * @property int $invoice_id
 * @property string $description
 * @property int $amount
 * @property int $percentage
 * @property int $percentage_rupiah
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Invoice $invoice
 *
 * @package App\Models
 */
class InvoiceDiscount extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'amount' => 'int',
		'percentage' => 'int',
		'percentage_rupiah' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'invoice_id',
		'description',
		'amount',
		'percentage',
		'percentage_rupiah',
		'created_by',
		'updated_by'
	];
    protected $appends = [
        'amount_string',
        'percentage_rupiah_string',
    ];

    public function getAmountStringAttribute(){
        return number_format($this->attributes['amount'], 0, ",", ".");
    }

    public function getPercentageRupiahStringAttribute(){
        return number_format($this->attributes['percentage_rupiah'], 0, ",", ".");
    }


    public function admin_user()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}
}
