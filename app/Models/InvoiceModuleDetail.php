<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Dec 2019 21:26:46 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceModuleDetail
 * 
 * @property int $id
 * @property int $invoice_id
 * @property int $module_detail_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 * 
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\ModuleDetail $module_detail
 *
 * @package App\Models
 */
class InvoiceModuleDetail extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'module_detail_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'invoice_id',
		'module_detail_id',
		'created_by',
		'updated_by'
	];

	public function admin_user()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function module_detail()
	{
		return $this->belongsTo(\App\Models\ModuleDetail::class);
	}
}
