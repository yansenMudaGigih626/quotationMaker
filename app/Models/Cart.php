<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Aug 2019 18:06:14 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cart
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property string $notes
 * @property int $qty
 * @property float $price
 * @property float $subtotal
 *
 * @property \App\Models\Product $product
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Cart extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'user_id' => 'int',
		'qty' => 'int',
		'price' => 'float',
		'subtotal' => 'float'
	];

	protected $fillable = [
		'product_id',
		'user_id',
		'notes',
		'qty',
		'price',
		'subtotal'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
