<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 18 Dec 2019 00:28:19 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ModuleDetail
 * 
 * @property int $id
 * @property int $module_id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 * 
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Module $module
 *
 * @package App\Models
 */
class ModuleDetail extends Eloquent
{
	protected $casts = [
		'module_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'module_id',
		'description',
		'created_by',
		'updated_by',
        'updated_at',
        'created_at'
	];

    public function updatedBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}
}
