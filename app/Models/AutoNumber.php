<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 29 Jul 2019 16:26:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AutoNumber
 *
 * @property string $id
 * @property int $next_no
 *
 * @package App\Models
 */
class AutoNumber extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'next_no' => 'int'
	];

	protected $fillable = [
	    'id',
		'next_no'
	];
}
