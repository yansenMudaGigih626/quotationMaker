<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 10:58:49 +0700.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

/**
 * Class AdminUser
 *
 * @property int $id
 * @property int $is_super_admin
 * @property int $role_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $language
 * @property int $status_id
 * @property string $image_path
 * @property string $remember_token
 * @property \Carbon\Carbon $email_verified_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Status $status
 * @property \App\Models\AdminUserRole $admin_user_role
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $banners
 * @property \Illuminate\Database\Eloquent\Collection $faqs
 * @property \Illuminate\Database\Eloquent\Collection $fcm_token_browsers
 * @property \Illuminate\Database\Eloquent\Collection $invoice_headers
 * @property \Illuminate\Database\Eloquent\Collection $invoice_settlements
 * @property \Illuminate\Database\Eloquent\Collection $kajian_order_reject_histories
 * @property \Illuminate\Database\Eloquent\Collection $permission_menu_headers
 * @property \Illuminate\Database\Eloquent\Collection $permission_menu_subs
 * @property \Illuminate\Database\Eloquent\Collection $permission_menus
 * @property \Illuminate\Database\Eloquent\Collection $product_brands
 * @property \Illuminate\Database\Eloquent\Collection $product_user_categories
 * @property \Illuminate\Database\Eloquent\Collection $sales_order_headers
 *
 * @package App\Models
 */
class AdminUser extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens;

	protected $casts = [
		'is_super_admin' => 'int',
		'role_id' => 'int',
		'status_id' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'is_super_admin',
		'role_id',
		'first_name',
		'last_name',
		'email',
		'password',
		'language',
		'status_id',
		'image_path',
		'remember_token',
		'email_verified_at'
	];

	public function status()
	{
		return $this->belongsTo(\App\Models\Status::class);
	}

	public function admin_user_role()
	{
		return $this->belongsTo(\App\Models\AdminUserRole::class, 'role_id');
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Address::class, 'updated_by');
	}

	public function banners()
	{
		return $this->hasMany(\App\Models\Banner::class, 'updated_by');
	}

	public function faqs()
	{
		return $this->hasMany(\App\Models\Faq::class, 'updated_by');
	}

	public function fcm_token_browsers()
	{
		return $this->hasMany(\App\Models\FcmTokenBrowser::class, 'user_admin_id');
	}

	public function invoice_headers()
	{
		return $this->hasMany(\App\Models\InvoiceHeader::class, 'updated_by');
	}

	public function invoice_settlements()
	{
		return $this->hasMany(\App\Models\InvoiceSettlement::class, 'created_by');
	}

	public function kajian_order_reject_histories()
	{
		return $this->hasMany(\App\Models\KajianOrderRejectHistory::class);
	}

	public function permission_menu_headers()
	{
		return $this->hasMany(\App\Models\PermissionMenuHeader::class, 'updated_by');
	}

	public function permission_menu_subs()
	{
		return $this->hasMany(\App\Models\PermissionMenuSub::class, 'updated_by');
	}

	public function permission_menus()
	{
		return $this->hasMany(\App\Models\PermissionMenu::class, 'updated_by');
	}

	public function product_brands()
	{
		return $this->hasMany(\App\Models\ProductBrand::class, 'updated_by');
	}

	public function product_user_categories()
	{
		return $this->hasMany(\App\Models\ProductUserCategory::class, 'updated_by');
	}

	public function sales_order_headers()
	{
		return $this->hasMany(\App\Models\SalesOrderHeader::class, 'updated_by');
	}
}
