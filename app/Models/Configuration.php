<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Dec 2019 11:29:33 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Configuration
 * 
 * @property int $id
 * @property string $pt_name
 * @property string $pt_address
 * @property string $sign_name
 * @property string $sign_position
 * @property string $acc_bank
 * @property string $acc_no
 * @property string $acc_holder
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Configuration extends Eloquent
{
	protected $fillable = [
		'pt_name',
		'pt_address',
		'sign_name',
		'sign_position',
		'acc_bank',
		'acc_no',
		'acc_holder'
	];
}
