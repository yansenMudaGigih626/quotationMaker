<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Dec 2019 21:26:38 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceModule
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $amount
 * @property int $module_id
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @property \App\Models\AdminUser $admin_user
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\Module $module
 *
 * @package App\Models
 */
class InvoiceModule extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'amount' => 'int',
		'module_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'invoice_id',
		'amount',
		'module_id',
		'created_by',
		'updated_by'
	];
    protected $appends = [
        'amount_string',
    ];

    public function getAmountStringAttribute(){
        return number_format($this->attributes['amount'], 0, ",", ".");
    }

	public function admin_user()
	{
		return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
	}

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}
}
