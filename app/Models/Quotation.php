<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 26 Jun 2020 09:42:33 +0700.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Quotation
 *
 * @property int $id
 * @property int $client_id
 * @property string $number
 * @property string $project_name
 * @property int $total_amount
 * @property int $discount_1
 * @property int $discount_2
 * @property int $discount_3
 * @property int $discount_4
 * @property int $subtotal
 * @property \Carbon\Carbon $date
 * @property int $header_type
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property int $updated_by
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\ClientData $client_data
 * @property \App\Models\AdminUser $createdBy
 * @property \App\Models\AdminUser $updatedBy
 * @property \Illuminate\Database\Eloquent\Collection $quotation_discounts
 * @property \Illuminate\Database\Eloquent\Collection $module_details
 * @property \Illuminate\Database\Eloquent\Collection $modules
 *
 * @package App\Models
 */
class Quotation extends Eloquent
{
	protected $casts = [
		'client_id' => 'int',
		'total_amount' => 'int',
		'discount_1' => 'int',
		'discount_2' => 'int',
		'discount_3' => 'int',
		'discount_4' => 'int',
		'subtotal' => 'int',
		'header_type' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'client_id',
		'number',
		'project_name',
		'total_amount',
		'discount_1',
		'discount_2',
		'discount_3',
		'discount_4',
		'subtotal',
		'date',
		'header_type',
		'created_by',
		'updated_by'
	];

    protected $appends = [
        'created_at_string',
        'total_amount_string',
        'subtotal_string',
        'discount_1_string',
        'discount_2_string',
        'discount_3_string',
        'discount_4_string',
    ];

    public function getCreatedAtStringAttribute(){
        return Carbon::parse($this->attributes['created_at'])->format('d M Y');
    }

    public function getTotalAmountStringAttribute(){
        return number_format($this->attributes['total_amount'], 0, ",", ".");
    }

    public function getDiscount1StringAttribute(){
        return number_format($this->attributes['discount_1'], 0, ",", ".");
    }

    public function getDiscount2StringAttribute(){
        return number_format($this->attributes['discount_2'], 0, ",", ".");
    }

    public function getDiscount3StringAttribute(){
        return number_format($this->attributes['discount_3'], 0, ",", ".");
    }

    public function getDiscount4StringAttribute(){
        return number_format($this->attributes['discount_4'], 0, ",", ".");
    }

    public function getSubtotalStringAttribute(){
        return number_format($this->attributes['subtotal'], 0, ",", ".");
    }

	public function client_data()
	{
		return $this->belongsTo(\App\Models\ClientData::class, 'client_id');
	}

    public function updatedBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'updated_by');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'created_by');
    }

	public function quotation_discounts()
	{
		return $this->hasMany(\App\Models\QuotationDiscount::class);
	}

    public function module_details()
    {
        return $this->hasMany(\App\Models\QuotationModuleDetail::class, 'quotation_id');
    }

    public function modules()
    {
        return $this->hasMany(\App\Models\QuotationModule::class, 'quotation_id');
    }
}
