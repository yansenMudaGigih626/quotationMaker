<?php


namespace App\Transformer;


use App\Models\AdminUserRole;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    public function transform(AdminUserRole $role){

        try{
            $createdDate = Carbon::parse($role->created_at)->toIso8601String();

            $routeEditUrl = route('admin.role.edit', ['id' => $role->id]);

            $action = "<a class='btn btn-xs btn-info' href='". $routeEditUrl. "' data-toggle='tooltip' data-placement='top'><i class='fas fa-pencil-alt'></i></a>";
//            $action .= "<a class='delete-modal btn btn-xs btn-danger' data-id='". $role->id ."' ><i class='fas fa-trash-alt text-white'></i></a>";

            return[
                'name'              => $role->name,
                'description'       => $role->description,
                'created_at'        => $createdDate,
                'action'            => $action
            ];
        }
        catch (\Exception $ex){
            Log::error('RoleTransformer - transform error EX: '. $ex);
        }
    }
}
