<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 13/02/2018
 * Time: 11:34
 */

namespace App\Transformer;


use App\Models\Module;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ModuleTransformer extends TransformerAbstract
{
    public function transform(Module $module){

        try{
            $createdDate = Carbon::parse($module->created_at)->toIso8601String();

            $routeEditUrl = route('admin.modules.edit', ['id' => $module->id]);

            $action = "<a class='btn btn-xs btn-info' href='".$routeEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>";
            $action .= "<a class='delete-modal btn btn-xs btn-danger text-white' data-id='". $module->id ."' ><i class='fas fa-trash-alt'></i></a>";

            return[
                'description'       => $module->description,
                'created_at'        => $createdDate,
                'action'            => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
