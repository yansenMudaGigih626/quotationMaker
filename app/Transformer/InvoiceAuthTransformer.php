<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 13/02/2018
 * Time: 11:34
 */

namespace App\Transformer;


use App\Models\Invoice;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class InvoiceAuthTransformer extends TransformerAbstract
{
    public function transform(Invoice $invoice){

        try{
            $createdDate = Carbon::parse($invoice->created_at)->toDateTimeString();
            $date = Carbon::parse($invoice->date)->toDateTimeString();
            $routeEditUrl = route('admin.auth-invoices.show', ['id' => $invoice->id]);

            $action = "<a class='btn btn-xs btn-info' href='".$routeEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>"."&nbsp;";

            $isAuthorized = 'Not Authorized';

            if($invoice->is_authorized == 1){
                $isAuthorized = 'Authorized';
            }

            return[
                'number'        => $invoice->number,
                'client'        => $invoice->client_data->name,
                'project_name'  => $invoice->project_name,
                'date'          => $date,
                'sub_total'     => "Rp".$invoice->subtotal_string,
                'discount_1'    => $invoice->discount_1_string,
                'discount_2'    => $invoice->discount_2_string,
                'discount_3'    => $invoice->discount_3_string,
                'discount_4'    => $invoice->discount_4_string,
                'amount'        => "Rp".$invoice->total_amount_string,
                'is_authorized' => $isAuthorized,
                'created_by'    => $invoice->createdBy->first_name . ' ' . $invoice->createdBy->last_name,
                'created_at'    => $createdDate,
                'action'        => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
