<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 13/02/2018
 * Time: 11:34
 */

namespace App\Transformer;


use App\Models\ClientData;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    public function transform(ClientData $client){

        try{
            $createdDate = Carbon::parse($client->created_at)->toIso8601String();

            $routeEditUrl = route('admin.clients.edit', ['id' => $client->id]);

            $action = "<a class='btn btn-xs btn-info' href='".$routeEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>";
            $action .= "<a class='delete-modal btn btn-xs btn-danger text-white' data-id='". $client->id ."' ><i class='fas fa-trash-alt'></i></a>";

            return[
                'name'              => $client->name,
                'address'           => $client->address,
                'pic_name'           => $client->pic_name,
                'pic_position'           => $client->pic_position,
                'created_at'        => $createdDate,
                'action'            => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
