<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 13/02/2018
 * Time: 11:34
 */

namespace App\Transformer;


use App\Models\Quotation;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class QuotationTransformer extends TransformerAbstract
{
    public function transform(Quotation $quotation){

        try{
            $createdDate = Carbon::parse($quotation->created_at)->toIso8601String();
            $date = Carbon::parse($quotation->date)->toIso8601String();
            $routeEditUrl = route('admin.quotations.edit', ['id' => $quotation->id]);
            $routePrintSignUrl = route('admin.print.show-quotation', ['id' => $quotation->id])."?type=1";
            $routePrintNoSignUrl = route('admin.print.show-quotation', ['id' => $quotation->id])."?type=0";

            $action = "<a class='btn btn-xs btn-info' href='".$routeEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>"."&nbsp;";
            $action .= "<a class='delete-modal btn btn-xs btn-danger text-white' data-id='". $quotation->id ."' ><i class='fas fa-trash-alt'></i></a>"."&nbsp;";
            $action .= "<a class='btn btn-xs btn-primary' target='_blank' href='".$routePrintSignUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-print'></i>&nbsp;sign</a>"."&nbsp;";
            $action .= "<a class='btn btn-xs btn-primary' target='_blank' href='".$routePrintNoSignUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-print'></i>&nbsp;no sign</a>";

            return[
                'number'        => $quotation->number,
                'client'        => $quotation->client_data->name,
                'project_name'  => $quotation->project_name,
                'date'          => $date,
                'sub_total'     => "Rp".$quotation->subtotal_string,
                'discount_1'      => $quotation->discount_1_string,
                'discount_2'      => $quotation->discount_2_string,
                'discount_3'      => $quotation->discount_3_string,
                'discount_4'      => $quotation->discount_4_string,
                'amount'        => "Rp".$quotation->total_amount_string,
                'created_by'    => $quotation->createdBy->first_name . ' ' . $quotation->createdBy->last_name,
                'created_at'    => $createdDate,
                'action'        => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
