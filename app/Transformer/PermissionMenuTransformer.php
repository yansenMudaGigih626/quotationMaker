<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 23/01/2018
 * Time: 10:45
 */

namespace App\Transformer;

use App\Models\AdminUserRole;
use App\Models\PermissionMenu;
use League\Fractal\TransformerAbstract;

class PermissionMenuTransformer extends TransformerAbstract
{
    public function transform(AdminUserRole $role){
        $permissionMenuRoute = route('admin.permission-menus.show', ['permission_menu' => $role->id]);
        $roleName =  "<a style='text-decoration: underline;' href='" . $permissionMenuRoute. "' target='_blank'>". $role->name . "</a>";
        $action =
            "<a class='btn btn-xs btn-info' href='permission-menus/edit/". $role->id."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>";

        $permission = PermissionMenu::where('admin_role_id', $role->id)->count();

        return[
            'admin_role'    => $roleName,
            'permission'    => $permission,
            'action'        => $action
        ];
    }
}