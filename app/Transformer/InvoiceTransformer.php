<?php
/**
 * Created by PhpStorm.
 * User: GMG-Developer
 * Date: 13/02/2018
 * Time: 11:34
 */

namespace App\Transformer;


use App\Models\Invoice;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class InvoiceTransformer extends TransformerAbstract
{
    public function transform(Invoice $invoice){

        try{
            $createdDate = Carbon::parse($invoice->created_at)->toIso8601String();
            $date = Carbon::parse($invoice->date)->toIso8601String();
            $routeEditUrl = route('admin.invoices.edit', ['id' => $invoice->id]);
            $routePrintSignUrl = route('admin.print.show-invoice', ['id' => $invoice->id])."?type=1";
            $routePrintNoSignUrl = route('admin.print.show-invoice', ['id' => $invoice->id])."?type=0";

            $action = "<a class='btn btn-xs btn-info' href='".$routeEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-edit'></i></a>"."&nbsp;";
            $action .= "<a class='delete-modal btn btn-xs btn-danger text-white' data-id='". $invoice->id ."' ><i class='fas fa-trash-alt'></i></a>"."&nbsp;";
            $action .= "<a class='btn btn-xs btn-primary' target='_blank' href='".$routePrintSignUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-print'></i>&nbsp;sign</a>"."&nbsp;";
            $action .= "<a class='btn btn-xs btn-primary' target='_blank' href='".$routePrintNoSignUrl."' data-toggle='tooltip' data-placement='top'><i class='fas fa-print'></i>&nbsp;no sign</a>";

            return[
                'number'        => $invoice->number,
                'client'        => $invoice->client_data->name,
                'project_name'  => $invoice->project_name,
                'date'          => $date,
                'sub_total'     => "Rp".$invoice->subtotal_string,
                'discount_1'      => $invoice->discount_1_string,
                'discount_2'      => $invoice->discount_2_string,
                'discount_3'      => $invoice->discount_3_string,
                'discount_4'      => $invoice->discount_4_string,
                'amount'        => "Rp".$invoice->total_amount_string,
                'created_by'    => $invoice->createdBy->first_name . ' ' . $invoice->createdBy->last_name,
                'created_at'    => $createdDate,
                'action'        => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
