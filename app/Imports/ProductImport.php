<?php

namespace App\Imports;

use App\libs\Utilities;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductUserCategory;
use App\Models\ShippingMethod;
use App\Models\User;
use App\Models\UserCategory;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ProductImport implements ToCollection, WithStartRow
{

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        $dateTimeNow = Carbon::now('Asia/Jakarta')->toDateTimeString();

        foreach($rows as $row){
            //$sku = trim($row[1]);
            $name = trim($row[0]);
            if(!empty($name)){
                if(!DB::table('products')
                    ->where('name', $name)
                    ->exists()){
                   // $name = trim($row[0]);
                    $brandStr = trim($row[2]);
                    $brand = ProductBrand::where('name', $brandStr)->first();
                    $weight = 0;
                    if(!empty($row[5])){
                        $weightInput = Utilities::toFloat($row[5]);
                        $weight = $weightInput * 1000;
                    }

                    $length = 0;
                    if(!empty($row[6])){
                        $lengthInput = Utilities::toFloat($row[6]);
                        $length = $lengthInput * 10;
                    }

                    $width = 0;
                    if(!empty($row[7])){
                        $widthInput = Utilities::toFloat($row[7]);
                        $width = $widthInput * 10;
                    }

                    $height = 0;
                    if(!empty($row[8])){
                        $heightInput = Utilities::toFloat($row[8]);
                        $height = $heightInput * 10;
                    }

                    //$colors = trim($row[9]);
                    //$colorArrs = explode(',', $colors);

                    //foreach ($colorArrs as $colorArr){
                        //$color = trim($colorArr);


                    //}

                    $newProduct = Product::create([
                        'name'          => $name,
                        'category_id'   => 1,
                        'brand_id'      => $brand->id,
                        'slug'          => Utilities::CreateProductSlug($name),
                        'sku'           => $name,
                        'description'   => '',
                        'price'         => $row[4],
                        'in_stock'      => 0,
                        'weight'        => $weight,
                        'length'        => $length,
                        'width'         => $width,
                        'height'        => $height,
                        'external_link' => 'www.google.com',
                        'status_id'     => 1,
                        'created_at'    => $dateTimeNow,
                        'updated_at'    => $dateTimeNow
                    ]);

                    // Customize price

//                    $vipPriceDarat = floatval($row[5]);
//                    if(!empty($row[10])){
//                        $vipPriceDarat = floatval($row[10]);
//                    }
//
//                    $vipPriceUdara = floatval($row[5]);
//                    if(!empty($row[11])){
//                        $vipPriceUdara = floatval($row[11]);
//                    }
//
//                    $vvipPriceDarat = floatval($row[5]);
//                    if(!empty($row[12])){
//                        $vvipPriceDarat = floatval($row[12]);
//                    }
//
//                    $vvipPriceUdara = floatval($row[5]);
//                    if(!empty($row[13])){
//                        $vvipPriceUdara = floatval($row[13]);
//                    }

                    $shippingMethods = ShippingMethod::all();
                    $userCategories = UserCategory::all();
                    foreach ($userCategories as $userCategory){
                        foreach ($shippingMethods as $shippingMethod){
                            $shippingPrice = $row[4];
//                            $shippingPrice = 0;
//                            if($shippingMethod->id === 1 && $userCategory->id === 2){
//                                $shippingPrice = $vipPriceDarat;
//                            }
//                            elseif($shippingMethod->id === 2 && $userCategory->id === 2){
//                                $shippingPrice = $vipPriceUdara;
//                            }
//                            elseif($shippingMethod->id === 1 && $userCategory->id === 5){
//                                $shippingPrice = $vvipPriceDarat;
//                            }
//                            elseif($shippingMethod->id === 2 && $userCategory->id === 5){
//                                $shippingPrice = $vvipPriceUdara;
//                            }

                            ProductUserCategory::create([
                                'product_id'        => $newProduct->id,
                                'user_category_id'  => $userCategory->id,
                                'shipping_method'   => $shippingMethod->name,
                                'price'             => $shippingPrice,
                                'created_at'        => $dateTimeNow,
                                'created_by'        => 6,
                                'updated_at'        => $dateTimeNow,
                                'updated_by'        => 6
                            ]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
