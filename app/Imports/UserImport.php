<?php

namespace App\Imports;

use App\libs\Utilities;
use App\Models\Address;
use App\Models\AddressCity;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class UserImport implements ToCollection, WithStartRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $dateTimeNow = Carbon::now('Asia/Jakarta')->toDateTimeString();

        foreach($rows as $row){
            // Create user
            $name = trim($row[0]);
            //error_log($name);
            if(!empty($name)){
                $user = User::where('name', $name)->first();
                if(empty($user)){
                    $email = Utilities::createUserEmail($name);
                    //$email = trim($row[1]);
                    $category = trim($row[3]);
                    $newUser = User::create([
                        'name'              => $name,
                        'category_id'       => $category === 'VIP' ? 5 : 2,
                        'email'             => $email. '@custumergenai.com',
                        'password'          => Hash::make('yifang123'),
                        'email_token'       => base64_encode($email),
                        'phone'             => !empty($row[6]) ? trim($row[6]) : '',
                        'status_id'         => 1,
                        'ktp'               => !empty($row[4]) ? trim($row[4]) : '',
                        'npwp'              => !empty($row[5]) ? trim($row[5]) : '',
                        'created_at'        => $dateTimeNow,
                        'updated_at'        => $dateTimeNow
                    ]);

                    $city = AddressCity::find($row[8]);

                    // Create address
                    Address::create([
                        'user_id'       => $newUser->id,
                        'description'   => !empty($row[7]) ? strtoupper(trim($row[7])) : '',
                        'primary'       => 1,
                        'city_id'       => $row[8],
                        'province_id'   => $city->province_id,
                        'postal_code'   => '',
                        'created_by'    => 6,
                        'created_at'    => $dateTimeNow,
                        'updated_by'    => 6,
                        'updated_at'    => $dateTimeNow
                    ]);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 1;
    }
}
