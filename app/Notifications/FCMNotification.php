<?php
/**
 * Created by PhpStorm.
 * User: YANSEN
 * Date: 2/13/2019
 * Time: 21:26
 */

namespace App\Notifications;


use App\Models\FcmTokenApp;
use App\Models\FcmTokenBrowser;
use App\Models\FcmTokenCollector;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class FCMNotification
{
    public static function SaveToken($userId, $token, $type){
        try{
            if($type == 'app'){
                $isExistToken = FcmTokenApp::where('user_id', $userId)
                    ->where('token', $token)
                    ->first();

                if(empty($isExistToken)){
                    FcmTokenApp::create([
                        'user_id' => $userId,
                        'token' => $token
                    ]);
                }
            }
            else{
                $isExistToken = FcmTokenBrowser::where('user_admin_id', $userId)->first();
                if(!empty($isExistToken)){
                    $isExistToken->token = $token;
                    $isExistToken->save();
                }
                else{
                    FcmTokenBrowser::create([
                        'user_admin_id' => $userId,
                        'token' => $token
                    ]);
                }
            }

            return 1;
        }
        catch (\Exception $ex){
            Log::error("FCMNotification - SaveToken Error: ". $ex);
            return 0;
        }
    }

    public static function SendNotification($userId, $type, $title, $body, $notifData){
        try{
            if($type == 'app'){
                $userFcmTokens  = FcmTokenApp::where('user_id', $userId)->get();
            }
            else{
                $userFcmTokens  = FcmTokenBrowser::where('user_admin_id', $userId)->get();
            }

            if($userFcmTokens->count() === 0){
                Log::error("FCMNotification - SendNotification Error: FCM Token Null, userId = ".$userId);
                return "";
            }

            foreach ($userFcmTokens as $userFcmToken){
                $token = $userFcmToken->token;
                $data = array(
                    "to" => $token,
                    "notification" => [
                        "title"=> $title,
                        "body"=> $body,
                    ],
                    "data" => $notifData,
                );

                $data_string = json_encode($data);
                $client = new Client([
                    'base_uri' => "https://fcm.googleapis.com/fcm/send",
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'key=' .env('FCM_SERVER_KEY'),
                    ],
                ]);

                $response = $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                    'body' => $data_string
                ]);
                $responseJSON = json_decode($response->getBody());

                Log::info("FCMNotification - Message: ". $response->getBody());
            }

            return 1;
        }
        catch (\Exception $exception){
            Log::error("FCMNotification - SendNotification Error: ". $exception);
            return 0;
        }
    }
}
