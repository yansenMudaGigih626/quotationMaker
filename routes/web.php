<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();


Route::get('/', 'Admin\DashboardController@dashboard')->name('admin.dashboard');
Route::get('/test-notif', 'Frontend\HomeController@testNotif')->name('testNotif');
Route::get('/test-notif-send', 'Frontend\HomeController@testNotifSend')->name('testNotifSend');
// ADMIN ROUTE
// ====================================================================================================================

Route::post('/keluar', 'Admin\AdminController@logout')->name('admin.keluar');
Route::prefix('admin')->group(function(){

    Route::get('/testing', 'Admin\AdminController@test')->name('admin.test');
//    Route::get('/', 'Admin\DashboardController@dashboard')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    // Quotation
    Route::get('/quotations', 'Admin\QuotationController@index')->name('admin.quotations.index');
    Route::get('/quotations/create', 'Admin\QuotationController@create')->name('admin.quotations.create');
    Route::post('/quotations/store', 'Admin\QuotationController@store')->name('admin.quotations.store');
    Route::get('/quotations/edit/{quotation}', 'Admin\QuotationController@edit')->name('admin.quotations.edit');
    Route::post('/quotations/update', 'Admin\QuotationController@update')->name('admin.quotations.update');
    Route::post('/quotations/delete', 'Admin\QuotationController@destroy')->name('admin.quotations.destroy');

    Route::get('/quotations/create-invoice/{quotation}', 'Admin\QuotationController@createInvoice')->name('admin.quotations.create-invoice');

    // Modules
    Route::get('/modules', 'Admin\ModuleController@index')->name('admin.modules.index');
    Route::get('/modules/create', 'Admin\ModuleController@create')->name('admin.modules.create');
    Route::post('/modules/store', 'Admin\ModuleController@store')->name('admin.modules.store');
    Route::get('/modules/edit/{module}', 'Admin\ModuleController@edit')->name('admin.modules.edit');
    Route::post('/modules/update', 'Admin\ModuleController@update')->name('admin.modules.update');
    Route::post('/modules/delete', 'Admin\ModuleController@destroy')->name('admin.modules.destroy');

    // Clients
    Route::get('/clients', 'Admin\ClientController@index')->name('admin.clients.index');
    Route::get('/clients/create', 'Admin\ClientController@create')->name('admin.clients.create');
    Route::post('/clients/store', 'Admin\ClientController@store')->name('admin.clients.store');
    Route::get('/clients/edit/{module}', 'Admin\ClientController@edit')->name('admin.clients.edit');
    Route::post('/clients/update', 'Admin\ClientController@update')->name('admin.clients.update');
    Route::post('/clients/delete', 'Admin\ClientController@destroy')->name('admin.clients.destroy');

    // Invoice
    Route::get('/invoice', 'Admin\InvoiceController@index')->name('admin.invoices.index');
    Route::get('/invoice/show/{id}', 'Admin\InvoiceController@show')->name('admin.invoices.show');
    Route::get('/invoice/create', 'Admin\InvoiceController@create')->name('admin.invoices.create');
    Route::post('/invoice/store', 'Admin\InvoiceController@store')->name('admin.invoices.store');
    Route::get('/invoice/edit/{id}', 'Admin\InvoiceController@edit')->name('admin.invoices.edit');
    Route::post('/invoice/update', 'Admin\InvoiceController@update')->name('admin.invoices.update');
    Route::post('/invoice/destroy', 'Admin\InvoiceController@destroy')->name('admin.invoices.destroy');

    // Auth Invoice & Quotation
    Route::get('/invoice/auth', 'Admin\AuthorizationController@indexInvoices')->name('admin.auth-invoices.index');
    Route::get('/invoice/auth/show/{id}', 'Admin\AuthorizationController@showInvoice')->name('admin.auth-invoices.show');
    Route::post('/invoice/auth/approve', 'Admin\AuthorizationController@approveInvoice')->name('admin.auth-invoices.approve');
//    Route::post('/invoice/auth/reject', 'Admin\AuthorizationController@rejectInvoice')->name('admin.auth-invoices.reject');

    //Print
    Route::get('/print/quotation/{id}', 'Admin\PrintController@printQuotation')->name('admin.print.show-quotation');
    Route::get('/print/invoice/{id}', 'Admin\PrintController@printInvoice')->name('admin.print.show-invoice');

    //Configuration
    Route::get('/configuration', 'Admin\ConfigurationController@edit')->name('admin.configuration.edit');
    Route::post('/configuration', 'Admin\ConfigurationController@update')->name('admin.configuration.update');


    // Setting
    Route::get('/setting', 'Admin\AdminController@showSetting')->name('admin.setting');
    Route::post('/setting-update', 'Admin\AdminController@saveSetting')->name('admin.setting.update');
    Route::get('/setting/password', 'Admin\SettingController@editPassword')->name('admin.setting.password.edit');
    Route::post('/setting/password/update', 'Admin\SettingController@updatePassword')->name('admin.setting.password.update');

    // Token
    Route::post('/save-token', 'Admin\AdminController@saveUserToken')->name('admin.save.token');

    // Permission Menus
    Route::get('/permission-menus', 'Admin\PermissionMenuController@index')->name('admin.permission-menus.index');
    Route::get('permission-menus/detail/{permission_menu}', 'Admin\PermissionMenuController@show')->name('admin.permission-menus.show');
    Route::get('/permission-menus/create', 'Admin\PermissionMenuController@create')->name('admin.permission-menus.create');
    Route::post('/permission-menus/store', 'Admin\PermissionMenuController@store')->name('admin.permission-menus.store');
    Route::get('/permission-menus/edit/{permission_menu}', 'Admin\PermissionMenuController@edit')->name('admin.permission-menus.edit');
    Route::post('/permission-menus/update', 'Admin\PermissionMenuController@update')->name('admin.permission-menus.update');
    Route::post('/permission-menus/delete', 'Admin\PermissionMenuController@destroy')->name('admin.permission-menus.destroy');

    // Menus
    Route::get('/menus', 'Admin\MenuController@index')->name('admin.menus.index');
    Route::get('menus/detail/{menu}', 'Admin\MenuController@show')->name('admin.menus.show');
    Route::get('/menus/create', 'Admin\MenuController@create')->name('admin.menus.create');
    Route::post('/menus/store', 'Admin\MenuController@store')->name('admin.menus.store');
    Route::get('/menus/edit/{menu}', 'Admin\MenuController@edit')->name('admin.menus.edit');
    Route::post('/menus/update', 'Admin\MenuController@update')->name('admin.menus.update');
    Route::post('/menus/delete', 'Admin\MenuController@destroy')->name('admin.menus.destroy');

    // Menu Headers
    Route::get('/menu_headers', 'Admin\MenuHeaderController@index')->name('admin.menu_headers.index');
    Route::get('menu_headers/detail/{menu}', 'Admin\MenuHeaderController@show')->name('admin.menu_headers.show');
    Route::get('/menu_headers/create', 'Admin\MenuHeaderController@create')->name('admin.menu_headers.create');
    Route::post('/menu_headers/store', 'Admin\MenuHeaderController@store')->name('admin.menu_headers.store');
    Route::get('/menu_headers/edit/{menu}', 'Admin\MenuHeaderController@edit')->name('admin.menu_headers.edit');
    Route::post('/menu_headers/update', 'Admin\MenuHeaderController@update')->name('admin.menu_headers.update');
    Route::post('/menu_headers/delete', 'Admin\MenuHeaderController@destroy')->name('admin.menu_headers.destroy');

    // Sub Menus
    Route::get('/menu-subs', 'Admin\SubMenuController@index')->name('admin.menu-subs.index');
    Route::get('menu-subs/detail/{menu}', 'Admin\SubMenuController@show')->name('admin.menu-subs.show');
    Route::get('/menu-subs/create', 'Admin\SubMenuController@create')->name('admin.menu-subs.create');
    Route::post('/menu-subs/store', 'Admin\SubMenuController@store')->name('admin.menu-subs.store');
    Route::get('/menu-subs/edit/{menu}', 'Admin\SubMenuController@edit')->name('admin.menu-subs.edit');
    Route::post('/menu-subs/update', 'Admin\SubMenuController@update')->name('admin.menu-subs.update');
    Route::post('/menu-subs/delete', 'Admin\SubMenuController@destroy')->name('admin.menu-subs.destroy');

    // Admin User
    Route::get('/admin-users', 'Admin\AdminUserController@index')->name('admin.admin-users.index');
    Route::get('/admin-users/create', 'Admin\AdminUserController@create')->name('admin.admin-users.create');
    Route::post('/admin-users/store', 'Admin\AdminUserController@store')->name('admin.admin-users.store');
    Route::get('/admin-users/edit/{item}', 'Admin\AdminUserController@edit')->name('admin.admin-users.edit');
    Route::post('/admin-users/update', 'Admin\AdminUserController@update')->name('admin.admin-users.update');
    Route::post('/admin-users/delete', 'Admin\AdminUserController@destroy')->name('admin.admin-users.destroy');

    // Admin Role
    Route::get('/admin_roles', 'Admin\RoleController@index')->name('admin.role.index');
    Route::get('/admin_roles/create', 'Admin\RoleController@create')->name('admin.role.create');
    Route::post('/admin_roles/store', 'Admin\RoleController@store')->name('admin.role.store');
    Route::get('/admin_roles/edit/{id}', 'Admin\RoleController@edit')->name('admin.role.edit');
    Route::post('/admin_roles/update/{id}', 'Admin\RoleController@update')->name('admin.role.update');
    Route::post('/admin_roles/delete', 'Admin\RoleController@destroy')->name('admin.role.destroy');

    // User
    Route::get('/users', 'Admin\UserController@index')->name('admin.users.index');
    Route::get('/users/show/{id}', 'Admin\UserController@show')->name('admin.users.show');
    Route::get('/users/create', 'Admin\UserController@create')->name('admin.users.create');
    Route::post('/users/store', 'Admin\UserController@store')->name('admin.users.store');
    Route::get('/users/edit/{item}', 'Admin\UserController@edit')->name('admin.users.edit');
    Route::post('/users/update/{id}', 'Admin\UserController@update')->name('admin.users.update');
    Route::post('/users/delete', 'Admin\UserController@destroy')->name('admin.users.destroy');

    // User Category
    Route::get('/user_categories', 'Admin\UserCategoryController@index')->name('admin.user_categories.index');
    Route::get('/user_categories/create', 'Admin\UserCategoryController@create')->name('admin.user_categories.create');
    Route::post('/user_categories/store', 'Admin\UserCategoryController@store')->name('admin.user_categories.store');
    Route::get('/user_categories/edit/{id}', 'Admin\UserCategoryController@edit')->name('admin.user_categories.edit');
    Route::post('/user_categories/update/{id}', 'Admin\UserCategoryController@update')->name('admin.user_categories.update');
    Route::post('/user_categories/delete', 'Admin\UserCategoryController@destroy')->name('admin.user_categories.destroy');
});

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Route::view('/send-email', 'auth.send-email');



// Datatables
Route::get('/datatables-quotations', 'Admin\QuotationController@getIndex')->name('datatables.quotations');
Route::get('/datatables-invoices', 'Admin\InvoiceController@getIndex')->name('datatables.invoices');
Route::get('/datatables-auth-invoices', 'Admin\AuthorizationController@getInvoicesIndex')->name('datatables.auth-invoices');
Route::get('/datatables-modules', 'Admin\ModuleController@getIndex')->name('datatables.modules');
Route::get('/datatables-clients', 'Admin\ClientController@getIndex')->name('datatables.clients');
Route::get('/datatables-menu-headers', 'Admin\MenuHeaderController@getIndex')->name('datatables.menu_headers');
Route::get('/datatables-menus', 'Admin\MenuController@getIndex')->name('datatables.menus');
Route::get('/datatables-menu-subs', 'Admin\SubMenuController@getIndex')->name('datatables.menu-subs');
Route::get('/datatables-admin-users', 'Admin\AdminUserController@getIndex')->name('datatables.admin_users');
Route::get('/datatables-roles', 'Admin\RoleController@getIndex')->name('datatables.roles');
Route::get('/datatables-admin-products', 'Admin\ProductController@getIndex')->name('datatables.admin_products');
Route::get('/datatables-users', 'Admin\UserController@getIndex')->name('datatables.users');
Route::get('/datatables-categories', 'Admin\CategoryController@getIndex')->name('datatables.categories');
Route::get('/datatables-brands', 'Admin\BrandController@getIndex')->name('datatables.brands');
Route::get('/datatables-user-categories', 'Admin\UserCategoryController@getIndex')->name('datatables.user_categories');
Route::get('/datatables-permission-menus', 'Admin\PermissionMenuController@getIndex')->name('datatables.permission-menus');

// Select2
Route::get('/select-modules', 'Admin\ModuleController@getModules')->name('select.modules');
Route::get('/select-module-details', 'Admin\ModuleController@getModuleDetails')->name('select.module-details');


Route::get('/select-admin-users', 'Admin\AdminUserController@getAdminUsers')->name('select.admin-users');
Route::get('/select-users', 'Admin\UserController@getUsers')->name('select.users');
Route::get('/select-user-categories', 'Admin\UserCategoryController@getCategories')->name('select.user-categories');
Route::get('/select-products', 'Admin\ProductController@getProducts')->name('select.products');
