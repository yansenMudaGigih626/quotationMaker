<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/logout', 'Api\UserManagementController@logout');

Route::middleware('auth:api')->get('/users', 'Api\UserController@index');
Route::get('/noauth/users', 'Api\UserController@index');
Route::post('/closest-waste-banks', 'Api\WasteBankController@getClosestWasteBanks');
Route::get('/dws-category', 'Api\DwsWasteController@getData');
Route::get('/masaro-category', 'Api\MasaroWasteController@getData');
//Route::post('/waste-banks/get-schedules', 'Api\WasteBankController@getWasteBankSchedules');
//Route::post('/on-demand/create', 'Api\TransactionHeaderController@createTransaction');
//Route::post('/routine-pickup', 'Api\UserController@changeRoutinePickup');

// Register
Route::post('/register', 'Api\RegisterController@register');
Route::get('/verifyemail/{token}', 'Api\RegisterController@verify');
Route::post('/external-register', 'Api\RegisterController@externalAuth');
Route::post('/register/exist/email', 'Api\RegisterController@isEmailExist');
Route::post('/register/exist/phone', 'Api\RegisterController@isPhoneExist');

//User Management
//Route::group(['namespace' => 'Api', 'middleware' => 'api', 'prefix' => 'user'], function () {
Route::middleware('auth:api')->prefix('user')->group(function(){
    Route::get('/testing', 'Api\UserController@testingAuthToken');
    Route::get('/get-users', 'Api\UserController@index');
    Route::get('/get-user-data', 'Api\UserController@show');
    Route::post('/save-user-device', 'Api\UserController@saveUserToken');
    Route::get('/waste-banks', 'Api\WasteBankController@getData');
    Route::get('/check-category', 'Api\GeneralController@checkCategory');
    Route::get('/address', 'Api\UserController@getAddress');
    Route::post('/set-address', 'Api\UserController@setAddress');
    Route::post('/profile/update', 'Api\UserController@updateProfile');

    //Transactions
    Route::get('/get-transactions', 'Api\TransactionHeaderController@getTransactions');
    Route::post('/get-transaction-details', 'Api\TransactionHeaderController@getTransactionDetails');
    Route::post('/get-transaction-data', 'Api\TransactionHeaderController@getTransactionData');


    //Voucher
    Route::get('/voucher-categories', 'Api\VoucherController@getCategories');
    Route::post('/vouchers', 'Api\VoucherController@get');
    Route::post('/vouchers/all', 'Api\VoucherController@getAll');
    Route::post('/vouchers/buy', 'Api\VoucherController@buy');
    Route::post('/vouchers/redeem', 'Api\VoucherController@redeem');
    Route::get('/vouchers/list', 'Api\VoucherController@getAllUserVoucher');

    //Point
    Route::get('/point/get', 'Api\PoinController@getCustomerPoint');
    Route::post('/redeem-poin', 'Api\PoinController@redeem');
});

//Forgot Password
Route::post('/checkEmail', 'Api\ForgotPasswordController@checkEmail');
Route::post('/sendResetLinkEmail', 'Api\ForgotPasswordController@sendResetLinkEmail');
Route::post('/setNewPassword', 'Api\ForgotPasswordController@setNewPassword');
Route::get('/registration', 'Api\RegisterController@registrationData');

Route::group(['namespace' => 'Api', 'middleware' => 'api', 'prefix' => 'password'], function () {
    Route::post('forgotpassword', 'ForgotPasswordController@forgotPassword');
    Route::get('find/{token}', 'ForgotPasswordController@find');
    Route::post('reset', 'ForgotPasswordController@reset');
});

//Beta
Route::post('/subscribe', 'Api\SubscribeController@save');
Route::post('/submit-demo', 'Api\SubscribeController@demoSubmit');

//Coba2
Route::post('/test', 'Api\TransactionHeaderController@test');


// Yifang

// Dashboard
Route::get('/dashboard', 'Api\DashboardController@getData');

Route::middleware('auth:api')->group(function() {
    // Cart
    Route::get('/cart', 'Api\CartController@getCart');
    Route::post('/cart/add', 'Api\CartController@addToCart');
    Route::post('/cart/update', 'Api\CartController@updateCart');
    Route::post('/cart/delete', 'Api\CartController@deleteCart');
    Route::get('/cart/shipping', 'Api\CartController@getShippingPrices');
    Route::post('/cart/checkout', 'Api\CartController@checkOutCart');
    Route::get('/cart/amount', 'Api\CartController@getCartAmount');

    //Orders
    Route::post('/order', 'Api\OrderController@getTransactions');
    Route::post('/order/detail', 'Api\OrderController@getTransactionData');

    Route::post('/user/password/change', 'Api\UserController@changePassword');
});

// Product
Route::post('/product/get', 'Api\ProductController@getAllProduct');
Route::post('/product/show', 'Api\ProductController@show');

// Home Page
Route::get('/home/banner/get', 'Api\BannerController@getHomeBanner');
Route::post('/home/category_brand/get', 'Api\ProductCategoryController@getHomeBrandAndCategory');
Route::post('/home/product/get', 'Api\ProductController@getHomeProduct');
