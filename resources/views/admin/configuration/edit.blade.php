@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3>UBAH CONFIGURATION</h3>
                    </div>
                </div>

                {{ Form::open(['route'=>['admin.configuration.update'],'method' => 'post','id' => 'general-form', 'enctype' => 'multipart/form-data']) }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">
                                <div class="tab-content pb-3" id="v-pills-tabContent">
                                    <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                        @include('partials.admin._messages')
                                        @foreach($errors->all() as $error)
                                            <ul>
                                                <li>
                                                    <span class="help-block">
                                                        <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                    </span>
                                                </li>
                                            </ul>
                                        @endforeach
                                        <!-- Input -->

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="name">Nama PT *</label>
                                                    <input id="pt_name" type="text" class="form-control" style="text-transform: uppercase;"
                                                           name="pt_name" value="{{ $configuration->pt_name }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="description">Alamat *</label>
                                                    <textarea id="pt_address" class="form-control"
                                                              name="pt_address" rows="4">{{ $configuration->pt_address }}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="name">Nama Pada tanda tangan *</label>
                                                    <input id="sign_name" type="text" class="form-control" style="text-transform: uppercase;"
                                                           name="sign_name" value="{{ $configuration->sign_name }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="name">Jabatan *</label>
                                                    <input id="sign_position" type="text" class="form-control"
                                                           name="sign_position" value="{{ $configuration->sign_position }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="name">Bank *</label>
                                                    <input id="acc_bank" type="text" class="form-control" style="text-transform: uppercase;"
                                                           name="acc_bank" value="{{ $configuration->acc_bank }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="url">Account No </label>
                                                    <input id="acc_no" type="text" class="form-control"
                                                           name="acc_no" value="{{ $configuration->acc_no }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <label class="form-label" for="name">Account holder *</label>
                                                    <input id="acc_holder" type="text" class="form-control"
                                                           name="acc_holder" value="{{ $configuration->acc_holder }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                            <a href="{{ route('admin.dashboard') }}" class="btn btn-danger">BATAL</a>
                                            <input type="submit" class="btn btn-success" value="SIMPAN">
                                        </div>
                                        <!-- #END# Input -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
@endsection


@section('styles')
    <link href="{{ asset('kartik-v-bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet"/>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('kartik-v-bootstrap-fileinput/js/fileinput.min.js') }}"></script>
@endsection
