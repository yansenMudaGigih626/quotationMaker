@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Edit Module</h2>

                {{ Form::open(['route'=>['admin.modules.update'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                            @endforeach
                                            <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="name">Description *</label>
                                                            <input id="name" name="description" type="text" value="{{ $module->description }}"
                                                                   class="form-control" required>
                                                            <input type="hidden" name="id" value="{{ $module->id }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="description">Details *</label>
                                                            <div
                                                                    v-for="(detail, index) in details"
                                                                    class="form-inline"
                                                            >
                                                                <div class="col-md-8">
                                                                    <input name="details[]" type="text"
                                                                           class="form-control"
                                                                           style="width: 100%;"
                                                                           v-model="detail.description">
                                                                    <input name="ids[]" type="hidden"
                                                                           class="form-control"
                                                                           v-model="detail.id">
                                                                </div>
                                                                <div class="col-md-4"
                                                                     v-if="details.length > 1"
                                                                >
                                                                    <i class='fas fa-trash-alt' v-on:click="removeDetail(index)"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="btn btn-primary" v-on:click="addMoreDetail">Add more Details</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.modules.index') }}" class="btn btn-danger">Exit</a>
                                                <input type="submit" class="btn btn-success" value="Save">
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
    <script>
        let data1 = '{{ $details }}';
        let data = JSON.parse(data1.replace(/&quot;/g, '"'));
        new Vue({
            el: '#app',
            data: {
                details: data
            },
            methods:{
                addMoreDetail(){
                    let nDetail = {
                        value: ''
                    };
                    this.details.push(nDetail);
                },
                removeDetail(index){
                    this.details.splice(index, 1);
                }
            }
        });
    </script>
@endsection