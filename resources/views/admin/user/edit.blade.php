@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3>UBAH DATA MASTER DEALER</h3>
                    </div>
                </div>

                {{ Form::open(['route'=>['admin.users.update', $user->id],'method' => 'post','id' => 'general-form', 'enctype' => 'multipart/form-data']) }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">
                                <div class="tab-content pb-3" id="v-pills-tabContent">
                                    <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                        <!-- Input -->
                                        <div class="body">

                                            @if(count($errors))
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                                <ul>
                                                                    @foreach($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-4 col-12">
                                                <div class="form-group form-float form-group-lg">
                                                    <div class="form-line">
                                                        <label class="form-label" for="email">Email Login *</label>
                                                        <input id="email" type="email" class="form-control"
                                                               name="email" value="{{ $user->email }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="password">Ganti Kata Sandi</label>
                                                                <input id="password" type="password" class="form-control"
                                                                       name="password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="password_confirmation">Konfirmasi Kata Sandi</label>
                                                                <input id="password_confirmation" type="password" class="form-control"
                                                                       name="password_confirmation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="name">Nama *</label>
                                                                <input id="name" name="name" type="text"
                                                                       class="form-control" style="text-transform: uppercase;" value="{{ $user->name }}" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="phone">Telepon/Fax</label>
                                                                <input id="phone" name="phone" type="text"
                                                                       class="form-control" value="{{ $user->phone }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="ktp">Nomor KTP</label>
                                                                <input id="ktp" name="ktp" type="text"
                                                                       class="form-control" style="text-transform: uppercase;" value="{{ $user->ktp }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="npwp">Nomor NPWP</label>
                                                                <input id="npwp" name="npwp" type="text"
                                                                       class="form-control" value="{{ $user->npwp }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="image_main">Foto KTP</label>
                                                                {!! Form::file('ktp_image', array('id' => 'ktp_image', 'class' => 'file-loading', 'accept' => 'image/*')) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="image_main">Foto NPWP</label>
                                                                {!! Form::file('npwp_image', array('id' => 'npwp_image', 'class' => 'file-loading', 'accept' => 'image/*')) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group form-float form-group-lg">
                                                    <div class="form-line">
                                                        <label class="form-label" for="address">Alamat</label>
                                                        <textarea id="address" name="address" rows="3"
                                                                  class="form-control" style="text-transform: uppercase">{{ $address->description }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="postal_code">Kodepos</label>
                                                                <input id="postal_code" name="postal_code" type="text"
                                                                       class="form-control" value="{{ $address->postal_code }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="city">Kota *</label>
                                                                <select id="city" name="city" class="form-control">
                                                                    @if(!empty($address->city_id))
                                                                        <option value="{{ $address->city_id }}" selected>{{ $address->address_city->name }}</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
{{--                                                    <div class="col-md-4 col-12">--}}
{{--                                                        <div class="form-group form-float form-group-lg">--}}
{{--                                                            <div class="form-line">--}}
{{--                                                                <label class="form-label" for="island">Wilayah *</label>--}}
{{--                                                                <select id="island" name="island" class="form-control">--}}
{{--                                                                    <option value="-1"> - Pilih Wilayah - </option>--}}
{{--                                                                    @foreach($islands as $island)--}}
{{--                                                                        <option value="{{ $island->id }}" @if($address->island_id === $island->id) selected @endif>{{ $island->name }}</option>--}}
{{--                                                                    @endforeach--}}
{{--                                                                </select>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="province">Provinsi *</label>
                                                                <select id="province" name="province" class="form-control">
                                                                    @foreach($provinces as $province)
                                                                        <option value="{{ $province->id }}" @if($address->province_id == $province->id) selected @endif>{{ $province->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="role">Kategori MD *</label>
                                                    <select id="role" name="category" class="form-control">
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}" @if($user->category_id === $category->id) selected @endif>{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}" class="btn btn-danger">BATAL</a>
                                            <input type="submit" class="btn btn-success" value="SIMPAN">
                                        </div>
                                        <!-- #END# Input -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


@endsection

@section('styles')
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('kartik-v-bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet"/>
    <style>
        .select2-container--default .select2-search--dropdown::before {
            content: "";
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('kartik-v-bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('kartik-v-bootstrap-fileinput/themes/fas/theme.js') }}"></script>
    <script>
        $('#city').select2({
            placeholder: {
                id: '-1',
                text: ' - Pilih Kota - '
            },
            width: '100%',
            minimumInputLength: 0,
            ajax: {
                url: '{{ route('select.cities') }}',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });

        $('#province').select2({
            placeholder: {
                id: '-1',
                text: ' - Pilih Provinsi - '
            },
            width: '100%',
            minimumInputLength: 0
        });

        @if(!empty($user->ktp_image_path))
            var ktpImageUrl = '{{ asset('storage/ktps/'. $user->ktp_image_path) }}';
            $("#ktp_image").fileinput({
                theme: 'fas',
                initialPreview : [ktpImageUrl],
                initialPreviewAsData: true,
                overwriteInitial: true,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
                maxFileCount: 1
            });
        @else
    $("#ktp_image").fileinput({
        theme: 'fas',
        showUpload: false,
        allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
        maxFileCount: 1
    });
        @endif


        @if(!empty($user->npwp_image_path))
            var npwpImageUrl = '{{ asset('storage/npwps/'. $user->npwp_image_path) }}';
            $("#npwp_image").fileinput({
                theme: 'fas',
                initialPreview : [npwpImageUrl],
                initialPreviewAsData: true,
                overwriteInitial: true,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
                maxFileCount: 1
            });
        @else
            $("#npwp_image").fileinput({
                theme: 'fas',
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
                maxFileCount: 1
            });
        @endif

    </script>
@endsection
