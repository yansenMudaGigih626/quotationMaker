@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8 col-12">
                        <a href="{{ route('admin.users.index') }}" class="btn btn-outline-primary float-left">
                            <i class="fas fa-arrow-left"></i>
                        </a>
                        <h3 class="float-left ml-3">DETIL MASTER DEALER {{ $user->name }}</h3>
                    </div>
                    <div class="col-md-4 col-12 text-right">
                        <a href="{{ route('admin.users.edit', ['item' => $user->id]) }}" class="btn btn-primary">UBAH</a>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">HAPUS</button>
                    </div>
                </div>

                <form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">
                                <div class="tab-content pb-3" id="v-pills-tabContent">
                                    <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                        <!-- Input -->
                                        <div class="body">
                                            @include('partials.admin._messages')
                                            <div class="col-md-4 col-12">
                                                <div class="form-group form-float form-group-lg">
                                                    <div class="form-line">
                                                        <label class="form-label" for="email">Email</label>
                                                        <input id="email" type="email" class="form-control"
                                                               name="email" value="{{ $user->email }}" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="name">Nama</label>
                                                                <input id="name" name="name" type="text"
                                                                       class="form-control" value="{{ $user->name }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="phone">Telepon/Fax</label>
                                                                <input id="phone" name="phone" type="text"
                                                                       class="form-control" value="{{ $user->phone }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="ktp">KTP</label>
                                                                <input id="ktp" name="ktp" type="text"
                                                                       class="form-control" value="{{ $user->ktp }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="npwp">NPWP</label>
                                                                <input id="npwp" name="npwp" type="text"
                                                                       class="form-control" value="{{ $user->npwp }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 text-center">
                                                        @if(!empty($user->ktp_image_path))
                                                            <img src="{{ asset('storage/ktps/'. $user->ktp_image_path) }}" style="height: 140px; width: auto;">
                                                        @else
                                                            <h3>Tidak Ada Foto</h3>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6 col-12 text-center">
                                                        @if(!empty($user->npwp_image_path))
                                                            <img src="{{ asset('storage/npwps/'. $user->npwp_image_path) }}" style="height: 140px; width: auto;">
                                                        @else
                                                            <h3>Tidak Ada Foto</h3>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group form-float form-group-lg">
                                                    <div class="form-line">
                                                        <label class="form-label" for="address">Alamat</label>
                                                        <textarea id="address" name="address" rows="3"
                                                                  class="form-control" readonly>{{ $address->description }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="postal_code">Kodepos</label>
                                                                <input id="postal_code" name="postal_code" type="text"
                                                                       class="form-control" value="{{ $address->postal_code }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="city">Kota</label>
                                                                <input id="city" name="city" type="text"
                                                                       class="form-control" value="{{ $address->address_city->name }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
{{--                                                    <div class="col-md-4 col-12">--}}
{{--                                                        <div class="form-group form-float form-group-lg">--}}
{{--                                                            <div class="form-line">--}}
{{--                                                                <label class="form-label" for="island">Wilayah</label>--}}
{{--                                                                <input id="island" name="island" type="text"--}}
{{--                                                                       class="form-control" value="{{ $address->island->name }}" readonly>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="province">Provinsi</label>
                                                                <input id="province" name="province" type="text"
                                                                       class="form-control" value="{{ $address->address_province->name }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="role">Kategori MD</label>
                                                    <input id="role" name="role" type="text"
                                                           class="form-control" value="{{ $user->user_category->name }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
