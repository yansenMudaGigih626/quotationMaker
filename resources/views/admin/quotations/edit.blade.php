@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Edit Quotation</h2>

                {{ Form::open(['route'=>['admin.quotations.update'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @include('partials.admin._messages')
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                        @endforeach
                                        <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="quotation_number">Quotation Number *</label>
                                                            <input id="quotation_number" name="quotation_number" type="text" value="{{ $quotation->number }}"
                                                                   class="form-control" required>
                                                            <input type="hidden" name="id" value="{{ $quotation->id }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="project_name">Project Name *</label>
                                                            <input id="project_name" name="project_name" type="text" value="{{ $quotation->project_name }}"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="date">Quotation Date *</label>
                                                            <input type="text" id="date" name="date" class="form-control" value="{{ $quotation_date }}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="clients">Client *</label>
                                                            <select class="form-control select2 custom-select" id="clients" name="client">
                                                                @foreach($clients as $client)
                                                                    @if($client->id == $quotation->client_id)
                                                                        <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                                                                    @else
                                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="form-label">Modules *</label>
                                                    <table>
                                                        @foreach($modules as $module)
                                                            <tr>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox mr-sm-2">
                                                                        @php($isSelected=false)
                                                                        @foreach($quotation->modules as $nModule)
                                                                            @if($module->id == $nModule->module_id)
                                                                                @php($isSelected=true)
                                                                            @endif
                                                                        @endforeach

                                                                        <input type="checkbox" class="custom-control-input"
                                                                               @if($isSelected) checked @endif
                                                                               id="{{ $module->id }}" name="modules[]" value="{{ $module->id }}">
                                                                        <label class="custom-control-label" for="{{ $module->id }}">{{ $module->description }}</label>
                                                                    </div>
                                                                    @foreach($module->module_details as $detail)
                                                                        <div class="custom-control custom-checkbox mr-sm-2 m-l-20">
                                                                            @php($isSelected=false)
                                                                            @foreach($quotation->module_details as $nDetail)
                                                                                @if($detail->id == $nDetail->module_detail_id)
                                                                                    @php($isSelected=true)
                                                                                @endif
                                                                            @endforeach
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   @if($isSelected) checked @endif
                                                                                   id="detail{{ $detail->id }}" name="details[]" value="{{ $detail->id }}">
                                                                            <label class="custom-control-label" for="detail{{ $detail->id }}">{{ $detail->description }}</label>
                                                                        </div>
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    @php($amount=null)
                                                                    @foreach($quotation->modules as $nModule)
                                                                        @if($module->id == $nModule->module_id)
                                                                            @php($amount=$nModule->amount)
                                                                        @endif
                                                                    @endforeach

                                                                    <input type="text" name="amounts[]" class="form-control" value="{{ $amount }}" placeholder="Price">
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group form-float form-group-lg">--}}
{{--                                                        <div class="form-line">--}}
{{--                                                            <label class="form-label" for="discount">Discount *</label>--}}
{{--                                                            <input id="discount" name="discount" type="text" value="{{ $quotation->discount }}"--}}
{{--                                                                   class="form-control" placeholder="Discount Total">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_1">Discount 1</label>
                                                            <input id="discount_1" name="discount_1" type="text" value="{{ $quotation->discount_1 }}"
                                                                   class="form-control" placeholder="Discount 1 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_2">Discount 2</label>
                                                            <input id="discount_2" name="discount_2" type="text" value="{{ $quotation->discount_2 }}"
                                                                   class="form-control" placeholder="Discount 2 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_3">Discount 3</label>
                                                            <input id="discount_3" name="discount_3" type="text" value="{{ $quotation->discount_3 }}"
                                                                   class="form-control" placeholder="Discount 3 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_4">Discount 4</label>
                                                            <input id="discount_4" name="discount_4" type="text" value="{{ $quotation->discount_4 }}"
                                                                   class="form-control" placeholder="Discount 4 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <span class="font-weight-bold" style="padding-left: 12px;">Header Type</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="radio" id="header_1" name="header_type" value="1" @if($quotation->header_type == 1) checked @endif>
                                                    <label for="header_1">BEN & WYATT DESIGN HOUSE</label><br>
                                                    <input type="radio" id="header_2" name="header_type" value="2" @if($quotation->header_type == 2) checked @endif>
                                                    <label for="header_2">PT PUTRA JAYA VISUAL</label>
                                                </div>
                                            </div>

                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.quotations.index') }}" class="btn btn-danger">Exit</a>
                                                <input type="submit" class="btn btn-success" value="Save">
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        jQuery('#date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy"
        });
        $(".select2").select2();
    </script>
@endsection
