@extends('layouts.admin')

@section('content')
    <div id="vue-section" class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Create New Quotation</h2>

                {{ Form::open(['route'=>['admin.quotations.store'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @include('partials.admin._messages')
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                        @endforeach
                                        <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="quotation_number">{{$type}} Number *</label>
                                                            <input id="quotation_number" name="quotation_number" type="text" value="{{ old('quotation_number') }}"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="project_name">Project Name </label>
                                                            <input id="project_name" name="project_name" type="text" value="{{ old('project_name') }}"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="date">{{$type}} Date *</label>
                                                            <input type="text" id="date" name="date" class="form-control" value="{{ $todayDate }}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="clients">Client</label>
                                                            <select class="form-control select2 custom-select" name="client" id="clients">
                                                                @foreach($clients as $client)
                                                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="clients">Or Create New Client</label>
                                                                <input type="text" name="newClientName" class="form-control" placeholder="Client Name" value=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-float form-group-lg">
                                                            <div class="form-line">
                                                                <label class="form-label" for="clients">&nbsp;</label>
                                                                <input type="text" name="newClientPic" class="form-control" placeholder="PIC Name" value=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
{{--                                                <div class="col-md-12">--}}
{{--                                                    <label class="form-label">Modules *</label>--}}
{{--                                                    <table>--}}
{{--                                                        @foreach($modules as $module)--}}
{{--                                                            <tr>--}}
{{--                                                                <td>--}}
{{--                                                                    <div class="custom-control custom-checkbox mr-sm-2">--}}
{{--                                                                        <input type="checkbox" class="custom-control-input" id="{{ $module->id }}" name="modules[]" value="{{ $module->id }}">--}}
{{--                                                                        <label class="custom-control-label" for="{{ $module->id }}">{{ $module->description }}</label>--}}
{{--                                                                    </div>--}}
{{--                                                                    @foreach($module->module_details as $detail)--}}
{{--                                                                        <div class="custom-control custom-checkbox mr-sm-2 m-l-20">--}}
{{--                                                                            <input type="checkbox" class="custom-control-input" id="detail{{ $detail->id }}" name="details[]" value="{{ $detail->id }}">--}}
{{--                                                                            <label class="custom-control-label" for="detail{{ $detail->id }}">{{ $detail->description }}</label>--}}
{{--                                                                        </div>--}}
{{--                                                                    @endforeach--}}
{{--                                                                </td>--}}
{{--                                                                <td>--}}
{{--                                                                    <input type="text" name="amounts[]" class="form-control" placeholder="Price">--}}
{{--                                                                </td>--}}
{{--                                                            </tr>--}}
{{--                                                        @endforeach--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="table_detail" class="table table-bordered" style="table-layout: fixed; width: 2000px;">
                                                            <thead>
                                                            <tr class="text-center">
                                                                <th width="20%">Module</th>
                                                                <th width="20%">(Or) New Module</th>
                                                                <th width="20%">Module Detail</th>
                                                                <th width="20%">(Or) New Module Detail</th>
                                                                <th width="15%">Price</th>
                                                                <th width="10%"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr v-for="(item, index) in itemList">
                                                                <td>
                                                                    <select class="form-control" placeholder="Select Module"
                                                                            v-bind:id="item.itemId" v-on:change="getDetailModule(item.itemId, index)">
                                                                        <option value="0">-- Select Module --</option>
                                                                        <option v-for="(module, indexModule) in item.modules" :value="module.id">
                                                                            @{{module.description}}
                                                                        </option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="new-modules[]" class="form-control" placeholder="Create New Module">
                                                                    <input type="hidden" name="modules[]" class="form-control"
                                                                           :value="item.idModules">
                                                                </td>
                                                                <td>
                                                                    <div class="row mb-3" v-for="(itemDetail, indexDetail) in item.detailModules">
{{--                                                                        <select2 url="{{ route('select.modules') }}"--}}
{{--                                                                                 name="moduleDetails[index][]" class="form-control form-control-select2" placeholder="Select Detail">--}}
{{--                                                                        </select2>--}}
                                                                        <select name="moduleDetails[]" class="form-control" placeholder="Select Detail">
                                                                            <option value="0">-- Select Detail --</option>
                                                                            <option v-for="(detail, indexModule) in item.details" :value="detail.id">
                                                                                @{{detail.text}}
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <a id="add-btn" class="btn btn-primary text-white" v-on:click="addMoreDetails(index)">Add</a> &nbsp;
                                                                        <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeModuleDetail(index)">Delete</a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="row mb-3" v-for="(itemDetail, indexDetail) in item.newDetailModules">
                                                                        <input type="text" name="new-module-details[]" class="form-control" placeholder="Create New Detail">
                                                                        <input type="hidden" name="newModuleDetailIds[]" class="form-control"
                                                                               :value="item.idModules">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <a id="add-btn" class="btn btn-primary text-white" v-on:click="addMoreNewDetails(index)">Add</a> &nbsp;
                                                                        <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeNewModuleDetail(index)">Delete</a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <vue-autonumeric :options="autonumericFormat"
                                                                                     name="prices[]" class="form-control text-right" placeholder="Module Price"></vue-autonumeric>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeModule(item)">Delete</a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <a id="add-btn" class="btn btn-primary mt-3 text-white" v-on:click="addMoreModule">Add More Item</a>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-md-12 mt-3" v-for="(item, index) in discountList">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <span class="font-weight-bold" style="padding-left: 12px;">Discount : </span>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
{{--                                                                    <input type="radio" name="disc_type[]" value="rp" checked>--}}
{{--                                                                    <label>Rp</label><br>--}}
{{--                                                                    <input type="radio" name="disc_type[]" value="percent">--}}
{{--                                                                    <label>Percentage</label>--}}
                                                                    <label class="form-label" for="discount_1">Name</label>
                                                                    <input id="discount_1" name="disc_name[]" type="text" value=""
                                                                           class="form-control" placeholder="Discount">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
                                                                    <label class="form-label" for="discount_1">Disc Amount</label>
                                                                    <vue-autonumeric :options="autonumericFormat"
                                                                                     name="disc_amount[]" class="form-control" placeholder="Discount Amount"></vue-autonumeric>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
                                                                    <label class="form-label" for="discount_1">(Or) Disc Percentage</label>
                                                                    <vue-autonumeric :options="autonumericFormatPercent"
                                                                                     name="disc_percentage[]" class="form-control" placeholder="Discount Percentage"></vue-autonumeric>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <a id="add-btn" class="btn btn-primary mt-3 text-white" v-on:click="addMoreDiscount">Add More Discount</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <span class="font-weight-bold" style="padding-left: 12px;">Header Type</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="radio" id="header_3" name="header_type" value="1" checked>
                                                        <label for="header_3">BEN & WYATT DESIGN HOUSE</label><br>
                                                        <input type="radio" id="header_4" name="header_type" value="2">
                                                        <label for="header_4">PT PUTRA JAYA VISUAL</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <span class="font-weight-bold" style="padding-left: 12px;">Approval by Carla Safira</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" checked data-toggle="toggle" data-size="sm" name="authorization" data-on="Yes" data-off="No">
{{--                                                        <button type="button" class="btn btn-sm btn-secondary btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="authProcess()">--}}
{{--                                                            <div class="handle"></div>--}}
{{--                                                        </button>--}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.invoices.index') }}" class="btn btn-danger">Exit</a>
                                                <input type="submit" class="btn btn-success" value="Save">
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.5.4"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-autonumeric@1.2.6/dist/vue-autonumeric.min.js"></script>
    <script src="https://unpkg.com/vue-toasted"></script>

    <script type="text/x-template" id="select2-template">
        <select>
            <slot></slot>
        </select>
    </script>

    <script>
        Vue.component('select2', {
            props: ['options','value', 'url' ,'placeholder','extra', 'selected_action'],
            template: '#select2-template',
            data : function() {
                var thisVal = this;
                return {
                    ajaxOptions: {
                        url: this.url,
                        dataType: 'json',
                        delay: 250,
                        tags: true,
                        data: function(params) {
                            if (params === undefined || params === null)return this.extra;
                            if (thisVal.extra !== undefined){
                                thisVal.extra.q = $.trim(params.term);
                                return thisVal.extra;
                            }
                            else {
                                return {
                                    q: $.trim(params.term)
                                };
                            }
                        },
                        processResults: function(data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                };
            },
            mounted: function () {
                var vm = this;

                if (this.url != undefined){
                    $(vm.$el)
                    // init select2
                        .select2({
                            placeholder : {
                                id : "-1",
                                text : vm.placeholder
                            },
                            width: '100%',
                            ajax: vm.ajaxOptions
                        })
                        .val(this.value)
                        .trigger('change')
                        // emit event on change.
                        .on('change', function () {
                            // vm.$emit('input', this.value)
                            vm.$emit('input', $(this).val());
                            vm.$emit('selected_action', $(this).val());
                            // debugger;
                        });


                    if (this.value !== 0 && this.value !== null){
                        //there is preselected value, we need to query right away.
                        var initData = axios.get(this.url,{
                            params : this.extra
                        }).then(function(val){
                            if (val.data.length > 0){
                                for(var idx = 0 ; idx < val.data.length; idx++){

                                    if (val.data[idx].id == vm.value){
                                        var option = new Option(val.data[idx].text, val.data[idx].id, true, true);
                                        $(vm.$el).append(option).trigger('change');

                                        // manually trigger the `select2:select` event
                                        // $(vm.$el).trigger({
                                        //     type: 'select2:select',
                                        //     params: {
                                        //         data: val.data[idx]
                                        //     }
                                        // });
                                    }
                                }
                            }
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                }else if (vm.options !== undefined){
                    var opt = vm.options;
                    if (typeof(vm.options) === "object"){
                        opt = Object.entries(vm.options).map(function(v){
                            return {id : v[0], text : v[1]};
                        });
                    }
                    $(vm.$el)
                    // init select2
                        .select2({ data: opt })
                        .val(this.value)
                        .trigger('change')
                        // emit event on change.
                        .on('change', function () {
                            // vm.$emit('input', this.value)
                            vm.$emit('input', $(this).val());
                            vm.$emit('selected_action', $(this).val());
                        });
                }

            },
            watch: {
                value: function (value) {
                    if ([...value].sort().join(",") !== [...$(this.$el).val()].sort().join(","))
                        $(this.$el).val(value).trigger('change');
                    // update value
                    // $(this.$el)
                    //     .val(value)
                    //     .trigger('change');
                },
                options: function (value) {
                    this.options = value;
                    // update value
                    $(this.$el)
                    // init select2
                        .select2({ data: this.options })
                        .val(this.value)
                        .trigger('change')
                },
                url: function(value) {
                    this.ajaxOptions.url = this.url;
                    $(this.$el).select2({
                        placeholder : {
                            id : "-1",
                            text : value
                        },
                        width: '100%',
                        ajax: this.ajaxOptions
                    });
                },
                placeholder : function(value){
                    this.placeholder = value;
                    $(this.$el).select2({
                        placeholder : {
                            id : "-1",
                            text : value
                        },
                        width: '100%',
                        ajax: this.ajaxOptions
                    })
                },
                extra : function(value){
                    this.extra = value;
                    $(this.$el).select2({
                        placeholder : {
                            id : "-1",
                            text : value
                        },
                        width: '100%',
                        ajax: this.ajaxOptions
                    });
                }

            },
            destroyed: function () {
                $(this.$el).off().select2('destroy')
            }
        });

        let tmp = "{{$moduleJsons}}";
        console.log(tmp);
        let tmpStr = tmp.replace(/&quot;/g, '"');
        tmpStr = tmpStr.replace(/\t/g, '');
        let moduleJsons = JSON.parse(tmpStr);
        console.log(moduleJsons);
        let vueComponent = new Vue({
            el: '#vue-section',
            data: {
                idModules: parseInt('{{$latestModule}}'),
                itemList: [{
                    index: 0,
                    no: 1,
                    itemId: `item${1}`,
                    price: 0,
                    idModules: parseInt('{{$latestModule}}'),
                    modules: moduleJsons,
                    details: [],
                    detailModules: [{
                        index: 0,
                        no: 1,
                        value: ""
                    }],
                    newDetailModules:[{
                        value: ""
                    }]
                }],
                discountList:[{
                    index: 0,
                    no: 1,
                    description: '',
                    type:'Rp',
                    value:''
                }],

                autonumericFormat: {
                    minimumValue: '0',
                    maximumValue: '9999999999',
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    modifyValueOnWheel: false,
                    allowDecimalPadding: false,
                },
                autonumericFormatPercent: {
                    minimumValue: '0',
                    maximumValue: '100',
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    modifyValueOnWheel: false,
                    allowDecimalPadding: false,
                },
            },
            methods: {
                addMoreModule() {
                    let itemIdIdx = this.itemList.length + 1;
                    console.log(itemIdIdx);
                    this.idModules = parseInt(this.idModules) + 1;
                    let nData = {
                        index: 1,
                        no: 2,
                        itemId: `item${itemIdIdx}`,
                        price: 0,
                        idModules: this.idModules,
                        modules: moduleJsons,
                        detailModules: [{
                            index: 0,
                            no: 1,
                            value: ""
                        }],
                        newDetailModules:[{
                            value: ""
                        }]
                    };
                    this.itemList.push(nData);
                    debugger;
                },
                addMoreDetails(index) {
                    let itemIdIdx = this.itemList[index].detailModules.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        index: 1,
                        no: 2,
                        value: ""
                    };
                    this.itemList[index].detailModules.push(nData);
                    debugger;
                },
                addMoreNewDetails(index) {
                    let itemIdIdx = this.itemList[index].newDetailModules.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        value: this.itemList[index].idModules
                    };
                    this.itemList[index].newDetailModules.push(nData);
                    debugger;
                },
                addMoreDiscount() {
                    let itemIdIdx = this.discountList.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        index: 1,
                        no: 2,
                        description: '',
                        type:'Rp',
                        value:''
                    };
                    this.discountList.push(nData);
                    debugger;
                },
                removeModule(unit) {
                    this.itemList = this.itemList.filter(function (x) { return x !== unit; });
                },
                removeModuleDetail(index) {
                    let detailIndex = this.itemList[index].detailModules.length - 1;
                    console.log(detailIndex);
                    let unit = this.itemList[index].detailModules[detailIndex];
                    this.itemList[index].detailModules = this.itemList[index].detailModules.filter(function (x) { return x !== unit; });
                },
                removeNewModuleDetail(index) {
                    let detailIndex = this.itemList[index].newDetailModules.length - 1;
                    console.log(detailIndex);
                    let unit = this.itemList[index].newDetailModules[detailIndex];
                    this.itemList[index].newDetailModules = this.itemList[index].newDetailModules.filter(function (x) { return x !== unit; });
                },
                removeDiscount(unit) {
                    this.discountList = this.discountList.filter(function (x) { return x !== unit; });
                },
                resetItem() {
                    this.itemList = [];
                    this.addMoreUnit();
                },

                getDetailModule(itemId, index){
                    let id = $('#' + itemId).val();
                    axios.get('{{route('select.module-details')}}?header_id=' + id)
                        .then(response => {
                            debugger;
                            this.itemList[index].idModules = id;
                            this.itemList[index].detailModules = [];
                            this.addMoreDetails(index);
                            this.itemList[index].details = [];
                            this.itemList[index].details = response.data;
                            debugger;
                        });
                }
            }
        });
    </script>

    <script type="text/javascript">
        jQuery('#date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy"
        });
        $(".select2").select2();

        function authProcess(){
            alert("adf");
        }
    </script>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

    <style>
        body {
            font-family: 'Montserrat', 'Lato', 'Open Sans', 'Helvetica Neue', Helvetica, Calibri, Arial, sans-serif;
            color: #6b7381;
            background: #f2f2f2;
        }
        .jumbotron {
            background: #6b7381;
            color: #bdc1c8;
        }
        .jumbotron h1 {
            color: #fff;
        }
        .example {
            margin: 4rem auto;
        }
        .example > .row {
            margin-top: 2rem;
            height: 5rem;
            vertical-align: middle;
            text-align: center;
            border: 1px solid rgba(189, 193, 200, 0.5);
        }
        .example > .row:first-of-type {
            border: none;
            height: auto;
            text-align: left;
        }
        .example h3 {
            font-weight: 400;
        }
        .example h3 > small {
            font-weight: 200;
            font-size: 0.75em;
            color: #993aa5;
        }
        .example h6 {
            font-weight: 700;
            font-size: 0.65rem;
            letter-spacing: 3.32px;
            text-transform: uppercase;
            color: #bdc1c8;
            margin: 0;
            line-height: 5rem;
        }
        .example .btn-toggle {
            top: 50%;
            transform: translateY(-50%);
        }
        .btn-toggle {
            margin: 0 4rem;
            padding: 0;
            position: relative;
            border: none;
            height: 1.5rem;
            width: 3rem;
            border-radius: 1.5rem;
            color: #6b7381;
            background: #bdc1c8;
        }
        .btn-toggle:focus,
        .btn-toggle.focus,
        .btn-toggle:focus.active,
        .btn-toggle.focus.active {
            outline: none;
        }
        .btn-toggle:before,
        .btn-toggle:after {
            line-height: 1.5rem;
            width: 4rem;
            text-align: center;
            font-weight: 600;
            font-size: 0.75rem;
            text-transform: uppercase;
            letter-spacing: 2px;
            position: absolute;
            bottom: 0;
            transition: opacity 0.25s;
        }
        .btn-toggle:before {
            content: 'No';
            left: -4rem;
        }
        .btn-toggle:after {
            content: 'Yes';
            right: -4rem;
            opacity: 0.5;
        }
        .btn-toggle > .handle {
            position: absolute;
            top: 0.1875rem;
            left: 0.1875rem;
            width: 1.125rem;
            height: 1.125rem;
            border-radius: 1.125rem;
            background: #fff;
            transition: left 0.25s;
        }
        .btn-toggle.active {
            transition: background-color 0.25s;
        }
        .btn-toggle.active > .handle {
            left: 1.6875rem;
            transition: left 0.25s;
        }
        .btn-toggle.active:before {
            opacity: 0.5;
        }
        .btn-toggle.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-sm:before,
        .btn-toggle.btn-sm:after {
            line-height: -0.5rem;
            color: #fff;
            letter-spacing: 0.75px;
            left: 0.4125rem;
            width: 2.325rem;
        }
        .btn-toggle.btn-sm:before {
            text-align: right;
        }
        .btn-toggle.btn-sm:after {
            text-align: left;
            opacity: 0;
        }
        .btn-toggle.btn-sm.active:before {
            opacity: 0;
        }
        .btn-toggle.btn-sm.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-xs:before,
        .btn-toggle.btn-xs:after {
            display: none;
        }
        .btn-toggle:before,
        .btn-toggle:after {
            color: #6b7381;
        }
        .btn-toggle.active {
            background-color: #29b5a8;
        }
        .btn-toggle.btn-lg {
            margin: 0 5rem;
            padding: 0;
            position: relative;
            border: none;
            height: 2.5rem;
            width: 5rem;
            border-radius: 2.5rem;
        }
        .btn-toggle.btn-lg:focus,
        .btn-toggle.btn-lg.focus,
        .btn-toggle.btn-lg:focus.active,
        .btn-toggle.btn-lg.focus.active {
            outline: none;
        }
        .btn-toggle.btn-lg:before,
        .btn-toggle.btn-lg:after {
            line-height: 2.5rem;
            width: 5rem;
            text-align: center;
            font-weight: 600;
            font-size: 1rem;
            text-transform: uppercase;
            letter-spacing: 2px;
            position: absolute;
            bottom: 0;
            transition: opacity 0.25s;
        }
        .btn-toggle.btn-lg:before {
            content: 'No';
            left: -5rem;
        }
        .btn-toggle.btn-lg:after {
            content: 'Yes';
            right: -5rem;
            opacity: 0.5;
        }
        .btn-toggle.btn-lg > .handle {
            position: absolute;
            top: 0.3125rem;
            left: 0.3125rem;
            width: 1.875rem;
            height: 1.875rem;
            border-radius: 1.875rem;
            background: #fff;
            transition: left 0.25s;
        }
        .btn-toggle.btn-lg.active {
            transition: background-color 0.25s;
        }
        .btn-toggle.btn-lg.active > .handle {
            left: 2.8125rem;
            transition: left 0.25s;
        }
        .btn-toggle.btn-lg.active:before {
            opacity: 0.5;
        }
        .btn-toggle.btn-lg.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-lg.btn-sm:before,
        .btn-toggle.btn-lg.btn-sm:after {
            line-height: 0.5rem;
            color: #fff;
            letter-spacing: 0.75px;
            left: 0.6875rem;
            width: 3.875rem;
        }
        .btn-toggle.btn-lg.btn-sm:before {
            text-align: right;
        }
        .btn-toggle.btn-lg.btn-sm:after {
            text-align: left;
            opacity: 0;
        }
        .btn-toggle.btn-lg.btn-sm.active:before {
            opacity: 0;
        }
        .btn-toggle.btn-lg.btn-sm.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-lg.btn-xs:before,
        .btn-toggle.btn-lg.btn-xs:after {
            display: none;
        }
        .btn-toggle.btn-sm {
            margin: 0 0.5rem;
            padding: 0;
            position: relative;
            border: none;
            height: 1.5rem;
            width: 3rem;
            border-radius: 1.5rem;
        }
        .btn-toggle.btn-sm:focus,
        .btn-toggle.btn-sm.focus,
        .btn-toggle.btn-sm:focus.active,
        .btn-toggle.btn-sm.focus.active {
            outline: none;
        }
        .btn-toggle.btn-sm:before,
        .btn-toggle.btn-sm:after {
            line-height: 1.5rem;
            width: 0.5rem;
            text-align: center;
            font-weight: 600;
            font-size: 0.55rem;
            text-transform: uppercase;
            letter-spacing: 2px;
            position: absolute;
            bottom: 0;
            transition: opacity 0.25s;
        }
        .btn-toggle.btn-sm:before {
            content: 'No';
            left: -0.5rem;
        }
        .btn-toggle.btn-sm:after {
            content: 'Yes';
            right: -0.5rem;
            opacity: 0.5;
        }
        .btn-toggle.btn-sm > .handle {
            position: absolute;
            top: 0.1875rem;
            left: 0.1875rem;
            width: 1.125rem;
            height: 1.125rem;
            border-radius: 1.125rem;
            background: #fff;
            transition: left 0.25s;
        }
        .btn-toggle.btn-sm.active {
            transition: background-color 0.25s;
        }
        .btn-toggle.btn-sm.active > .handle {
            left: 1.6875rem;
            transition: left 0.25s;
        }
        .btn-toggle.btn-sm.active:before {
            opacity: 0.5;
        }
        .btn-toggle.btn-sm.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-sm.btn-sm:before,
        .btn-toggle.btn-sm.btn-sm:after {
            line-height: -0.5rem;
            color: #fff;
            letter-spacing: 0.75px;
            left: 0.4125rem;
            width: 2.325rem;
        }
        .btn-toggle.btn-sm.btn-sm:before {
            text-align: right;
        }
        .btn-toggle.btn-sm.btn-sm:after {
            text-align: left;
            opacity: 0;
        }
        .btn-toggle.btn-sm.btn-sm.active:before {
            opacity: 0;
        }
        .btn-toggle.btn-sm.btn-sm.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-sm.btn-xs:before,
        .btn-toggle.btn-sm.btn-xs:after {
            display: none;
        }
        .btn-toggle.btn-xs {
            margin: 0 0;
            padding: 0;
            position: relative;
            border: none;
            height: 1rem;
            width: 2rem;
            border-radius: 1rem;
        }
        .btn-toggle.btn-xs:focus,
        .btn-toggle.btn-xs.focus,
        .btn-toggle.btn-xs:focus.active,
        .btn-toggle.btn-xs.focus.active {
            outline: none;
        }
        .btn-toggle.btn-xs:before,
        .btn-toggle.btn-xs:after {
            line-height: 1rem;
            width: 0;
            text-align: center;
            font-weight: 600;
            font-size: 0.75rem;
            text-transform: uppercase;
            letter-spacing: 2px;
            position: absolute;
            bottom: 0;
            transition: opacity 0.25s;
        }
        .btn-toggle.btn-xs:before {
            content: 'No';
            left: 0;
        }
        .btn-toggle.btn-xs:after {
            content: 'Yes';
            right: 0;
            opacity: 0.5;
        }
        .btn-toggle.btn-xs > .handle {
            position: absolute;
            top: 0.125rem;
            left: 0.125rem;
            width: 0.75rem;
            height: 0.75rem;
            border-radius: 0.75rem;
            background: #fff;
            transition: left 0.25s;
        }
        .btn-toggle.btn-xs.active {
            transition: background-color 0.25s;
        }
        .btn-toggle.btn-xs.active > .handle {
            left: 1.125rem;
            transition: left 0.25s;
        }
        .btn-toggle.btn-xs.active:before {
            opacity: 0.5;
        }
        .btn-toggle.btn-xs.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-xs.btn-sm:before,
        .btn-toggle.btn-xs.btn-sm:after {
            line-height: -1rem;
            color: #fff;
            letter-spacing: 0.75px;
            left: 0.275rem;
            width: 1.55rem;
        }
        .btn-toggle.btn-xs.btn-sm:before {
            text-align: right;
        }
        .btn-toggle.btn-xs.btn-sm:after {
            text-align: left;
            opacity: 0;
        }
        .btn-toggle.btn-xs.btn-sm.active:before {
            opacity: 0;
        }
        .btn-toggle.btn-xs.btn-sm.active:after {
            opacity: 1;
        }
        .btn-toggle.btn-xs.btn-xs:before,
        .btn-toggle.btn-xs.btn-xs:after {
            display: none;
        }
        .btn-toggle.btn-secondary {
            color: #6b7381;
            background: #bdc1c8;
        }
        .btn-toggle.btn-secondary:before,
        .btn-toggle.btn-secondary:after {
            color: #6b7381;
        }
        .btn-toggle.btn-secondary.active {
            background-color: #ff8300;
        }

    </style>
@endsection
