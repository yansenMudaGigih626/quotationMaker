@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Create New Quotation</h2>

                {{ Form::open(['route'=>['admin.quotations.store'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @include('partials.admin._messages')
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                            @endforeach
                                            <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="quotation_number">Quotation Number *</label>
                                                            <input id="quotation_number" name="quotation_number" type="text" value="{{ old('quotation_number') }}"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="project_name">Project Name </label>
                                                            <input id="project_name" name="project_name" type="text" value="{{ old('project_name') }}"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="date">Quotation Date *</label>
                                                            <input type="text" id="date" name="date" class="form-control" value="{{ $todayDate }}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="clients">Client *</label>
                                                            <select class="form-control select2 custom-select" name="client" id="clients">
                                                                @foreach($clients as $client)
                                                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="form-label">Modules *</label>
                                                    <table>
                                                    @foreach($modules as $module)
                                                        <tr>
                                                            <td>
                                                                <div class="custom-control custom-checkbox mr-sm-2">
                                                                    <input type="checkbox" class="custom-control-input" id="{{ $module->id }}" name="modules[]" value="{{ $module->id }}">
                                                                    <label class="custom-control-label" for="{{ $module->id }}">{{ $module->description }}</label>
                                                                </div>
                                                                @foreach($module->module_details as $detail)
                                                                    <div class="custom-control custom-checkbox mr-sm-2 m-l-20">
                                                                        <input type="checkbox" class="custom-control-input" id="detail{{ $detail->id }}" name="details[]" value="{{ $detail->id }}">
                                                                        <label class="custom-control-label" for="detail{{ $detail->id }}">{{ $detail->description }}</label>
                                                                    </div>
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <input type="text" name="amounts[]" class="form-control" placeholder="Price">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </table>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_1">Discount 1</label>
                                                            <input id="discount_1" name="discount_1" type="text" value="{{ old('discount_1') }}"
                                                                   class="form-control" placeholder="Discount 1 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_2">Discount 2</label>
                                                            <input id="discount_2" name="discount_2" type="text" value="{{ old('discount_2') }}"
                                                                   class="form-control" placeholder="Discount 2 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_3">Discount 3</label>
                                                            <input id="discount_3" name="discount_3" type="text" value="{{ old('discount_3') }}"
                                                                   class="form-control" placeholder="Discount 3 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_4">Discount 4</label>
                                                            <input id="discount_4" name="discount_4" type="text" value="{{ old('discount_4') }}"
                                                                   class="form-control" placeholder="Discount 4 Total">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <span class="font-weight-bold" style="padding-left: 12px;">Header Type</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="radio" id="header_1" name="header_type" value="1" checked>
                                                        <label for="header_1">BEN & WYATT DESIGN HOUSE</label><br>
                                                        <input type="radio" id="header_2" name="header_type" value="2">
                                                        <label for="header_2">PT PUTRA JAYA VISUAL</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="font-weight-bold" style="padding-left: 12px;">Approval by Carla Safira</span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" class="btn btn-sm btn-secondary btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                                                            <div class="handle"></div>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.quotations.index') }}" class="btn btn-danger">Exit</a>
                                                <input type="submit" class="btn btn-success" value="Save">
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<style>
    body {
    font-family: 'Montserrat', 'Lato', 'Open Sans', 'Helvetica Neue', Helvetica, Calibri, Arial, sans-serif;
    color: #6b7381;
    background: #f2f2f2;
    }
    .jumbotron {
    background: #6b7381;
    color: #bdc1c8;
    }
    .jumbotron h1 {
    color: #fff;
    }
    .example {
    margin: 4rem auto;
    }
    .example > .row {
    margin-top: 2rem;
    height: 5rem;
    vertical-align: middle;
    text-align: center;
    border: 1px solid rgba(189, 193, 200, 0.5);
    }
    .example > .row:first-of-type {
    border: none;
    height: auto;
    text-align: left;
    }
    .example h3 {
    font-weight: 400;
    }
    .example h3 > small {
    font-weight: 200;
    font-size: 0.75em;
    color: #993aa5;
    }
    .example h6 {
    font-weight: 700;
    font-size: 0.65rem;
    letter-spacing: 3.32px;
    text-transform: uppercase;
    color: #bdc1c8;
    margin: 0;
    line-height: 5rem;
    }
    .example .btn-toggle {
    top: 50%;
    transform: translateY(-50%);
    }
    .btn-toggle {
    margin: 0 4rem;
    padding: 0;
    position: relative;
    border: none;
    height: 1.5rem;
    width: 3rem;
    border-radius: 1.5rem;
    color: #6b7381;
    background: #bdc1c8;
    }
    .btn-toggle:focus,
    .btn-toggle.focus,
    .btn-toggle:focus.active,
    .btn-toggle.focus.active {
    outline: none;
    }
    .btn-toggle:before,
    .btn-toggle:after {
    line-height: 1.5rem;
    width: 4rem;
    text-align: center;
    font-weight: 600;
    font-size: 0.75rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    position: absolute;
    bottom: 0;
    transition: opacity 0.25s;
    }
    .btn-toggle:before {
    content: 'No';
    left: -4rem;
    }
    .btn-toggle:after {
    content: 'Yes';
    right: -4rem;
    opacity: 0.5;
    }
    .btn-toggle > .handle {
    position: absolute;
    top: 0.1875rem;
    left: 0.1875rem;
    width: 1.125rem;
    height: 1.125rem;
    border-radius: 1.125rem;
    background: #fff;
    transition: left 0.25s;
    }
    .btn-toggle.active {
    transition: background-color 0.25s;
    }
    .btn-toggle.active > .handle {
    left: 1.6875rem;
    transition: left 0.25s;
    }
    .btn-toggle.active:before {
    opacity: 0.5;
    }
    .btn-toggle.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-sm:before,
    .btn-toggle.btn-sm:after {
    line-height: -0.5rem;
    color: #fff;
    letter-spacing: 0.75px;
    left: 0.4125rem;
    width: 2.325rem;
    }
    .btn-toggle.btn-sm:before {
    text-align: right;
    }
    .btn-toggle.btn-sm:after {
    text-align: left;
    opacity: 0;
    }
    .btn-toggle.btn-sm.active:before {
    opacity: 0;
    }
    .btn-toggle.btn-sm.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-xs:before,
    .btn-toggle.btn-xs:after {
    display: none;
    }
    .btn-toggle:before,
    .btn-toggle:after {
    color: #6b7381;
    }
    .btn-toggle.active {
    background-color: #29b5a8;
    }
    .btn-toggle.btn-lg {
    margin: 0 5rem;
    padding: 0;
    position: relative;
    border: none;
    height: 2.5rem;
    width: 5rem;
    border-radius: 2.5rem;
    }
    .btn-toggle.btn-lg:focus,
    .btn-toggle.btn-lg.focus,
    .btn-toggle.btn-lg:focus.active,
    .btn-toggle.btn-lg.focus.active {
    outline: none;
    }
    .btn-toggle.btn-lg:before,
    .btn-toggle.btn-lg:after {
    line-height: 2.5rem;
    width: 5rem;
    text-align: center;
    font-weight: 600;
    font-size: 1rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    position: absolute;
    bottom: 0;
    transition: opacity 0.25s;
    }
    .btn-toggle.btn-lg:before {
    content: 'No';
    left: -5rem;
    }
    .btn-toggle.btn-lg:after {
    content: 'Yes';
    right: -5rem;
    opacity: 0.5;
    }
    .btn-toggle.btn-lg > .handle {
    position: absolute;
    top: 0.3125rem;
    left: 0.3125rem;
    width: 1.875rem;
    height: 1.875rem;
    border-radius: 1.875rem;
    background: #fff;
    transition: left 0.25s;
    }
    .btn-toggle.btn-lg.active {
    transition: background-color 0.25s;
    }
    .btn-toggle.btn-lg.active > .handle {
    left: 2.8125rem;
    transition: left 0.25s;
    }
    .btn-toggle.btn-lg.active:before {
    opacity: 0.5;
    }
    .btn-toggle.btn-lg.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-lg.btn-sm:before,
    .btn-toggle.btn-lg.btn-sm:after {
    line-height: 0.5rem;
    color: #fff;
    letter-spacing: 0.75px;
    left: 0.6875rem;
    width: 3.875rem;
    }
    .btn-toggle.btn-lg.btn-sm:before {
    text-align: right;
    }
    .btn-toggle.btn-lg.btn-sm:after {
    text-align: left;
    opacity: 0;
    }
    .btn-toggle.btn-lg.btn-sm.active:before {
    opacity: 0;
    }
    .btn-toggle.btn-lg.btn-sm.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-lg.btn-xs:before,
    .btn-toggle.btn-lg.btn-xs:after {
    display: none;
    }
    .btn-toggle.btn-sm {
    margin: 0 0.5rem;
    padding: 0;
    position: relative;
    border: none;
    height: 1.5rem;
    width: 3rem;
    border-radius: 1.5rem;
    }
    .btn-toggle.btn-sm:focus,
    .btn-toggle.btn-sm.focus,
    .btn-toggle.btn-sm:focus.active,
    .btn-toggle.btn-sm.focus.active {
    outline: none;
    }
    .btn-toggle.btn-sm:before,
    .btn-toggle.btn-sm:after {
    line-height: 1.5rem;
    width: 0.5rem;
    text-align: center;
    font-weight: 600;
    font-size: 0.55rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    position: absolute;
    bottom: 0;
    transition: opacity 0.25s;
    }
    .btn-toggle.btn-sm:before {
    content: 'No';
    left: -0.5rem;
    }
    .btn-toggle.btn-sm:after {
    content: 'Yes';
    right: -0.5rem;
    opacity: 0.5;
    }
    .btn-toggle.btn-sm > .handle {
    position: absolute;
    top: 0.1875rem;
    left: 0.1875rem;
    width: 1.125rem;
    height: 1.125rem;
    border-radius: 1.125rem;
    background: #fff;
    transition: left 0.25s;
    }
    .btn-toggle.btn-sm.active {
    transition: background-color 0.25s;
    }
    .btn-toggle.btn-sm.active > .handle {
    left: 1.6875rem;
    transition: left 0.25s;
    }
    .btn-toggle.btn-sm.active:before {
    opacity: 0.5;
    }
    .btn-toggle.btn-sm.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-sm.btn-sm:before,
    .btn-toggle.btn-sm.btn-sm:after {
    line-height: -0.5rem;
    color: #fff;
    letter-spacing: 0.75px;
    left: 0.4125rem;
    width: 2.325rem;
    }
    .btn-toggle.btn-sm.btn-sm:before {
    text-align: right;
    }
    .btn-toggle.btn-sm.btn-sm:after {
    text-align: left;
    opacity: 0;
    }
    .btn-toggle.btn-sm.btn-sm.active:before {
    opacity: 0;
    }
    .btn-toggle.btn-sm.btn-sm.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-sm.btn-xs:before,
    .btn-toggle.btn-sm.btn-xs:after {
    display: none;
    }
    .btn-toggle.btn-xs {
    margin: 0 0;
    padding: 0;
    position: relative;
    border: none;
    height: 1rem;
    width: 2rem;
    border-radius: 1rem;
    }
    .btn-toggle.btn-xs:focus,
    .btn-toggle.btn-xs.focus,
    .btn-toggle.btn-xs:focus.active,
    .btn-toggle.btn-xs.focus.active {
    outline: none;
    }
    .btn-toggle.btn-xs:before,
    .btn-toggle.btn-xs:after {
    line-height: 1rem;
    width: 0;
    text-align: center;
    font-weight: 600;
    font-size: 0.75rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    position: absolute;
    bottom: 0;
    transition: opacity 0.25s;
    }
    .btn-toggle.btn-xs:before {
    content: 'No';
    left: 0;
    }
    .btn-toggle.btn-xs:after {
    content: 'Yes';
    right: 0;
    opacity: 0.5;
    }
    .btn-toggle.btn-xs > .handle {
    position: absolute;
    top: 0.125rem;
    left: 0.125rem;
    width: 0.75rem;
    height: 0.75rem;
    border-radius: 0.75rem;
    background: #fff;
    transition: left 0.25s;
    }
    .btn-toggle.btn-xs.active {
    transition: background-color 0.25s;
    }
    .btn-toggle.btn-xs.active > .handle {
    left: 1.125rem;
    transition: left 0.25s;
    }
    .btn-toggle.btn-xs.active:before {
    opacity: 0.5;
    }
    .btn-toggle.btn-xs.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-xs.btn-sm:before,
    .btn-toggle.btn-xs.btn-sm:after {
    line-height: -1rem;
    color: #fff;
    letter-spacing: 0.75px;
    left: 0.275rem;
    width: 1.55rem;
    }
    .btn-toggle.btn-xs.btn-sm:before {
    text-align: right;
    }
    .btn-toggle.btn-xs.btn-sm:after {
    text-align: left;
    opacity: 0;
    }
    .btn-toggle.btn-xs.btn-sm.active:before {
    opacity: 0;
    }
    .btn-toggle.btn-xs.btn-sm.active:after {
    opacity: 1;
    }
    .btn-toggle.btn-xs.btn-xs:before,
    .btn-toggle.btn-xs.btn-xs:after {
    display: none;
    }
    .btn-toggle.btn-secondary {
    color: #6b7381;
    background: #bdc1c8;
    }
    .btn-toggle.btn-secondary:before,
    .btn-toggle.btn-secondary:after {
    color: #6b7381;
    }
    .btn-toggle.btn-secondary.active {
    background-color: #ff8300;
    }

    </style>
@endsection

@section('scripts')
    <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        jQuery('#date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy"
        });
        $(".select2").select2();
    </script>
@endsection
