@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Invoices</h2>
                <div class="ml-auto text-right">
                    <a href="{{ route('admin.invoices.create') }}" class="btn btn-success">
                        <i class="fas fa-plus"></i> Add
                    </a>
                </div>
                @include('partials.admin._messages')
                <table id="user-admin" class="table table-striped table-bordered dt-responsive nowrap" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Created At</th>
                        <th>Number</th>
                        <th>Client</th>
                        <th>Project Name</th>
                        <th>Date</th>
{{--                        <th>Sub Total</th>--}}
{{--                        <th>Discount_1</th>--}}
{{--                        <th>Discount_2</th>--}}
{{--                        <th>Discount_3</th>--}}
{{--                        <th>Discount_4</th>--}}
                        <th>Amount</th>
                        <th>Created By</th>
                        <th>Option</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    @include('partials._delete')
@endsection

@section('styles')
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.js') }}"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $('#user-admin').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            ajax: '{!! route('datatables.invoices') !!}',
            // order: [ [0, 'asc'] ],
            columns: [
                { data: 'created_at', name: 'created_at', class: 'text-center', searchable: false,
                    render: function ( data, type, row ){
                        if ( type === 'display' || type === 'filter' ){
                            // return moment(data).format('DD MMMM YYYY HH:mm:ss');
                            return moment(data).format('DD MMMM YYYY');
                        }
                        return data;
                    }
                },
                { data: 'number', name: 'number', class: 'text-center'},
                { data: 'client', name: 'client', class: 'text-center'},
                { data: 'project_name', name: 'project_name', class: 'text-center'},
                { data: 'date', name: 'date', class: 'text-center', searchable: false,
                    render: function ( data, type, row ){
                        if ( type === 'display' || type === 'filter' ){
                            // return moment(data).format('DD MMMM YYYY HH:mm:ss');
                            return moment(data).format('DD MMMM YYYY');
                        }
                        return data;
                    }
                },
                // { data: 'sub_total', name: 'sub_total', class: 'text-center'},
                // { data: 'discount_1', name: 'discount_1', class: 'text-center'},
                // { data: 'discount_2', name: 'discount_2', class: 'text-center'},
                // { data: 'discount_3', name: 'discount_3', class: 'text-center'},
                // { data: 'discount_4', name: 'discount_4', class: 'text-center'},
                { data: 'amount', name: 'amount', class: 'text-center'},
                { data: 'created_by', name: 'created_by', class: 'text-center'},
                { data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            language: {
                url: "{{ asset('indonesian.json') }}"
            }
        });

        $(document).on('click', '.delete-modal', function(){
            $('#deleteModal').modal({
                backdrop: 'static',
                keyboard: false
            });

            $('#deleted-id').val($(this).data('id'));
        });
    </script>
    @include('partials._deletejs', ['routeUrl' => 'admin.invoices.destroy', 'redirectUrl' => 'admin.invoices.index'])
@endsection
