@extends('layouts.admin')

@section('content')
    <div id="vue-section" class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Edit Invoice</h2>

                {{ Form::open(['route'=>['admin.invoices.update'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @include('partials.admin._messages')
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                        @endforeach
                                        <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="invoice_number">Invoice Number *</label>
                                                            <input id="quotation_number" name="invoice_number" type="text" value="{{ $invoice->number }}"
                                                                   class="form-control" required>
                                                            <input type="hidden" name="id" value="{{ $invoice->id }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="project_name">Project Name *</label>
                                                            <input id="project_name" name="project_name" type="text" value="{{ $invoice->project_name }}"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="date">Quotation Date *</label>
                                                            <input type="text" id="date" name="date" class="form-control" value="{{ $quotation_date }}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="clients">Client *</label>
                                                            <select class="form-control select2 custom-select" id="clients" name="client">
                                                                @foreach($clients as $client)
                                                                    @if($client->id == $invoice->client_id)
                                                                        <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                                                                    @else
                                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="table_detail" class="table table-bordered" style="table-layout: fixed; width: 2000px;">
                                                            <thead>
                                                            <tr class="text-center">
                                                                <th width="20%">Module</th>
                                                                <th width="20%">(Or) New Module</th>
                                                                <th width="20%">Module Detail</th>
                                                                <th width="20%">(Or) New Module Detail</th>
                                                                <th width="15%">Price</th>
                                                                <th width="10%"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr v-for="(item, index) in itemList">
                                                                <td>
                                                                    <select class="form-control" placeholder="Select Module"
                                                                            v-bind:id="item.itemId" v-on:change="getDetailModule(item.itemId, index)">
                                                                        <option value="0">-- Select Module --</option>
                                                                        <option v-for="(module, indexModule) in item.modules" :value="module.id" :selected="checkSelected(module.id, item.selectedModule)">
                                                                            @{{module.description}}
                                                                        </option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="new-modules[]" class="form-control" placeholder="Create New Module">
                                                                    <input type="hidden" name="modules[]" class="form-control" :value="item.idModules">
                                                                </td>
                                                                <td>
                                                                    <div class="row mb-3" v-for="(itemDetail, indexDetail) in item.detailModules">
                                                                        {{--                                                                        <select2 url="{{ route('select.modules') }}"--}}
                                                                        {{--                                                                                 name="moduleDetails[index][]" class="form-control form-control-select2" placeholder="Select Detail">--}}
                                                                        {{--                                                                        </select2>--}}
                                                                        <select name="moduleDetails[]" class="form-control" placeholder="Select Detail">
                                                                            <option value="0">-- Select Detail --</option>
                                                                            <option v-for="(detail, indexModule) in item.details" :value="detail.id" :selected="checkSelectedDetail(itemDetail.value, detail.id)">
                                                                                @{{detail.text}}
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <a id="add-btn" class="btn btn-primary text-white" v-on:click="addMoreDetails(index)">Add</a> &nbsp;
                                                                        <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeModuleDetail(index)">Delete</a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="row mb-3" v-for="(itemDetail, indexDetail) in item.newDetailModules">
                                                                        <input type="text" name="new-module-details[]" class="form-control" placeholder="Create New Detail">
                                                                        <input type="hidden" name="newModuleDetailIds[]" class="form-control" :value="item.idModules">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <a id="add-btn" class="btn btn-primary text-white" v-on:click="addMoreNewDetails(index)">Add</a> &nbsp;
                                                                        <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeNewModuleDetail(index)">Delete</a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <vue-autonumeric :options="autonumericFormat"
                                                                                     name="prices[]" class="form-control text-right" placeholder="Module Price" :value="item.price"></vue-autonumeric>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a class="btn btn-danger text-white" style="cursor: pointer" v-on:click="removeModule(item)">Delete</a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <a id="add-btn" class="btn btn-primary mt-3 text-white" v-on:click="addMoreModule">Add More Item</a>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-md-12 mt-3" v-for="(item, index) in discountList">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <span class="font-weight-bold" style="padding-left: 12px;">Discount : </span>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
                                                                    {{--                                                                    <input type="radio" name="disc_type[]" value="rp" checked>--}}
                                                                    {{--                                                                    <label>Rp</label><br>--}}
                                                                    {{--                                                                    <input type="radio" name="disc_type[]" value="percent">--}}
                                                                    {{--                                                                    <label>Percentage</label>--}}
                                                                    <label class="form-label" for="discount_1">Name</label>
                                                                    <input id="discount_1" name="disc_name[]" type="text" value=""
                                                                           class="form-control" placeholder="Discount" :value="item.description">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
                                                                    <label class="form-label" for="discount_1">Disc Amount</label>
                                                                    <vue-autonumeric :options="autonumericFormat"
                                                                                     name="disc_amount[]" class="form-control" placeholder="Discount Amount" :value="item.amount"></vue-autonumeric>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-float form-group-lg">
                                                                <div class="form-line">
                                                                    <label class="form-label" for="discount_1">(Or) Disc Percentage</label>
                                                                    <vue-autonumeric :options="autonumericFormatPercent"
                                                                                     name="disc_percentage[]" class="form-control" placeholder="Discount Percentage" :value="item.percentage"></vue-autonumeric>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <a id="add-btn" class="btn btn-primary mt-3 text-white" v-on:click="addMoreDiscount">Add More Discount</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label">Invoice Type</label>
                                                            <input type="text" name="invoice_type" class="form-control"
                                                                   placeholder="Down Payment/Pelunasan/Pembayaran" value="{{ $invoice->invoice_type }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label">Percentage (DP/Pelunasan/Pembayaran)</label>
                                                            <vue-autonumeric :options="autonumericFormatPercent"
                                                                             name="invoice_type_percent" class="form-control"
                                                                             placeholder="percentage" value="{{ $invoice->invoice_type_percent }}"></vue-autonumeric>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <span class="font-weight-bold" style="padding-left: 12px;">Header Type</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="radio" id="header_1" name="header_type" value="1" @if($invoice->header_type == 1) checked @endif>
                                                    <label for="header_1">BEN & WYATT DESIGN HOUSE</label><br>
                                                    <input type="radio" id="header_2" name="header_type" value="2" @if($invoice->header_type == 2) checked @endif>
                                                    <label for="header_2">PT PUTRA JAYA VISUAL</label>
                                                </div>
                                            </div>

                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.invoices.index') }}" class="btn btn-danger">Exit</a>
                                                <input type="submit" class="btn btn-success" value="Save">
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.5.4"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-autonumeric@1.2.6/dist/vue-autonumeric.min.js"></script>
    <script src="https://unpkg.com/vue-toasted"></script>

    <script type="text/x-template" id="select2-template">
        <select>
            <slot></slot>
        </select>
    </script>

    <script>

        let tmp = "{{$moduleJsons}}";
        let tmpStr = tmp.replace(/&quot;/g, '"');
        tmpStr = tmpStr.replace(/\t/g, '');
        let moduleJsons = JSON.parse(tmpStr);

        let tmp2 = "{{$itemListJsons}}";
        let tmpStr2 = tmp2.replace(/&quot;/g, '"');
        tmpStr2 = tmpStr2.replace(/\t/g, '');
        let itemListJsons = JSON.parse(tmpStr2);
        console.log(itemListJsons);

        let tmp4 = "{{$discountListJsons}}";
        let tmpStr4 = tmp4.replace(/&quot;/g, '"');
        tmpStr4 = tmpStr4.replace(/\t/g, '');
        let discountListJsons = JSON.parse(tmpStr4);
        console.log(discountListJsons);

        let vueComponent = new Vue({
            el: '#vue-section',
            data: {
                idModules: parseInt('{{$latestModule}}'),
                itemList: itemListJsons,
                discountList: discountListJsons,

                autonumericFormat: {
                    minimumValue: '0',
                    maximumValue: '9999999999',
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    modifyValueOnWheel: false,
                    allowDecimalPadding: false,
                },
                autonumericFormatPercent: {
                    minimumValue: '0',
                    maximumValue: '100',
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    modifyValueOnWheel: false,
                    allowDecimalPadding: false,
                },
            },
            methods: {
                addMoreModule() {
                    let itemIdIdx = this.itemList.length + 1;
                    console.log(itemIdIdx);
                    this.idModules = parseInt(this.idModules) + 1;
                    let nData = {
                        index: 1,
                        no: 2,
                        itemId: `item${itemIdIdx}`,
                        price: 0,
                        idModules: this.idModules,
                        modules: moduleJsons,
                        detailModules: [{
                            index: 0,
                            no: 1,
                            value: ""
                        }],
                        newDetailModules:[{
                            value: ""
                        }]
                    };
                    this.itemList.push(nData);
                debugger;
                },
                addMoreDetails(index) {
                    let itemIdIdx = this.itemList[index].detailModules.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        index: 1,
                        no: 2,
                        value: ""
                    };
                    this.itemList[index].detailModules.push(nData);
                debugger;
                },
                addMoreNewDetails(index) {
                    let itemIdIdx = this.itemList[index].newDetailModules.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        value: this.itemList[index].idModules
                    };
                    this.itemList[index].newDetailModules.push(nData);
                debugger;
                },
                addMoreDiscount() {
                    let itemIdIdx = this.discountList.length + 1;
                    console.log(itemIdIdx);
                    let nData = {
                        index: 1,
                        no: 2,
                        description: '',
                        type:'Rp',
                        value:''
                    };
                    this.discountList.push(nData);
                debugger;
                },
                removeModule(unit) {
                    this.itemList = this.itemList.filter(function (x) { return x !== unit; });
                },
                removeModuleDetail(index) {
                    let detailIndex = this.itemList[index].detailModules.length - 1;
                    console.log(detailIndex);
                    let unit = this.itemList[index].detailModules[detailIndex];
                    this.itemList[index].detailModules = this.itemList[index].detailModules.filter(function (x) { return x !== unit; });
                },
                removeNewModuleDetail(index) {
                    let detailIndex = this.itemList[index].newDetailModules.length - 1;
                    console.log(detailIndex);
                    let unit = this.itemList[index].newDetailModules[detailIndex];
                    this.itemList[index].newDetailModules = this.itemList[index].newDetailModules.filter(function (x) { return x !== unit; });
                },
                removeDiscount(unit) {
                    this.discountList = this.discountList.filter(function (x) { return x !== unit; });
                },
                resetItem() {
                    this.itemList = [];
                    this.addMoreUnit();
                },

                getDetailModule(itemId, index){
                    let id = $('#' + itemId).val();
                    axios.get('{{route('select.module-details')}}?header_id=' + id)
                        .then(response => {
                        debugger;
                            this.itemList[index].idModules = id;
                            this.itemList[index].detailModules = [];
                            this.addMoreDetails(index);
                            this.itemList[index].details = [];
                            this.itemList[index].details = response.data;
                        debugger;
                        });
                },
                checkSelected(dtId, unitId) {
                    if (dtId === unitId) {
                        console.log(dtId, unitId);
                        return true;
                    } else {
                        return false;
                    }
                },
                checkSelectedDetail(dtId, unitId) {
                    console.log(dtId, unitId);
                    if (dtId === unitId) {
                        console.log(dtId, unitId);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        });
    </script>

    <script type="text/javascript">
        jQuery('#date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy"
        });
        $(".select2").select2();
    </script>
@endsection
