@extends ('layouts.frontend')
@section('content')

    <section id="non-printable" class="py-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-6">
                </div>
                <div class="col-6 text-right">
                    <button onclick="print()" class="btn btn-success">Print</button>
                </div>
            </div>
        </div>
    </section>
    <div id="print-section">
        <table class="report-container left-margin">
            <thead class="report-header">
                <tr>
                    <th class="report-header-cell">
                        <section class="py-5 pb-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="font-weight-bold">{{$configuration->pt_name}}</p>
                                        <p class="font-weight-normal">{{$configuration->pt_address}}</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <img src="{{ asset('images/quotationmaker/bnw-logo.png') }}" alt="img" class="bnw-logo">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </th>
                </tr>
            </thead>
            <tbody class="report-content">
            <tr>
                <td class="report-content-cell">
                    <section class="pb-3">
                        <div class="container">
                            <div class="row pb-5 mb-5">
                                <div class="col-3">
                                    <p class="font-weight-bold quot-header">QUOTATION</p>
                                </div>
                                <div class="col-3">
                                    <p class="font-weight-bold">CLIENT</p>
                                    <p>{{$quotation->client_data->name}}</p>
                                </div>
                                <div class="col-3">
                                    <p class="font-weight-bold">PROJECT</p>
                                    <p>{{$quotation->project_name}}</p>
                                </div>
                                <div class="col-3">
                                    <p class="font-weight-bold">DATE</p>
                                    <p>{{$quotation->created_at_string}}</p>
                                </div>
                            </div>
                            <div class="row border-bottom-custom">
                                <div class="col-3">
                                    <p>Service</p>
                                </div>
                                <div class="col-3">
                                    <p>Details</p>
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-3">
                                    <p>Price</p>
                                </div>
                            </div>
                            @php($ct=1)
                            @foreach($quotation->modules as $nModule)
                                <div class="row pt-3 mb-3">
                                    <div class="col-3">
                                        <p class="font-weight-bold">{{$nModule->module->description}}</p>
                                        <p></p>
                                    </div>
                                    <div class="col-3">

                                        @foreach($quotation->module_details as $nDetail)
                                            @if($nModule->module_id == $nDetail->module_detail->module_id)
                                                <p>{{$nDetail->module_detail->description}}</p>
                                            @endif
                                        @endforeach
                                    </div>

                                    <div class="col-3">
                                    </div>
                                    <div class="col-3">
                                        <p class="font-weight-bold">{{$nModule->amount_string}}</p>
                                    </div>
                                </div>
                            @endforeach
                            @if($quotation->module_details->count() > 10 && $quotation->module_details->count() < 16)
                                <div class="col-12 print-show" style="height:350px;"></div>
                            @endif

                            <div class="row border-bottom-custom"></div>
                            <div class="row py-4">
                                <div class="col-3">
                                    <p>SUBTOTAL</p>
                                    <p>DISCOUNT 1</p>
                                    <p>DISCOUNT 2</p>
                                    <p>DISCOUNT 3</p>
                                    <p>DISCOUNT 4</p>
                                </div>
                                <div class="col-3">

                                </div>
                                <div class="col-3">

                                </div>
                                <div class="col-3 font-weight-bold">
                                    <p>{{$quotation->subtotal_string}}</p>
                                    <p>{{$quotation->discount_1_string}}</p>
                                    <p>{{$quotation->discount_2_string}}</p>
                                    <p>{{$quotation->discount_3_string}}</p>
                                    <p>{{$quotation->discount_4_string}}</p>
                                </div>
                            </div>
                            <div class="row border-bottom-custom"></div>
                            <div class="row pt-4 pb-3">
                                <div class="col-3 font-weight-bold">
                                    <p>TOTAL</p>
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-3 font-weight-bold">
                                    <p class="total-price">IDR <span>{{$quotation->total_amount_string}}</span></p>
                                </div>
                            </div>
                            <div class="row border-bottom-custom"></div>
                            @if($isSign == 1)
                                <div class="row padd-space">
                                    <div class="col-4">
                                        <p>Tangerang, {{$now}}</p>
                                        <p>Approved by</p>
                                        <img src="{{ asset('images/quotationmaker/test-sign.png') }}" alt="sign" class="signature">
                                    </div>
                                    <div class="col-8"></div>
                                </div>
                            @else
                                <div class="row padd-space">
                                    <div class="col-4">
                                        <p>Tangerang, {{$now}}</p>
                                        <p>Approved by</p>
                                    </div>
                                    <div class="col-8"></div>
                                </div>
                            @endif
                            <div class="row padd-space2">
                                <div class="col-4">
                                    <p class="font-weight-bold">{{$configuration->sign_name}}</p>
                                    <p>{{$configuration->sign_position}}</p>
                                </div>
                                <div class="col-4">
                                    <p class="font-weight-bold">{{$quotation->client_data->pic_name}}</p>
                                    <p>{{$quotation->client_data->pic_position}}</p>
                                </div>
                                <div class="col-4"></div>
                            </div>
                            <div class="row padd-space2">
                                <div class="col-3">
                                    <p class="font-weight-bold">Account Information</p>
                                    <p>Bank : {{$configuration->acc_bank}}</p>
                                </div>
                                <div class="col-3">
                                    <p>Account No.</p>
                                    <p>{{$configuration->acc_no}}</p>
                                </div>
                                <div class="col-3">
                                    <p>Account Holder</p>
                                    <p>{{$configuration->acc_holder}}</p>
                                </div>
                            </div>
                            <div class="row border-bottom-custom pb-3"></div>
                            <div class="row py-3">
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-4">
                                            <p><span class="font-weight-bold">E</span>&nbsp info@benandwyatt.com</p>
                                        </div>
                                        <div class="col-4">
                                            <p><span class="font-weight-bold">P</span>&nbsp 0822 9982 3888</p>
                                        </div>
                                        <div class="col-4">
                                            <p class="font-weight-bold">www.benandwyatt.com</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </tr>
            </tbody>
        </table>
    </div>
@endsection


@section('styles')
    <style>
        .signature{
            width:200px;
            padding-top:40px;
            margin-bottom: -50px !important;
        }
        /*tr {*/
        /*page-break-inside: avoid !important;*/
        /*page-break-after: auto !important;*/
        /*}*/
        /*td, th {*/
        /*page-break-inside: avoid !important;*/
        /*}*/

        /*table.report-container {*/
        /*page-break-after:always;*/
        /*!*margin-left:50px;*!*/
        /*}*/
        /*thead.report-header {*/
        /*display:table-header-group !important;*/
        /*!*page-break-after: avoid !important;*!*/
        /*}*/
        /*tfoot { display: table-row-group;*/
        /*}*/
        /*tr {page-break-inside: avoid;*/
        /*}*/

        .padd-space3{
            padding-top: 50px;
        }
        .padd-space2{
            padding-top: 100px;
        }
        .padd-space{
            padding-top: 100px;
        }
        .total-price{
            font-size:21px;
        }
        .padd-price{
            padding-top: 75px;
        }
        .border-bottom-custom{
            border-bottom: 1px solid black;
        }
        .quot-header{
            font-size:20px;
        }
        .bnw-logo{
            width:220px;
            padding-top:20px;
        }

        @media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
            #non-printable { display: none; }
            #print-section {
                display: block;
                padding-top: -50px;
            }
            .print-show{
                display: block !important;
            }
        }
        .print-show{
            display: none;
        }
    </style>
@endsection
@section('scripts')
    <script>
        function print(){
            w=window.open();
            w.document.write($('#print-section').html());
            w.print();
            w.close();
        }
    </script>
@endsection
