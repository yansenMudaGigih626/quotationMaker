
@extends ('layouts.frontend')
@section('content')

    <section id="non-printable" class="py-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-6">
                </div>
                <div class="col-6 text-right">
                    <a href="{{route('admin.quotations.create-invoice', ['id' => $quotation->id])}}"  class="btn btn-primary">Create Invoice</a>
                    <button onclick="print()" class="btn btn-success">Print</button>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5 pb-5 page-header">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <p class="font-weight-bold">{{$configuration->pt_name ?? ""}}</p>
                    <p class="font-weight-normal">{{$configuration->pt_address ?? ""}}</p>
                    <p class="font-weight-normal">(T) 0822-9982-3888, (E) info@benandwyatt.com, (W) www.benandwyatt.com</p>
                </div>
                <div class="col-4 text-right">
                    @if($configuration->id == 1)
                        <img src="{{ asset('images/quotationmaker/bnw-logo.png') }}" alt="img" class="bnw-logo">
                    @else

                        <img src="{{ asset('images/quotationmaker/pjv-04.png') }}" alt="img" class="pjv-logo">
                    @endif
                </div>
            </div>
        </div>
    </section>
    <body>

    <table style="width: 100%">

        <thead>
        <tr>
            <td>
                <!--place holder for the fixed-position header-->
                <div class="page-header-space"></div>
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                @php($ct=0)
                <div>
                    <div class="container">
                        <div class="row pb-5 mb-4">
                            <div class="col-3">
                                <p class="font-weight-bold quot-header">QUOTATION</p>
{{--                                <p>Full Payment {{$quotation->module_details->count()}}</p>--}}
                                <p>Full Payment</p>
                            </div>
                            <div class="col-3">
                                <p class="font-weight-bold">CLIENT</p>
                                <p>{{$quotation->client_data->name}}</p>
                            </div>
                            <div class="col-3">
                                <p class="font-weight-bold">PROJECT</p>
                                <p>{{$quotation->project_name}}</p>
                            </div>
                            <div class="col-3">
                                <p class="font-weight-bold">DATE</p>
                                <p>{{$quotation->created_at_string}}</p>
                            </div>
                        </div>
                        <div class="row border-bottom-custom">
                            <div class="col-3">
                                <p>Service</p>
                            </div>
                            <div class="col-3">
                                <p>Details</p>
                            </div>
                            <div class="col-3">
                            </div>
                            <div class="col-3">
                                <p>Price</p>
                            </div>
                        </div>
                        <div>
                            @foreach($quotation->modules as $nModule)
                                <div class="row pt-3 mb-3">
                                    <div class="col-3">
                                        <p class="font-weight-bold">{{$nModule->module->description}}</p>
                                        <p></p>
                                    </div>
                                    <div class="col-3">
                                        @foreach($quotation->module_details as $nDetail)
                                            @if($nModule->module_id == $nDetail->module_detail->module_id)
                                                <p>{{$nDetail->module_detail->description}}</p>
                                                @php($ct++)
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="col-3">
                                    </div>
                                    <div class="col-3">
                                        <p class="font-weight-bold">{{$nModule->amount_string}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div>
                            <div class="row border-bottom-custom "></div>
                            <div class="row py-4">
                                <div class="col-3">
                                    <p>SUBTOTAL</p>
                                    @foreach($quotation->quotation_discounts as $nModuleDiscount)
                                        <p>DISCOUNT {{$nModuleDiscount->description}}</p>
                                    @endforeach
                                </div>
                                <div class="col-3">

                                </div>
                                <div class="col-3">

                                </div>
                                <div class="col-3 font-weight-bold">
                                    <p>{{$quotation->subtotal_string}}</p>
                                    @foreach($quotation->quotation_discounts as $nModuleDiscount)
                                        @if($nModuleDiscount->amount > 0)
                                            <p>{{$nModuleDiscount->amount_string}}</p>
                                        @else
                                            <p>{{$nModuleDiscount->percentage_rupiah_string}} ({{$nModuleDiscount->percentage}}%)</p>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="row border-bottom-custom"></div>
                            <div class="row pt-4 pb-3 border-bottom-custom">
                                <div class="col-3 font-weight-bold">
                                    <p>TOTAL</p>
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-3 font-weight-bold">
                                    <p class="total-price">IDR <span>{{$quotation->total_amount_string}}</span></p>
                                </div>
                            </div>
                        </div>
                        @if($ct%20 > 10)
                            <div class="page"></div>
                        @endif
                        <div class="padd-space">
                            @if($isSign == 1)
                                <div class="row">
                                    <div class="col-4">
                                        <p class="">Tangerang, {{$now}}</p>
                                        <p>Approved by</p>
                                        <img src="{{ asset('images/quotationmaker/sign-02.png') }}" alt="sign" class="signature">
                                    </div>
                                    <div class="col-8"></div>
                                </div>

                            @else
                                <div class="row">
                                    <div class="col-4">
                                        <p  class="">Tangerang, {{$now}}</p>
                                        <p>Approved by</p>
{{--                                        <div style="height:100px;"></div>--}}
                                    </div>
                                    <div class="col-8"></div>
                                </div>
                            @endif
                            <div class="row padd-space2">
                                <div class="col-4">
                                    <p class="font-weight-bold">{{$configuration->sign_name}}</p>
                                    <p>{{$configuration->sign_position}}</p>
                                </div>
                                <div class="col-4">
                                    <p class="font-weight-bold">{{$quotation->client_data->pic_name}}</p>
                                    <p>{{$quotation->client_data->pic_position}}</p>
                                </div>
                            </div>
                            <div class="row padd-space3">
                                <div class="col-3">
                                    <p class="font-weight-bold">Account Information</p>
                                    <p>Bank : {{$configuration->acc_bank}}</p>
                                </div>
                                <div class="col-3">
                                    <p>Account No.</p>
                                    <p>{{$configuration->acc_no}}</p>
                                </div>
                                <div class="col-3">
                                    <p>Account Holder</p>
                                    <p>{{$configuration->acc_holder}}</p>
                                </div>
                            </div>
                            <div class="row border-bottom-custom pb-3"></div>
                            <div class="row py-3">
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-4">
                                            <p><span class="font-weight-bold">E</span>&nbsp info@benandwyatt.com</p>
                                        </div>
                                        <div class="col-4">
                                            <p><span class="font-weight-bold">P</span>&nbsp 0822 9982 3888</p>
                                        </div>
                                        <div class="col-4">
                                            <p class="font-weight-bold">www.benandwyatt.com</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td>
                <!--place holder for the fixed-position footer-->
                {{--                <div class="page-footer-space"></div>--}}
            </td>
        </tr>
        </tfoot>

    </table>

    </body>

@endsection


@section('styles')
    <style>
        .margs-min{
            margin-top:-50px;
            margin-bottom:-100px;
        }
        .padd-space3{
            padding-top: 50px;
        }
        .padd-space2{
            padding-top: 100px;
        }
        .padd-space{
            padding-top: 5%;
        }
        padd-space-0{
            padding-top:-50px !important;
        }
        .total-price{
            font-size:21px;
        }
        .padd-price{
            padding-top: 75px;
        }
        .border-bottom-custom{
            border-bottom: 1px solid black;
        }
        .quot-header{
            font-size:20px;
        }
        .bnw-logo{
            width:310px;
            padding-top:20px;
        }
        .pjv-logo{
            width:160px;
            padding-top:20px;
        }
        .signature{
            /*width:200px;*/
            /*padding-top:30px;*/
            /*margin-bottom: -60px !important;*/
            width: 130px;
            padding-top: 15px;
            margin-bottom: -70px !important;
            margin-left: 45px;
        }
        /* Styles go here */
        /*.page-header, .page-header-space {*/
        /*    height: 150px;*/
        /*}*/

        .page-footer, .page-footer-space {
            height: 50px;

        }

        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            border-top: 1px solid black; /* for demo */
            background: yellow; /* for demo */
        }

        .page-header {
            position: fixed;
            /*top: 0mm;*/
            width: 100%;
            /*border-bottom: 1px solid black; !* for demo *!*/
            /*background: yellow; !* for demo *!*/
        }

        .page {
            page-break-after: always !important;
        }

        @page  :first .page-header, .page-header-space {
            padding-top: 260px;
        }
        .page-header{
            position: relative !important;
        }

        @media print {
            thead {display: table-header-group;}
            tfoot {display: table-footer-group;}
            #non-printable { display: none; }
            button {display: none;}

            body {margin: 0;}

            .page{
                padding-left: 0px;
            }
            /*.page-header{*/
            /*    position: absolute !important;*/
            /*}*/

            /*.page-header, .page-header-space {*/
            /*    height: 160px;*/
            /*}*/
            .padd-space{
                padding-top: 2% !important;
            }
            .padd-space-210{
                padding-top: 50px !important;
            }
            /*.border-subtotal{*/
            /*    padding-top: 180px;*/
            /*}*/
            .border-subtotal{
                padding-top: 50px;
            }
            .border-subtotal-2{
                padding-top: 50px;
            }
            /*.custom-pt-5{*/
            /*    margin-top: 400px;*/
            /*}*/
        }
    </style>
@endsection
@section('scripts')
    <script>
        function print(){
            w=window.open();
            w.document.write($('#print-section').html());
            w.print();
            w.close();
        }
    </script>
@endsection
