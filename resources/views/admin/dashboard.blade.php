@extends('layouts.admin')

@section('content')


    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales Cards  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 mb-3">
                <h3>Selamat Datang Admin</h3>
            </div>
            @foreach($authorizedMenus as $authorizedMenu)
                @if($authorizedMenu->index > 4)
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{ !empty($authorizedMenu->route_dashboard) ? route($authorizedMenu->route_dashboard) : '#' }}" class="box bg-custom-red text-center">
                                <h1 class="font-light text-white"><i class="{{ $authorizedMenu->icon }}"></i></h1>
                                <h6 class="text-white">{{ $authorizedMenu->name }}</h6>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
            <!-- Column -->
        </div>

        <hr class="border-top-red"/>


    </div>

@endsection

@section('styles')
    <style>
        .notification-scrollable{
            height: 350px;
            overflow-y: scroll;
        }
    </style>
@endsection

@section('scripts')
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="{{ asset('backend/assets/libs/flot/excanvas.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('backend/dist/js/pages/chart/chart-page-init.js') }}"></script>
@endsection
