@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <h2 class="card-title m-b-0">Show Invoice</h2>

                {{ Form::open(['route'=>['admin.auth-invoices.approve'],'method' => 'post','id' => 'general-form']) }}
                <div class="container-fluid relative animatedParent animateOnce" id="app">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body b-b">
                                    <div class="tab-content pb-3" id="v-pills-tabContent">
                                        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                                            @include('partials.admin._messages')
                                            @foreach($errors->all() as $error)
                                                <ul>
                                                    <li>
                                                        <span class="help-block">
                                                            <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                                        </span>
                                                    </li>
                                                </ul>
                                        @endforeach
                                        <!-- Input -->
                                            <div class="body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="quotation_number">Invoice Number</label>
                                                            <input id="quotation_number" name="quotation_number" type="text" value="{{ $invoice->number }}"
                                                                   class="form-control" readonly>
                                                            <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="project_name">Project Name</label>
                                                            <input id="project_name" name="project_name" type="text" value="{{ $invoice->project_name }}"
                                                                   class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="date">Invoice Date</label>
                                                            <input type="text" id="date" name="date" class="form-control" value="{{ $quotation_date }}" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="client_name">Client</label>
                                                            <input type="text" id="client_name" class="form-control" value="{{ $invoice->client_data->name }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="form-label">Modules *</label>
                                                    @foreach($invoice->modules as $module)
                                                        <label class="form-control">{{ $module->description }}</label>
                                                        <ul>
                                                            @foreach($module->module_details as $detail)
                                                                <li>{{ $detail->description }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_1">Discount 1</label>
                                                            <input id="discount_1" name="discount_1" type="text" value="{{ $invoice->discount_1 }}"
                                                                   class="form-control" placeholder="Discount 1 Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_2">Discount 2</label>
                                                            <input id="discount_2" name="discount_2" type="text" value="{{ $invoice->discount_2 }}"
                                                                   class="form-control" placeholder="Discount 2 Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_3">Discount 3</label>
                                                            <input id="discount_3" name="discount_3" type="text" value="{{ $invoice->discount_3 }}"
                                                                   class="form-control" placeholder="Discount 3 Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-float form-group-lg">
                                                        <div class="form-line">
                                                            <label class="form-label" for="discount_4">Discount 4</label>
                                                            <input id="discount_4" name="discount_4" type="text" value="{{ $invoice->discount_4 }}"
                                                                   class="form-control" placeholder="Discount 4 Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                                <a href="{{ route('admin.auth-invoices.index') }}" class="btn btn-danger">Exit</a>
                                                @if($invoice->is_authorized == 0)
                                                    <input type="submit" class="btn btn-success" value="Approve">
                                                @endif
                                            </div>
                                            <!-- #END# Input -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        jQuery('#date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy"
        });
        $(".select2").select2();
    </script>
@endsection
