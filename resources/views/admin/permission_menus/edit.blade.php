@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3>UBAH OTORISASI MENU</h3>
                    </div>
                </div>

                {{ Form::open(['route'=>['admin.permission-menus.update'],'method' => 'post','id' => 'general-form']) }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">
                                <div class="body">
                                    @include('partials.admin._messages')
                                    @if(count($errors))
                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        <ul>
                                                            @foreach($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-12">
                                        <div class="form-group form-float form-group-lg">
                                            <div class="form-line">
                                                <label class="form-label" for="roleName">Role/Posisi</label>
                                                <input type="text" id="roleName" name="roleName" class="form-control col-md-7 col-xs-12" value="{{ $adminRole->name }}" readonly />
                                                <input type="hidden" id="role" name="admin_role_id" value="{{ $adminRole->id }}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        @php($idx = 0)
                                        <table class="table">
                                            <thead>
                                            <th colspan="3">
                                                Menu
                                            </th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                @foreach($menus as $menu)
                                                    @php($idx++)
                                                    <td>
                                                        <label>
                                                            @php($flag = 0)
                                                            @foreach($permissionMenus as $permission)
                                                                @if($permission->menu_id == $menu->id)
                                                                    @php($flag = 1)
                                                                @endif
                                                            @endforeach

                                                            @if($flag == 1)
                                                                <input type="checkbox" class="flat" id="chk{{$menu->id}}" name="chk[]" onchange="changeInput('{{ $menu->id }}')" checked="checked"> {{ $menu->name }}
                                                                <input type="text" hidden="true" value="{{ $menu->id }}" id="ids{{ $menu->id }}" name="ids[]"/>
                                                                <input type="text" hidden="true" value="{{ $menu->id }}" id="idsDelete{{ $menu->id }}" name="idsDelete[]" disabled/>
                                                            @else
                                                                <input type="checkbox" class="flat" id="chk{{$menu->id}}" name="chk[]" onchange="changeInput('{{ $menu->id }}')" > {{ $menu->name }}
                                                                <input type="text" hidden="true" value="{{ $menu->id }}" id="ids{{ $menu->id }}" name="ids[]" disabled/>
                                                                <input type="text" hidden="true" value="{{ $menu->id }}" id="idsDelete{{ $menu->id }}" name="idsDelete[]"/>
                                                            @endif
                                                        </label>
                                                    </td>
                                            @if($idx == 3)
                                                <tr/><tr>
                                                    @php($idx = 0)
                                                    @endif
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" class="flat" id="selectAll"/> Select/Unselect All
                                                        </label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                    <a href="{{ route('admin.permission-menus.index') }}" class="btn btn-danger">BATAL</a>
                                    <input type="submit" class="btn btn-success" value="SIMPAN">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection


@section('styles')

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#selectAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked).change();
        });
        function changeInput(id){
            if(document.getElementById("chk"+id).checked == true){
                document.getElementById("ids"+id).disabled = false;
                document.getElementById("idsDelete"+id).disabled = true;
            }
            else{
                document.getElementById("ids"+id).disabled = true;
                document.getElementById("idsDelete"+id).disabled = false;
            }
        }
    </script>
@endsection
