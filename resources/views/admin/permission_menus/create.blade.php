@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3>OTORISASI MENU</h3>
                    </div>
                </div>

                {{ Form::open(['route'=>['admin.permission-menus.store'],'method' => 'post','id' => 'general-form']) }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">
                                <div class="body">
                                    @include('partials.admin._messages')
                                    @if(count($errors))
                                        <div class="col-md-12">
                                            <div class="form-group form-float form-group-lg">
                                                <div class="form-line">
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        <ul>
                                                            @foreach($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-12">
                                        <div class="form-group form-float form-group-lg">
                                            <div class="form-line">
                                                <label class="form-label" for="role">Role/Posisi</label>
                                                <select id="role" name="role" class="form-control">
                                                    <option value="-1" @if(empty(old('role'))) selected @endif> - Pilih Role - </option>
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->id }}" {{ old('role') == $role->id ? "selected":"" }}>{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        @php($idx = 0)
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Menu</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                @foreach($menus as $menu)
                                                    @php($idx++)
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" class="flat" id="chk{{$menu->id}}" name="chk[]" onclick="changeInput('{{ $menu->id }}')" /> {{ $menu->name }}
                                                            <input type="text" hidden="true" value="{{ $menu->id }}" id="{{ $menu->id }}" name="ids[]" disabled/>
                                                        </label>
                                                    </td>
                                            @if($idx == 3)
                                                <tr/><tr>
                                                    @php($idx = 0)
                                                    @endif
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" class="flat" id="selectAll"/> Select All
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" class="flat" id="unSelectAll"/> Unselect All
                                                        </label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-11 col-sm-11 col-xs-12" style="margin: 3% 0 3% 0;">
                                    <a href="{{ route('admin.permission-menus.index') }}" class="btn btn-danger">BATAL</a>
                                    <input type="submit" class="btn btn-success" value="SIMPAN">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection


@section('styles')

@endsection

@section('scripts')
    <script type="text/javascript">
        $("#selectall").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        function changeInput(id){
            if(document.getElementById("chk"+id).checked == true){
                document.getElementById(id).disabled = false;
            }
            else{
                document.getElementById(id).disabled = true;
            }
        }
    </script>
@endsection
