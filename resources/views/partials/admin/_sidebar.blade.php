<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-3 mt-3" style="border-top: 1px solid #eeeeee;">
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('admin.dashboard') }}" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-view-dashboard"></i>--}}
{{--                        <span class="hide-menu">Dashboard</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                @foreach($authorizedMenus as $authorizedMenu)
                    <li class="sidebar-item">
                        <a class="sidebar-link {{ empty($authorizedMenu->route) ? 'has-arrow' : '' }} waves-effect waves-dark" href="{{ !empty($authorizedMenu->route) ? route($authorizedMenu->route) : 'javascript:void(0)' }}" aria-expanded="false">
                            <i class="{{ $authorizedMenu->icon }}"></i>
                            <span class="hide-menu">{!! $authorizedMenu->name !!} </span>
                        </a>
                        @if($authorizedMenu->menu->menu_subs->count() > 0)
                            <ul aria-expanded="false" class="collapse first-level">
                                @foreach($authorizedMenu->menu->menu_subs as $subMenu)
                                    <li class="sidebar-item">
                                        <a href="{{ route($subMenu->route) }}" class="sidebar-link">
                                            <i class="{{ $subMenu->icon }}"></i>
                                            <span class="hide-menu"> {{ $subMenu->name }} </span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach

{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link waves-effect waves-dark" href="{{ route('admin.permission-menus.index') }}" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-settings"></i>--}}
{{--                        <span class="hide-menu">Otorisasi Menu </span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-account-multiple"></i>--}}
{{--                        <span class="hide-menu">Master Dealer </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.users.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Daftar MD </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.users.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat MD Baru</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.user_categories.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Kategori MD </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.user_categories.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat Kategori MD Baru</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-account-multiple"></i>--}}
{{--                        <span class="hide-menu">Admin User </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.admin-users.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Daftar Admin </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.admin-users.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat Admin Baru</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-book-open"></i>--}}
{{--                        <span class="hide-menu">Banner </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.banner.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Daftar Banner </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.banner.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat Banner Baru</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-cellphone"></i>--}}
{{--                        <span class="hide-menu"> Produk </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.product.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Daftar Produk </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.product.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Tambah Produk </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.product.customize.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Kustomisasi Harga </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.product.category.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Kategori Produk </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.product.brand.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-format-list-bulleted"></i>--}}
{{--                                <span class="hide-menu"> Brand Produk </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-receipt"></i>--}}
{{--                        <span class="hide-menu">Order Request </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.orders.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-file-document"></i>--}}
{{--                                <span class="hide-menu"> Daftar OR </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-receipt"></i>--}}
{{--                        <span class="hide-menu">Sales Order </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.sales_order.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-file-document"></i>--}}
{{--                                <span class="hide-menu"> Daftar SO </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.sales_order.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat SO Baru </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-receipt"></i>--}}
{{--                        <span class="hide-menu">Kajian Order </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.kajian_order.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-file-document"></i>--}}
{{--                                <span class="hide-menu"> Daftar KO </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.kajian_order.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat KO Baru </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-receipt"></i>--}}
{{--                        <span class="hide-menu">Invoice </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.invoice.index')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-file-document"></i>--}}
{{--                                <span class="hide-menu"> Daftar Invoice </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.invoice.create')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-plus"></i>--}}
{{--                                <span class="hide-menu"> Buat Invoice Baru </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-receipt"></i>--}}
{{--                        <span class="hide-menu">Laporan </span>--}}
{{--                    </a>--}}
{{--                    <ul aria-expanded="false" class="collapse  first-level">--}}
{{--                        <li class="sidebar-item">--}}
{{--                            <a href="{{route('admin.report.sales')}}" class="sidebar-link">--}}
{{--                                <i class="mdi mdi-file-document"></i>--}}
{{--                                <span class="hide-menu"> Laporan Sales </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
